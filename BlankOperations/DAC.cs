﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Dynamics.Retail.Pos.BlankOperations
{
    class DAC
    {
        private SqlConnection sqlConn;
        public string msjError;
        public bool hasError;
        public DAC(string _conn)
        {
            sqlConn = new SqlConnection(_conn);
        }

        public DataTable getCustData(string _rfc, string _tranid = "", string _store = "", string _terminal = "", bool _forLayout = false)
        {
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_getCustData @rfc";
                SqlCommand sqlComm;
                if (_forLayout)
                {
                    commText = "exec xsp_getCustDataLT @storeid,@terminal,@transId, @rfc";
                    sqlComm = new SqlCommand(commText, sqlConn);
                    sqlComm.Parameters.Add(new SqlParameter("@storeid", (object)_store));
                    sqlComm.Parameters.Add(new SqlParameter("@terminal", (object)_terminal));
                    sqlComm.Parameters.Add(new SqlParameter("@transId", (object)_tranid));
                    sqlComm.Parameters.Add(new SqlParameter("@rfc", (object)_rfc));
                }
                else
                {
                    sqlComm = new SqlCommand(commText, sqlConn);
                    sqlComm.Parameters.Add(new SqlParameter("@rfc", (object)_rfc));
                }
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return DtResults;
        }

        public bool saveCustData(string _rfc, string _nombre, string _apellidos, string _email, string _telefono, string _calle, string _noInt, string _noExt, string _ciudad, string _colonia, string _municipio, string _cp, string _edo, string _pais, bool _notificar, string _tipoPersona)
        {
            bool retval = false;
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_saveCustData @rfc, @nombre, @apellidos, @email, @telefono, @calle, @noInt, @noExt, @ciudad, @colonia, @municipio, @cp, @edo, @pais, @notificar, @tipoPersona";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@rfc", (object)_rfc));
                sqlComm.Parameters.Add(new SqlParameter("@nombre", (object)_nombre));
                sqlComm.Parameters.Add(new SqlParameter("@apellidos", (object)_apellidos));
                sqlComm.Parameters.Add(new SqlParameter("@email", (object)_email));
                sqlComm.Parameters.Add(new SqlParameter("@telefono", (object)_telefono));
                sqlComm.Parameters.Add(new SqlParameter("@calle", (object)_calle));
                sqlComm.Parameters.Add(new SqlParameter("@noInt", (object)_noInt));
                sqlComm.Parameters.Add(new SqlParameter("@noExt", (object)_noExt));
                sqlComm.Parameters.Add(new SqlParameter("@ciudad", (object)_ciudad));
                sqlComm.Parameters.Add(new SqlParameter("@colonia", (object)_colonia));
                sqlComm.Parameters.Add(new SqlParameter("@municipio", (object)_municipio));
                sqlComm.Parameters.Add(new SqlParameter("@cp", (object)_cp));
                sqlComm.Parameters.Add(new SqlParameter("@edo", (object)_edo));
                sqlComm.Parameters.Add(new SqlParameter("@pais", (object)_pais));
                sqlComm.Parameters.Add(new SqlParameter("@notificar", (object)_notificar));
                sqlComm.Parameters.Add(new SqlParameter("@tipoPersona", (object)_tipoPersona));
                sqlComm.ExecuteNonQuery();
                retval = true;
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return retval;
        }
        public DataTable getInvcData(string _storeId, string _terminal, string _transaction)
        {
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_getInvcData @storeid, @terminal, @transaction";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@storeid", (object)_storeId));
                sqlComm.Parameters.Add(new SqlParameter("@terminal", (object)_terminal));
                sqlComm.Parameters.Add(new SqlParameter("@transaction", (object)_transaction));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return DtResults;
        }
        public DataTable getDetailData(string _storeId, string _terminal, string _transaction)
        {
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_getDetailData @storeid, @terminal, @transaction";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@storeid", (object)_storeId));
                sqlComm.Parameters.Add(new SqlParameter("@terminal", (object)_terminal));
                sqlComm.Parameters.Add(new SqlParameter("@transaction", (object)_transaction));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return DtResults;
        }
        public DataTable getStoreData(string _storeId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_storeAddr @storeid";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@storeid", (object)_storeId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return DtResults;
        }
        public DataTable getConfigData(string _dataAreaId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_getConfigData @dataAreaId";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@dataAreaId", (object)_dataAreaId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return DtResults;
        }
        public bool saveInvData(string store, string Terminal, string transactionid, string UUID, string dataArea, string xmlDoc)
        {
            bool retval = false;
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_insertInvData @store, @Terminal, @transactionid, @UUID, @dataArea, @xmlDoc";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@store", (object)store));
                sqlComm.Parameters.Add(new SqlParameter("@Terminal", (object)Terminal));
                sqlComm.Parameters.Add(new SqlParameter("@transactionid", (object)transactionid));
                sqlComm.Parameters.Add(new SqlParameter("@UUID", (object)UUID));
                sqlComm.Parameters.Add(new SqlParameter("@dataArea", (object)dataArea));
                sqlComm.Parameters.Add(new SqlParameter("@xmlDoc", (object)xmlDoc));
                sqlComm.ExecuteNonQuery();
                retval = true;
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return retval;
        }

        public bool checkTransDate(string _store, string _terminal, string _transactionId)
        {
            bool retVal = true;
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_checkTransDate @store, @terminal, @transactionid";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@store", (object)_store));
                sqlComm.Parameters.Add(new SqlParameter("@terminal", (object)_terminal));
                sqlComm.Parameters.Add(new SqlParameter("@transactionid", (object)_transactionId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
                if (DtResults.Rows.Count > 0)
                    retVal = false;
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return retVal;
        }

        public bool checkIsInvoiced(string _store, string _terminal, string _transactionId)
        {
            bool retVal = true;
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_checIsInvoiced @store, @terminal, @transactionid";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@store", (object)_store));
                sqlComm.Parameters.Add(new SqlParameter("@terminal", (object)_terminal));
                sqlComm.Parameters.Add(new SqlParameter("@transactionid", (object)_transactionId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
                if (DtResults.Rows.Count > 0)
                    retVal = false;
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return retVal;
        }

        public DataTable GRWValidateDigitalCard(string storeId, string itemId)
        {
            SqlDataAdapter dt = new SqlDataAdapter();
            DataSet retval = new DataSet();

            try
            {
                string cmdText = "Exec xsp_ValidateItemCFDI @STOREID, @ITEMID ";
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, sqlConn))
                {
                    command.Parameters.Add("@STOREID", SqlDbType.NVarChar, 10).Value = storeId;
                    command.Parameters.Add("@ITEMID", SqlDbType.NVarChar, 20).Value = itemId;
                    dt = new SqlDataAdapter(command);
                    dt.Fill(retval);
                }
            }
            finally
            {
                if (sqlConn.State != ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
            }
            return retval.Tables[0];
        }

        public DataTable getPedimentos(string _itemId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_getPedimentos @itemid";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@itemid", (object)_itemId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return DtResults;

        }
        public bool checkAnticipo(string _store, string _terminal, string _transactionId)
        {
            bool retVal = true;
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_checkAnticipo @store, @terminal, @transactionid";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@store", (object)_store));
                sqlComm.Parameters.Add(new SqlParameter("@terminal", (object)_terminal));
                sqlComm.Parameters.Add(new SqlParameter("@transactionid", (object)_transactionId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
                if (DtResults.Rows.Count > 0)
                {
                    string value = DtResults.Rows[0][0].ToString();
                    if (value == "1")
                        retVal = true;
                    else
                        retVal = false;
                }
                else
                    retVal = false;
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return retVal;
        }
        public DataTable getTicketsForInvoice(string _storeId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                string commText = "exec xsp_getTicketsForInvoice @storeid";
                SqlCommand sqlComm = new SqlCommand(commText, sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@storeid", (object)_storeId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
            }
            catch (Exception exc)
            {
                hasError = true;
                msjError = exc.Message;
            }
            finally
            {
                sqlConn.Close();
            }
            return DtResults;
        }

        public DataTable getTransactionSuspended(string SuspendedTransactionId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT TOP 1 [TRANSACTIONID],[SUSPENDEDTRAN],[SALESPERSONID],[SALESPERSONNAME] FROM [dbo].[GRWSUSPENDEDTRAN] WHERE TRANSACTIONID = @TRANSACTIONID";
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, sqlConn))
                {
                    command.Parameters.Add("@TRANSACTIONID", SqlDbType.NVarChar).Value = SuspendedTransactionId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (sqlConn.State != ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getTransactionSuspendedNum(string SuspendedTransactionId)
        {
            DataTable DtResults = new DataTable();
            try
            {

                string cmdText = "SELECT TOP 1 [SUSPENDEDTRAN],[SALESPERSONID],[SALESPERSONNAME],[FOLIO] FROM [dbo].[GRWSUSPENDEDTRAN] WHERE SUSPENDEDTRAN = @FOLIO";
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, sqlConn))
                {
                    command.Parameters.Add("@FOLIO", SqlDbType.NVarChar).Value = SuspendedTransactionId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (sqlConn.State != ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
            }
            return DtResults;
        }

        public string getDeftCust(string storeId)
        {
            string str = null;
            string cmdText = "select c.* from RETAILCHANNELTABLE c inner join RETAILSTORETABLE s on c.RECID = s.RECID where s.Storenumber =  @STOREID ";
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, sqlConn))
                {
                    command.Parameters.Add("@STOREID", SqlDbType.NVarChar, 10).Value = storeId;
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            str = reader["DefaultCustAccount"] as string;
                        }
                    }
                }
            }
            finally
            {
                if (sqlConn.State != ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
            }
            return str;
        }

        public DataTable getSuspendedFolio(string suspendedTransactionId)
        {
            DataTable retval = new DataTable();
            string cmdText = "SELECT TOP 1 [FOLIO],[SALESPERSONNAME] FROM [dbo].[GRWSUSPENDEDTRAN] WHERE SUSPENDEDTRAN = @FOLIO";
            try
            {
                using (SqlCommand command = new SqlCommand(cmdText, sqlConn))
                {
                    command.Parameters.Add("@FOLIO", SqlDbType.NVarChar).Value = suspendedTransactionId;
                    SqlDataAdapter adapt = new SqlDataAdapter(command);
                    adapt.Fill(retval);
                }
            }
            finally
            {
                if (sqlConn.State != ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
            }
            return retval;
        }

        public DataTable getSuspendedTranDetail(string storeId, string terminalid, string transactionId, Int64 lineNumber)
        {
            DataTable retval = new DataTable();
            try
            {
                string cmdText = "SELECT [SALESPERSONID],[SALESPERSONNAME] FROM [dbo].[GRWSUSPENDEDTRANDETAIL] WHERE [TRANSACTIONID] = @TRANSACTIONID AND [TERMINALID] = @TERMINALID AND [STORE] = @STORE AND [LINENUM] = @LINENUM";
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, sqlConn))
                {
                    command.Parameters.Add("@TRANSACTIONID", SqlDbType.NVarChar, 44).Value = transactionId;
                    command.Parameters.Add("@TERMINALID", SqlDbType.NVarChar, 10).Value = terminalid;
                    command.Parameters.Add("@STORE", SqlDbType.NVarChar, 10).Value = storeId;
                    command.Parameters.Add("@LINENUM", SqlDbType.BigInt).Value = lineNumber;
                    SqlDataAdapter adapt = new SqlDataAdapter(command);
                    adapt.Fill(retval);
                }
            }
            finally
            {
                if (sqlConn.State != ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
            }
            return retval;
        }

        public bool insertSuspendedTranDetail(string storeId, string terminalid, string transactionId, string salesPersonId, string salesPersonName, Int64 lineNumber, Int64 channelId)
        {
            try
            {
                string cmdText = "INSERT INTO [dbo].[GRWSUSPENDEDTRANDETAIL]([TRANSACTIONID],[TERMINALID],[STORE],[CHANNELID],[SALESPERSONID],[SALESPERSONNAME],[LINENUM])VALUES(@TRANSACTIONID,@TERMINALID,@STORE, @CHANNELID, @SALESPERSONID, @SALESPERSONNAME, @LINENUM)";
                if (sqlConn.State != ConnectionState.Open)
                {
                    sqlConn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, sqlConn))
                {
                    command.Parameters.Add("@TRANSACTIONID", SqlDbType.NVarChar, 44).Value = transactionId;
                    command.Parameters.Add("@TERMINALID", SqlDbType.NVarChar, 10).Value = terminalid;
                    command.Parameters.Add("@STORE", SqlDbType.NVarChar, 10).Value = storeId;
                    command.Parameters.Add("@CHANNELID", SqlDbType.BigInt).Value = channelId;
                    command.Parameters.Add("@SALESPERSONID", SqlDbType.NVarChar, 100).Value = salesPersonId;
                    command.Parameters.Add("@SALESPERSONNAME", SqlDbType.NVarChar, 200).Value = salesPersonName;
                    command.Parameters.Add("@LINENUM", SqlDbType.BigInt).Value = lineNumber;
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (sqlConn.State != ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
            }
            return true;
        }
    }
}
