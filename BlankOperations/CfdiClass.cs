﻿using Microsoft.Dynamics.Retail.Pos.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Dynamics.Retail.Pos.BlankOperations
{
    class CfdiClass
    {
        private DAC odac;
        private IApplication Appl;

        public IApplication Applict
        {
            get { return Appl; }
            set { Appl = value; }
        }



        public CfdiClass(string _strConn)
        {
            odac = new DAC(_strConn);
        }

        public bool proccessReceiptNbr(string _storeId, string _terminal, string _transaction, string _dataAreaId)
        {
            try
            {
                bool hasCustomer = false, hasInvc = false, hasDetail = false;
                int i = 1;
                string strLine = string.Empty;
                string filepath = Path.GetTempPath();
                string filename = filepath + _storeId + _terminal + _transaction;
                string fullpath = filename + ".txt";
                StreamWriter sWriter = new StreamWriter(fullpath);
                DataTable DtPedimentos = new DataTable();
                DataTable DtData = new DataTable();
                while (i < 3)
                {
                    switch (i)
                    {
                        case 0: DtData = odac.getCustData("", _transaction, _storeId, _terminal, true);
                            if (DtData.Rows.Count > 0)
                                hasCustomer = true;
                            break;
                        case 1: DtData = odac.getInvcData(_storeId, _terminal, _transaction);
                            if (DtData.Rows.Count > 0)
                                hasInvc = true;
                            break;
                        case 2: DtData = odac.getDetailData(_storeId, _terminal, _transaction);
                            if (DtData.Rows.Count > 0)
                                hasDetail = true;
                            break;
                        case 3: DtData = odac.getStoreData(_storeId);
                            break;
                    }
                    
                    int j=1;
                    foreach (DataRow row in DtData.Rows)
                    {
                        strLine = string.Empty;
                        string itemid = string.Empty;
                        foreach (DataColumn col in DtData.Columns)
                        {
                            if (col.ColumnName.Trim().ToLower() != "barcode")
                                strLine = strLine + row[col].ToString() + "|";
                            if(col.ColumnName.Trim().ToLower() == "itemid")
                                    itemid = row[col].ToString();
                        }
                        strLine = strLine.Substring(0, strLine.Length - 1);
                        sWriter.WriteLine(strLine);
                        if(i==2)
                        {
                            if (itemid.Trim() != "")
                            {
                                
                                string[] pedLine = getPedimentosLinea(itemid);
                                foreach (string linePed in pedLine)
                                {
                                    sWriter.WriteLine(linePed);
                                }
                            }
                        }
                        j++;
                    }
                    i++;
                }
                if (hasDetail && hasInvc)
                {
                    sWriter.Close();
                    return sendLayout(_storeId, _terminal, _transaction, fullpath, filename, _dataAreaId);
                }
                else
                {
                    Appl.Services.Dialog.ShowMessage("Error en la estructura del Layout", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
            }
            catch (Exception exc)
            {
                Appl.Services.Dialog.ShowMessage("Error en procesar ticket: " + exc.Message, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }
        }

        private bool sendLayout(string _storeId, string _terminal, string _transaction, string _pathFile, string _nombre, string _dataAreaId)
        {
            string nombrearchivo;
            nombrearchivo = _nombre;
            FileInfo Archivo = new FileInfo(_pathFile);
            //Appl.Services.Dialog.ShowMessage(_pathFile, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            DataTable DtResults = odac.getConfigData(_dataAreaId);
            if (Archivo.Exists && DtResults.Rows.Count > 0)
            {
                string user, psw, url;
                user = DtResults.Rows[0]["user"].ToString();
                psw = DtResults.Rows[0]["psw"].ToString();
                url = DtResults.Rows[0]["url"].ToString();

                AutoFactura.RespuestaRegistroTicket Respuesta = new AutoFactura.RespuestaRegistroTicket();
                AutoFactura.NotificacionAutoFacturacion enviarFact = new AutoFactura.NotificacionAutoFacturacion();
                AutoFactura.Authentication authClass = new AutoFactura.Authentication();
                authClass.User = user;
                authClass.Password = psw;

                enviarFact.Url = url;
                enviarFact.AuthenticationValue = authClass;
                byte[] ContenidoArchivoTexto;
                FileStream f = new FileStream(_pathFile, FileMode.Open, FileAccess.Read);
                int size = (int)f.Length;
                ContenidoArchivoTexto = new byte[size];
                size = f.Read(ContenidoArchivoTexto, 0, size);
                f.Close();

                if (enviarFact.NotificaTicketsFacturacion(ContenidoArchivoTexto))
                {
                    //MessageBox.Show(Respuesta.MensajeError);
                    Respuesta = enviarFact.RespuestaRegistroTicketValue;
                    //FileStream ContenidoGrabar;
                    string strXmlData = "*";
                    string UUid = "*";
                    if (Respuesta.XMLTimbrado != null)
                    {
                        byte[] ContenidoXML = Respuesta.XMLTimbrado;
                        FileStream ContenidoGrabar;

                        ContenidoGrabar = File.OpenWrite(nombrearchivo + ".XML");
                        ContenidoGrabar.Write(ContenidoXML, 0, ContenidoXML.Length);
                        ContenidoGrabar.Close();

                        StreamReader sReader = new StreamReader(nombrearchivo + ".XML");
                        strXmlData = sReader.ReadToEnd();
                    }
                    if (Respuesta.UUIDTimbrado != null && Respuesta.UUIDTimbrado != string.Empty)
                    {
                        UUid = Respuesta.UUIDTimbrado;
                    }
                        
                    odac.saveInvData(_storeId, _terminal, _transaction, UUid, _dataAreaId, strXmlData);
                    return true;
                    //if (Respuesta.RepresentacionPDF != null)
                    //{
                    //    ContenidoGrabar = File.OpenWrite(nombrearchivo + ".PDF");
                    //    ContenidoGrabar.Write(Respuesta.RepresentacionPDF, 0, Respuesta.RepresentacionPDF.Length);
                    //    ContenidoGrabar.Close();
                    //}
                    //MessageBox.Show(string.Format("Archivo Timbrado: UUID: {0}. Monto en Letra: {1}", Respuesta.UUID, Respuesta.LeyendaMoneda));
                }
                else
                {
                    Respuesta = enviarFact.RespuestaRegistroTicketValue;
                    Appl.Services.Dialog.ShowMessage(Respuesta.MensajeError, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        
        private string[] getPedimentosLinea(string _itemId)
        {
            DataTable ped = odac.getPedimentos(_itemId);
            string[] pedimentos = new string[ped.Rows.Count];
            int i = 0;
            foreach (DataRow row in ped.Rows)
            {
                string lineData = row["prefijo"].ToString() + "|";// 01/10/2014
                string fechaAduana = row["fecha"].ToString().Replace("/", "-");
                lineData = lineData + row["pedimento"].ToString() +"|";
                lineData = lineData + fechaAduana + "|";
                lineData = lineData + row["aduana"].ToString();
                lineData = lineData + row["finPed"].ToString();
                pedimentos[i] = lineData;
            }
            return pedimentos;
        }
    }
}
