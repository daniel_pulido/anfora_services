/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


using System.ComponentModel.Composition;
using System.Text;
using System.Windows.Forms;
using LSRetailPosis;
using LSRetailPosis.Settings.FunctionalityProfiles;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.BusinessObjects;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using Microsoft.Dynamics.Retail.Pos.Contracts.Services;
using System;
using System.Data;
using LSRetailPosis.Transaction;
using LSRetailPosis.DataAccess;
using LSRetailPosis.POSProcesses.Properties;
using LSRetailPosis.POSProcesses.WinControls;
using LSRetailPosis.Settings;
using LSRetailPosis.Settings.VisualProfiles;
using Microsoft.Dynamics.Retail.Pos.DataManager;
using Microsoft.Dynamics.Retail.Pos.SystemCore;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Runtime.CompilerServices;
using Microsoft.Dynamics.Retail.Pos.BlankOperations.WinForms;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.Transaction.Line.SalesOrderItem;

namespace Microsoft.Dynamics.Retail.Pos.BlankOperations
{

    [Export(typeof(IBlankOperations))]
    public sealed class BlankOperations : IBlankOperations
    {
        [Import]
        public IApplication Application { get; set; }

        // Get all text through the Translation function in the ApplicationLocalizer
        // TextID's for BlankOperations are reserved at 50700 - 50999

        #region IBlankOperations Members
        /// <summary>
        /// Displays an alert message according operation id passed.
        /// </summary>
        /// <param name="operationInfo"></param>
        /// <param name="posTransaction"></param>        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Grandfather")]
        public void BlankOperation(IBlankOperationInfo operationInfo, IPosTransaction posTransaction)
        {
            const string TransSuspendidas = "suspendform";
            const string CambiarCantidad = "setqty";
            const string Asignacliente = "setcustomer";
            const string SuspendeTransaccion = "suspendtran";
            const string Vendedor = "asignavendedor";
            const string RecuperarTicket = "recuperarticket";
            string opId = operationInfo.OperationId.ToLower();
            string Param = operationInfo.Parameter.ToLower();
            RetailTransaction retailTransaction = null;

            string[] paramss;
            string store;
            string terminal;
            string transactionid;

            switch (opId)
            {
                case "cfdipos":
                    operationInfo.OperationHandled = true;
                    DAC odac = new DAC(Application.Settings.Database.Connection.ConnectionString);
                    #region Tickets sin Datos del cliente

                    CfdiClass ticketsWithoutCustData = new CfdiClass(Application.Settings.Database.Connection.ConnectionString);
                    ticketsWithoutCustData.Applict = Application;
                    if (Param.Trim() == "*")
                    {
                        try
                        {
                            DataTable DtTickets = odac.getTicketsForInvoice(Application.Shift.StoreId);
                            int procesados = 0;
                            foreach (DataRow row in DtTickets.Rows)
                            {
                                store = row["store"].ToString();
                                terminal = row["TERMINAL"].ToString();
                                transactionid = row["TRANSACTIONID"].ToString();
                                if (odac.checkAnticipo(store, terminal, transactionid))
                                {
                                    if (ticketsWithoutCustData.proccessReceiptNbr(store, terminal, transactionid, Application.Settings.Database.DataAreaID))
                                    {
                                        procesados++;
                                    }
                                }

                            }
                            Application.Services.Dialog.ShowMessage("Tickets enviados: " + DtTickets.Rows.Count.ToString() + ", Procesados correctamente: " + procesados, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        catch (Exception exc)
                        {
                            Application.Services.Dialog.ShowMessage(exc.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        //Param = tiend|terminal|transactionid
                        paramss = Param.Split('|');
                        store = paramss[0];
                        terminal = paramss[1];
                        transactionid = paramss[2];
                        try
                        {
                            if (odac.checkAnticipo(store, terminal, transactionid))
                            {
                                if (ticketsWithoutCustData.proccessReceiptNbr(store, terminal, transactionid, Application.Settings.Database.DataAreaID))
                                {
                                    Application.Services.Dialog.ShowMessage("Ticket enviado satisfactoriamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    Application.Services.Dialog.ShowMessage("Error al querer facturar el ticket", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            Application.Services.Dialog.ShowMessage(exc.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    #endregion

                    break;
                case "recsuspen":
                    operationInfo.OperationHandled = true;
                    #region Recupera transacciones suspendidas Obsoleto
                    try
                    {
                        if (posTransaction is RetailTransaction || posTransaction is CustomerOrderTransaction)
                        {
                            Application.Services.Dialog.ShowMessage("Por favor finalice la transacción actual", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        else
                        {
                            string suspendedTranId = string.Empty;
                            Application.Services.Dialog.ShowMessage(Application.BusinessLogic.SuspendRetrieveSystem.GetLastSuspendedTransactionId(posTransaction.StoreId, posTransaction.TerminalId), MessageBoxButtons.OK, MessageBoxIcon.Information);
                            using (frmRecSuspend nfrm = new frmRecSuspend(Application))
                            {
                                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(nfrm);
                                if (nfrm.DialogResult == DialogResult.OK)
                                {
                                    SuspendRetrieveData data = new SuspendRetrieveData(ApplicationSettings.Database.LocalConnection, ApplicationSettings.Database.DATAAREAID);
                                    data.GetSuspendedTransaction(ApplicationSettings.Database.LocalConnection, nfrm.suspendTransId, (Microsoft.Dynamics.Retail.Pos.Contracts.Services.IRounding)PosApplication.Instance.Services.Rounding, ApplicationSettings.Terminal.StoreId, ApplicationSettings.Terminal.StoreCurrency, ApplicationSettings.Terminal.TaxIncludedInPrice);

                                    //Application.RunOperation(PosisOperations.RecallTransaction, nfrm.suspendTransId);
                                }
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        Application.Services.Dialog.ShowMessage(exc.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    #endregion
                    break;

                case TransSuspendidas:
                    operationInfo.OperationHandled = true;
                    #region Recupera transacciones suspendidas
                    using (frmSuspTran suspendform = new frmSuspTran(Application))
                    {
                        LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(suspendform);
                    }
                    #endregion
                    break;

                case CambiarCantidad:
                    operationInfo.OperationHandled = true;
                    #region CambiarCantidad
                    retailTransaction = posTransaction as RetailTransaction;
                    string[] Parametros = operationInfo.Parameter.Split('-');
                    int noline;
                    decimal qty;
                    string salespersonid;
                    string salespersonname;
                    int.TryParse(Parametros[0], out noline);
                    decimal.TryParse(Parametros[1], out qty);
                    salespersonid = Parametros[2];
                    salespersonname = Parametros[3];
                    SaleLineItem sli = retailTransaction.GetItem(noline);
                    sli.Quantity = qty;
                    sli.QuantityOrdered = qty;
                    sli.SalesPersonId = salespersonid;
                    sli.SalespersonName = salespersonname;
                    retailTransaction.CalcTotals();

                    break;
                    #endregion

                case Asignacliente:
                    operationInfo.OperationHandled = true;
                    #region Asignacliente
                    RetailTransaction retailtrans = posTransaction as RetailTransaction;
                    Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity.ICustomer cust = Application.BusinessLogic.CustomerSystem.GetCustomerInfo(operationInfo.Parameter);
                    retailtrans.Customer = (LSRetailPosis.Transaction.Customer)cust;
                    Application.RunOperation(PosisOperations.Customer, retailtrans.Customer.CustomerId);
                    retailtrans.Save();

                    break;
                    #endregion

                case Vendedor:
                    operationInfo.OperationHandled = true;
                    #region Vendedor
                    RetailTransaction retailtransac = posTransaction as RetailTransaction;
                    if (retailtransac.SaleItems.Count > 0)
                    {
                        using (LSRetailPosis.POSProcesses.frmSalesPerson dialog = new LSRetailPosis.POSProcesses.frmSalesPerson())
                        {
                            LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
                            if (dialog.DialogResult == DialogResult.OK)
                            {
                                if (!string.IsNullOrEmpty(dialog.SelectedEmployeeId))
                                {
                                    foreach (SaleLineItem sl in retailtransac.SaleItems)
                                    {
                                        if (string.IsNullOrEmpty(sl.SalesPersonId))
                                        {
                                            sl.SalesPersonId = dialog.SelectedEmployeeId;
                                            sl.SalespersonName = dialog.SelectEmployeeName;
                                        }
                                    }
                                    retailtransac.SalesPersonId = dialog.SelectedEmployeeId;
                                    retailtransac.SalesPersonName = dialog.SelectEmployeeName;
                                }
                            }
                        }
                    }
                    break;
                    #endregion

                case SuspendeTransaccion:
                    operationInfo.OperationHandled = true;
                    #region SuspendeTransaccion
                    retailTransaction = posTransaction as RetailTransaction;
                    retailTransaction.Comment = "suspendetransaccion";
                    //System.Collections.Generic.LinkedList<SaleLineItem> newTenderList = new System.Collections.Generic.LinkedList<SaleLineItem>();
                    //foreach (SaleLineItem oldSaleitem in retailTransaction.SaleItems)
                    //{
                    //    if (!oldSaleitem.Voided)
                    //    {
                    //        newTenderList.AddLast(oldSaleitem);
                    //    }
                    //}
                    //retailTransaction.SaleItems.Clear();
                    //foreach (SaleLineItem pasteTender in newTenderList)
                    //{
                    //    retailTransaction.Add(pasteTender);
                    //}
                    //retailTransaction.CalcTotals();
                    //retailTransaction.CalculateAmountDue();

                    foreach (SaleLineItem sl in retailTransaction.SaleItems)
                    {
                        try
                        {
                            DAC oDac = new DAC(Application.Settings.Database.Connection.ConnectionString);
                            oDac.insertSuspendedTranDetail(retailTransaction.StoreId, retailTransaction.TerminalId, retailTransaction.TransactionId, sl.SalesPersonId, sl.SalespersonName, sl.LineId, retailTransaction.ChannelId);
                        }
                        catch (Exception exc)
                        {
                            Application.Services.Dialog.ShowMessage("Error al guardar el vendedor de la linea" + exc.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    retailTransaction.Save();
                    posTransaction = Application.RunOperation(PosisOperations.SuspendTransaction, posTransaction.TransactionId, posTransaction);
                    break;
                    #endregion

                case RecuperarTicket:
                    operationInfo.OperationHandled = true;
                    #region RecuperaTicket
                    if (!(posTransaction is RetailTransaction))
                    {
                        posTransaction = new RetailTransaction(posTransaction.StoreId, ApplicationSettings.Terminal.StoreCurrency, ApplicationSettings.Terminal.TaxIncludedInPrice, Application.Services.Rounding);
                        Application.BusinessLogic.TransactionSystem.LoadTransactionStatus(posTransaction);
                    }

                    RetailTransaction[] arrayRetTran = new RetailTransaction[0];
                    RetailTransaction finalTransaction = posTransaction as RetailTransaction;
                    using (Microsoft.Dynamics.Retail.Pos.BlankOperations.WinForms.frmJournalSearch dialog = new Microsoft.Dynamics.Retail.Pos.BlankOperations.WinForms.frmJournalSearch())
                    {
                        dialog.App = this.Application;
                        LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
                        if (dialog.DialogResult == DialogResult.OK)
                        {
                            arrayRetTran = dialog.retailTransactionArr;
                        }
                    }
                    int lineid = finalTransaction.SaleItems.Count + 1;
                    for (int i = 0; i < arrayRetTran.Length; i++)
                    {
                        retailTransaction = arrayRetTran[i] as RetailTransaction;
                        if (retailTransaction != null)
                        {
                            foreach (SaleLineItem sale in retailTransaction.SaleItems)
                            {
                                Application.RunOperation(PosisOperations.ItemSale, sale);
                                Application.RunOperation(PosisOperations.BlankOperation, "setqty" + ";" + lineid.ToString() + "-" + sale.Quantity.ToString() + "-" + sale.SalesPersonId + "-" + sale.SalespersonName);
                                lineid++;
                            }
                            if (retailTransaction.Customer.CustomerId != null)
                            {
                                Application.RunOperation(PosisOperations.BlankOperation, "setcustomer" + ";" + retailTransaction.Customer.CustomerId);
                            }
                        }
                    }
                    // Calculating all totals in the tranaction.
                    finalTransaction.CalcTotals();
                    finalTransaction.Save();

                    break;
                    #endregion

                default:
                    break;
            }
        }
        #endregion

    }
}
