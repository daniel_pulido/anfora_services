/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using System;
using System.Windows.Forms;
using LSRetailPosis.POSProcesses;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using LSRetailPosis.Transaction;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Dynamics.Retail.Pos.Contracts.Services;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using LSRetailPosis.Settings;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.Transaction.Line.TenderItem;
using LSRetailPosis;
using LSRetailPosis.DataAccess;
using System.Drawing;
using Microsoft.Dynamics.Retail.Pos.SystemCore;

namespace Microsoft.Dynamics.Retail.Pos.BlankOperations.WinForms
{
    public partial class frmJournalSearch : frmTouchBase
    {

        private string selectedTransactionId;
        public IApplication App;
        public RetailTransaction retailTransaction;
        public RetailTransaction[] retailTransactionArr;
        private string transaccions;
        public string[] transId;

        /// <summary>
        /// Returns selectedtransactionid as string.
        /// </summary>
        public string SelectedTransactionId
        {
            get { return selectedTransactionId; }
        }

        /// <summary>
        /// Gives all details from database for search.
        /// </summary>
        public frmJournalSearch()
        {
            //
            // Get all text through the Translation function in the ApplicationLocalizer
            //
            // TextID's for frmJournalSearch are reserved at 2450 - 2499 
            // The last id in use is: 2456
            //
            InitializeComponent();
            try
            {
                // Translate all components...

                lblHeader.Text = LSRetailPosis.ApplicationLocalizer.Language.Translate(2457);                       // Journal search

            }
            catch (Exception)
            {
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void frmJournalSearch_Load(object sender, EventArgs e)
        {
            SetFormFocus();
        }

        private void SetFormFocus()
        {
            txtTransactionId.Focus();
        }

        private void txtTransactionId_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                simpleButton1_Click(this, new EventArgs());
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtTransactionId.Text = string.Empty;
            SetFormFocus();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                DAC odac = new DAC(ApplicationSettings.Database.LocalConnectionString);
                IPosTransaction transaction;
                transaccions += txtTransactionId.Text;
                txtTransactionId.ResetText();
                transId = transaccions.Split(';');
                retailTransactionArr = new RetailTransaction[transId.Length];
                for (int i = 0; i < transId.Length; i++)
                {
                    selectedTransactionId = transId[i];
                    DataTable DtRetVal = new DataTable();
                    DtRetVal = odac.getTransactionSuspended(selectedTransactionId);
                    if (DtRetVal.Rows.Count > 0)
                    {
                        string salesPersonId = string.Empty;
                        string salesPersonName = string.Empty;
                        selectedTransactionId = DtRetVal.Rows[0]["TRANSACTIONID"].ToString();
                        salesPersonId = DtRetVal.Rows[0]["SALESPERSONID"].ToString();
                        salesPersonName = DtRetVal.Rows[0]["SALESPERSONNAME"].ToString();
                        transaction = RetrieveTransaction(selectedTransactionId, salesPersonId, salesPersonName);
                        retailTransactionArr[i] = transaction as RetailTransaction;
                    }
                }
                this.DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                App.Services.Dialog.ShowMessage(ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Cancel;
                Close();
            }
        }

        private PosTransaction LoadTransaction(RetailTransaction ret)
        {
            TransactionData data = new TransactionData(App.Settings.Database.Connection, App.Settings.Database.DataAreaID, App);
            return data.GetOriginalRetailTransaction(ret);
        }

        private IPosTransaction RetrieveTransaction(string suspendedTransactionId, string salesPersonId, string salesPersonName)
        {
            IPosTransaction transaction2;
            try
            {
                PosTransaction transaction = null;
                transaction = (PosTransaction)App.BusinessLogic.SuspendRetrieveSystem.RetrieveTransaction(suspendedTransactionId);
                if (transaction != null)
                {
                    transaction.TransactionId = new TransactionData(this.App.Settings.Database.Connection, this.App.Settings.Database.DataAreaID, (IApplication)this.App).GetNextTransactionId(transaction.StoreId, transaction.TerminalId, transaction.TransactionIdNumberSequence);
                    transaction.EntryStatus = PosTransaction.TransactionStatus.Normal;
                    transaction.Shift = (IPosBatchStaging)App.Shift;
                    transaction.TerminalId = App.Shift.TerminalId;
                    transaction.OperatorId = ApplicationSettings.Terminal.TerminalOperator.OperatorId;
                    transaction.OperatorName = ApplicationSettings.Terminal.TerminalOperator.Name;
                    transaction.OperatorNameOnReceipt = ApplicationSettings.Terminal.TerminalOperator.NameOnReceipt;
                    transaction.BeginDateTime = DateTime.Now;
                    transaction.EndDateTime = DateTime.MinValue;
                    foreach (SaleLineItem item in ((RetailTransaction)transaction).SaleItems)
                    {
                        item.BeginDateTime = transaction.BeginDateTime;
                        App.BusinessLogic.CustomerSystem.ResetCustomerTaxGroup((ITaxableItem)item);
                        try
                        {
                            DAC odac = new DAC(ApplicationSettings.Database.LocalConnectionString);
                            DataTable DtRetVal = new DataTable();
                            DtRetVal = odac.getSuspendedTranDetail(transaction.StoreId, transaction.TerminalId, suspendedTransactionId, item.LineId);
                            if (DtRetVal.Rows.Count > 0)
                            {
                                item.SalesPersonId = DtRetVal.Rows[0]["SALESPERSONID"].ToString();
                                item.SalespersonName = DtRetVal.Rows[0]["SALESPERSONNAME"].ToString();
                            }
                        }
                        catch
                        {
                            continue;
                        }
                        if (item.Voided)
                            item.Voided = true;
                    }
                    foreach (TenderLineItem item2 in ((RetailTransaction)transaction).TenderLines)
                    {
                        item2.BeginDateTime = transaction.BeginDateTime;
                    }
                    ((RetailTransaction)transaction).ClearDiscountCache();
                    App.BusinessLogic.ItemSystem.RecalcPriceTaxDiscount((IPosTransaction)transaction, true);
                    ((RetailTransaction)transaction).CalcTotals();
                }
                ClearSuspendedTransaction(ApplicationSettings.Database.LocalConnection, suspendedTransactionId);
                ClearSuspendedTransactionDetail(ApplicationSettings.Database.LocalConnection, suspendedTransactionId);
                transaction2 = (IPosTransaction)transaction;
            }
            catch (PosisException exception)
            {
                ApplicationExceptionHandler.HandleException(this.ToString(), exception);
                throw;
            }
            catch (Exception exception2)
            {
                ApplicationExceptionHandler.HandleException(this.ToString(), exception2);
                throw;
            }
            return transaction2;
        }
        
        public void ClearSuspendedTransaction(SqlConnection connection, string suspendedTransactionID)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                string cmdText = "DELETE FROM dbo.GRWSUSPENDEDTRAN WHERE TRANSACTIONID = @TRANSACTIONID";
                using (SqlCommand command = new SqlCommand(cmdText, connection))
                {
                    command.Parameters.AddWithValue("@TRANSACTIONID", suspendedTransactionID.Trim());
                    command.ExecuteNonQuery();
                }
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }

        public void ClearSuspendedTransactionDetail(SqlConnection connection, string suspendedTransactionID)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                string cmdText = "DELETE FROM dbo.GRWSUSPENDEDTRANDETAIL WHERE TRANSACTIONID = @TRANSACTIONID";
                using (SqlCommand command = new SqlCommand(cmdText, connection))
                {
                    command.Parameters.AddWithValue("@TRANSACTIONID", suspendedTransactionID.Trim());
                    command.ExecuteNonQuery();
                }
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }

        private void txtTransactionId_EditValueChanged(object sender, EventArgs e)
        {
            /*transaccions += txtTransactionId.Text + ";";
            txtTransactionId.ResetText();
            txtTransactionId.Focus();*/
        }
    }
}
