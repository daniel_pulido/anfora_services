﻿using LSRetailPosis;
using LSRetailPosis.DataAccess;
using LSRetailPosis.POSProcesses;
using LSRetailPosis.POSProcesses.WinControls;
using LSRetailPosis.Settings;
using LSRetailPosis.Transaction;
using LSRetailPosis.Transaction.Line.Discount;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.Transaction.Line.TenderItem;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using Microsoft.Dynamics.Retail.Pos.Contracts.Services;
using Microsoft.Dynamics.Retail.Pos.DataManager;
using Microsoft.Dynamics.Retail.Pos.SystemCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Microsoft.Dynamics.Retail.Pos.BlankOperations.WinForms
{
    public partial class frmSuspTran : frmTouchBase
    {
        public DataTable ItemTable { get; private set; }
        private string selectedTransactionId;
        public IApplication App;
        public RetailTransaction retailTransaction;
        public RetailTransaction[] retailTransactionArr;
        private string transaccions;
        public string[] transId;
        private bool lastPageReached;
        private int nextPage = 1;
        private bool sortAsc = true;
        private string sortBy = "TRANSDATE";
        DataTable suspendedTransactionResultList;
        //private SuspendedTransactionDataManager suspendedTransactionDataManager = new SuspendedTransactionDataManager(ApplicationSettings.Database.LocalConnection, ApplicationSettings.Database.DATAAREAID);
        private int selectedItemIndex;
        private string atSign = "@";
           StringBuilder reportLayout;
        private const int paperWidth = 55;
        private static readonly string singleLine = string.Empty.PadLeft(paperWidth, '-');
        private static readonly string lineFormat = ApplicationLocalizer.Language.Translate(7060);
        private static readonly string currencyFormat = ApplicationLocalizer.Language.Translate(7061);
        private static readonly string typeFormat = ApplicationLocalizer.Language.Translate(7062);

        public frmSuspTran(IApplication _app)
        {
            App = _app;
            InitializeComponent();
        }

        private PosTransaction LoadTransaction(RetailTransaction ret)
        {
            TransactionData data = new TransactionData(App.Settings.Database.Connection, App.Settings.Database.DataAreaID, App);
            return data.GetOriginalRetailTransaction(ret);
        }

        private IPosTransaction RetrieveTransaction(string suspendedTransactionId, string salesPersonId, string salesPersonName)
        {
            IPosTransaction transaction2;
            try
            {
                PosTransaction transaction = null;
                //App.BusinessLogic.SuspendRetrieveSystem.RetrieveTransaction(suspendedTransactionId);
                //transaction = GetSuspendedTransaction(App.Settings.Database.Connection, suspendedTransactionId, (IRounding)this.App.Services.Rounding);
                transaction = (PosTransaction)App.BusinessLogic.SuspendRetrieveSystem.RetrieveTransaction(suspendedTransactionId);

                if (transaction != null)
                {
                    transaction.TransactionId = new TransactionData(this.App.Settings.Database.Connection, this.App.Settings.Database.DataAreaID, (IApplication)this.App).GetNextTransactionId(transaction.StoreId, transaction.TerminalId, transaction.TransactionIdNumberSequence);
                    transaction.EntryStatus = PosTransaction.TransactionStatus.Normal;
                    transaction.Shift = (IPosBatchStaging)App.Shift;
                    transaction.TerminalId = App.Shift.TerminalId;
                    transaction.OperatorId = ApplicationSettings.Terminal.TerminalOperator.OperatorId;
                    transaction.OperatorName = ApplicationSettings.Terminal.TerminalOperator.Name;
                    transaction.OperatorNameOnReceipt = ApplicationSettings.Terminal.TerminalOperator.NameOnReceipt;
                    transaction.BeginDateTime = DateTime.Now;
                    transaction.EndDateTime = DateTime.MinValue;
                    foreach (SaleLineItem item in ((RetailTransaction)transaction).SaleItems)
                    {
                        item.BeginDateTime = transaction.BeginDateTime;
                        App.BusinessLogic.CustomerSystem.ResetCustomerTaxGroup((ITaxableItem)item);
                        item.SalesPersonId = salesPersonId;
                        item.SalespersonName = salesPersonName;
                        if (item.Voided)
                            item.Voided = true;
                    }
                    foreach (TenderLineItem item2 in ((RetailTransaction)transaction).TenderLines)
                    {
                        item2.BeginDateTime = transaction.BeginDateTime;
                    }
                    ((RetailTransaction)transaction).ClearDiscountCache();
                    App.BusinessLogic.ItemSystem.RecalcPriceTaxDiscount((IPosTransaction)transaction, true);
                    ((RetailTransaction)transaction).CalcTotals();
                }
                //ClearSuspendedTransaction(ApplicationSettings.Database.LocalConnection, suspendedTransactionId);
                transaction2 = (IPosTransaction)transaction;
            }
            catch (PosisException exception)
            {
                ApplicationExceptionHandler.HandleException(this.ToString(), exception);
                throw;
            }
            catch (Exception exception2)
            {
                ApplicationExceptionHandler.HandleException(this.ToString(), exception2);
                throw;
            }
            return transaction2;
        }
        public void ClearSuspendedTransaction(SqlConnection connection, string suspendedTransactionID)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                string cmdText = "DELETE FROM dbo.RETAILSUSPENDEDTRANSACTIONS WHERE SUSPENDEDTRANSACTIONID = @TRANSACTIONID";
                using (SqlCommand command = new SqlCommand(cmdText, connection))
                {
                    command.Parameters.AddWithValue("@TRANSACTIONID", suspendedTransactionID.Trim());
                    command.ExecuteNonQuery();
                }
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }
        private PosTransaction GetSuspendedTransaction(SqlConnection connection, string SuspendedTransactionId, IRounding rounding)
        {
            PosTransaction transaction2;
            try
            {
                PosTransaction transaction = null;
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                string cmdText = "SELECT BYTELENGTH, TRANSACTIONDATA FROM dbo.RETAILSUSPENDEDTRANSACTIONS  WHERE SUSPENDEDTRANSACTIONID = @TRANSACTIONID";
                using (SqlCommand command = new SqlCommand(cmdText, connection))
                {
                    command.Parameters.AddWithValue("@TRANSACTIONID", SuspendedTransactionId);
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.SequentialAccess))
                    {
                        int length = 0;
                        long dataOffset = 0L;
                        while (reader.Read())
                        {
                            length = reader.GetInt32(0);
                            byte[] buffer = new byte[length];
                            using (MemoryStream streams = null)
                            {
                                MemoryStream stream = new MemoryStream();
                                using (BinaryWriter writer = new BinaryWriter(stream))
                                {
                                    MemoryStream serializationStream = stream;
                                    stream = null;
                                    BinaryFormatter formatter = new BinaryFormatter();
                                    dataOffset = 0L;
                                    reader.GetBytes(1, dataOffset, buffer, 0, length);
                                    writer.Write(buffer);
                                    serializationStream.Position = 0L;
                                    transaction = (PosTransaction)formatter.Deserialize(serializationStream);
                                }
                                continue;
                            }
                        }
                    }
                }
                if (transaction != null)
                {
                    transaction.InitializeRounding((IRounding)rounding);
                }
                transaction2 = transaction;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            return transaction2;
        }
        private void GetSuspendedTransactionResultList(string searchValue, int fromRow, int numberOfRows, string sortByColumn, bool sortAscending, bool mergeList)
        {
            DAC odac = new DAC(ApplicationSettings.Database.LocalConnectionString);
            try
            {
                base.UseWaitCursor = true;
                if (!this.lastPageReached)
                {
                    DataTable list = PosApplication.Instance.BusinessLogic.SuspendRetrieveSystem.RetrieveTransactionList(ApplicationSettings.Terminal.StoreId);
                    
                    list.Columns.Add(new DataColumn("FOLIO",typeof(string)));
                    list.Columns.Add(new DataColumn("SALESPERSONNAME", typeof(string)));
                    foreach(DataRow dr in list.Rows)
                    {
                        DataTable dra = odac.getSuspendedFolio(dr["SUSPENDEDTRANSACTIONID"].ToString());
                        dr["FOLIO"] = Convert.ToInt64(dra.Rows[0]["FOLIO"].ToString()).ToString("000000000000");
                        dr["SALESPERSONNAME"] = dra.Rows[0]["SALESPERSONNAME"].ToString();
                    }
                    //foreach (DataColumn dc in list.Columns)
                    //{
                    //    MessageBox.Show(dc.ColumnName);
                    //}
                    //IList<SuspendedTransactionSearchResult> list = suspendedTransactionDataManager.GetSuspendedTransactionList(searchValue, fromRow, numberOfRows, sortByColumn, sortAscending);
                    if (((list == null) || (list.Rows.Count == 0)) || (list.Rows.Count < numberOfRows))
                    {
                        this.lastPageReached = true;
                    }
                    //if (mergeList)
                    //{
                    //    foreach (SuspendedTransactionSearchResult result in list)
                    //    {
                    //        this.suspendedTransactionResultList.Add(result);
                    //    }
                    //}
                    //else
                    //{
                    this.suspendedTransactionResultList = list;
                    //}
                }
            }
            catch (Exception)
            {
                this.suspendedTransactionResultList = null;
                this.gridControl1.DataSource = this.suspendedTransactionResultList;
                this.gridControl1.RefreshDataSource();
                this.CheckRowPosition();
                throw;
            }
            finally
            {
                base.UseWaitCursor = false;
            }
        }

        private void CheckRowPosition()
        {

            this.btnDown.Enabled = !this.gridView1.IsLastRow;
            this.btnUp.Enabled = !this.gridView1.IsFirstRow;
        }

        public LinkedList<string> SuspendedTransactionIdList { get; private set; }

        private static PosTransaction LoadTransaction(string transactionId)
        {
            SuspendRetrieveData data = new SuspendRetrieveData(ApplicationSettings.Database.LocalConnection, ApplicationSettings.Database.DATAAREAID);
            return data.GetSuspendedTransaction(ApplicationSettings.Database.LocalConnection, transactionId, (Microsoft.Dynamics.Retail.Pos.Contracts.Services.IRounding) PosApplication.Instance.Services.Rounding, ApplicationSettings.Terminal.StoreId, ApplicationSettings.Terminal.StoreCurrency, ApplicationSettings.Terminal.TaxIncludedInPrice);
            //SuspendRetrieveData data = new SuspendRetrieveData(ApplicationSettings.Database.LocalConnection, ApplicationSettings.Database.DATAAREAID);
            //return data.GetSuspendedTransaction(ApplicationSettings.Database.LocalConnection, transactionId, (Microsoft.Dynamics.Retail.Pos.Contracts.Services.IRounding)PosApplication.Instance.Services.Rounding, ApplicationSettings.Terminal.StoreId, ApplicationSettings.Terminal.StoreCurrency, ApplicationSettings.Terminal.TaxIncludedInPrice);
        }

        private void frmSuspTran_Load(object sender, EventArgs e)
        {
            this.CreateItemTable();
            GetSuspendedTransactionResultList(string.Empty, this.nextPage, 200, this.sortBy, this.sortAsc, false);
            this.gridControl1.DataSource = this.suspendedTransactionResultList;
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            try
            {
                if (this.gridView1.RowCount > 0)
                {
                    int num = this.gridView1.GetSelectedRows()[0];
                    this.selectedTransactionId = this.suspendedTransactionResultList.Rows[num]["SUSPENDEDTRANSACTIONID"].ToString();
                    PosTransaction transaction = LoadTransaction(this.selectedTransactionId);
                    
                    if (transaction.IRounding != null)
                    {
                        ShowTransaction(transaction);
                    }
                }
                //else
                //{
                //    this.receipt1.ShowTransaction();
                //    this.DisplayCustomerInfo(transaction);
                //}
            }
            catch (Exception exception)
            {
                ApplicationExceptionHandler.HandleException(this.ToString(), exception);
            }
        }

        public void DisplayRTItems(PosTransaction posTransaction)
        {
            this.ItemTable.Clear();
            //this.lblOrderMode.Text = this.GetOrderModeText(posTransaction);
            UnitOfMeasureData unitOfMeasureData = new UnitOfMeasureData(PosApplication.Instance.Settings.Database.Connection, PosApplication.Instance.Settings.Database.DataAreaID, ApplicationSettings.Terminal.StorePrimaryId, (Microsoft.Dynamics.Retail.Pos.Contracts.IApplication)PosApplication.Instance);
            RetailTransaction transaction = posTransaction as RetailTransaction;
            
            try
            {
                if (posTransaction != null)
                {
                   
                    foreach (SaleLineItem item in transaction.SaleItems)
                    {
                        
                        Dimensions dimension = item.Dimension;
                        DataRow row = this.ItemTable.NewRow();
                        row["BARCODEID"] = item.BarcodeId;
                        row["ITEMID"] = item.ItemId;
                        row["ITEMNAME"] = item.Description;
                        row["ITEMNAMEALIAS"] = item.DescriptionAlias;
                        row["AMOUNT"] = posTransaction.IRounding.Round(item.Price, false);
                        row["QUANTITY"] = posTransaction.IRounding.Round(item.Quantity, unitOfMeasureData.GetUnitDecimals(item.SalesOrderUnitOfMeasure));
                        row["TOTALAMOUNT"] = posTransaction.IRounding.Round(item.NetAmountWithTax, false);
                        row["TOTALAMOUNTWITHOUTTAX"] = posTransaction.IRounding.Round(item.NetAmountWithNoTax, false);
                        row["TAXCODE"] = item.TaxGroupId;
                        row["TAXAMOUNT"] = posTransaction.IRounding.Round(item.TaxAmount, false);
                        row["TAXRATEPCT"] = posTransaction.IRounding.Round(item.TaxRatePct, false);
                        row["VOIDED"] = item.Voided;
                        row["COMMENT"] = string.IsNullOrEmpty(item.CommentLocalizedForWorker) ? (item.Comment ?? string.Empty) : item.CommentLocalizedForWorker;
                        row["LINEID"] = item.LineId;
                        row["ORIGINALPRICEWITHTAX"] = posTransaction.IRounding.Round(item.OriginalPrice, false);
                        row["STANDARDRETAILPRICEWITHTAX"] = posTransaction.IRounding.Round(item.StandardRetailPrice, false);
                        row["COLORNAME"] = (dimension != null) ? dimension.ColorName : string.Empty;
                        row["SIZENAME"] = (dimension != null) ? dimension.SizeName : string.Empty;
                        row["STYLENAME"] = (dimension != null) ? dimension.StyleName : string.Empty;
                        row["CONFIGNAME"] = (dimension != null) ? dimension.ConfigName : string.Empty;
                        row["OFFERID"] = item.TotalPeriodicOfferId;
                        row["LINKEDITEM"] = item.IsLinkedItem;
                        row["INFOCODEITEM"] = item.IsInfoCodeItem;
                        row["CORPORATECARD"] = !string.IsNullOrEmpty(item.TenderRestrictionId);
                        row["SHOULDBEMANUALLYREMOVED"] = item.ShouldBeManuallyRemoved;
                        row["SERIALID"] = item.SerialId ?? string.Empty;
                        row["FOUND"] = item.Found;
                        row["ACTIVATED"] = item.DateToActivateItem <= DateTime.Now;
                        row["RFID"] = item.RFIDTagId;
                        row["BLOCKED"] = item.Blocked;
                        row["UOM"] = item.SalesOrderUnitOfMeasure;
                        row["SELECTED"] = false;
                        if (item.PaymentIndex >= 0)
                        {
                            row["PAYMENTINDEX"] = item.PaymentIndex.ToString();
                            foreach (TenderLineItem item2 in transaction.TenderLines)
                            {
                                if (item2.LineId == (item.PaymentIndex + 1))
                                {
                                    row["PAYMENTNAME"] = item2.Description;
                                }
                            }
                        }
                        else
                        {
                            row["PAYMENTINDEX"] = string.Empty;
                            row["PAYMENTNAME"] = string.Empty;
                        }
                        row["ITEMGROUP"] = item.ItemGroupId ?? string.Empty;
                        row["BATCHID"] = item.BatchId ?? string.Empty;
                        row["BATCHEXPDATE"] = (item.BatchExpDate == DateTime.MinValue) ? string.Empty : item.BatchExpDate.ToShortDateString();
                        if (!((bool)row["ACTIVATED"]))
                        {
                            DataRow row2;
                            (row2 = row)["COMMENT"] = row2["COMMENT"] + Environment.NewLine + string.Format(ApplicationLocalizer.Language.Translate(105), item.DateToActivateItem.ToString("D"));
                        }
                        else if (item.Blocked)
                        {
                            DataRow row3;
                            (row3 = row)["COMMENT"] = row3["COMMENT"] + Environment.NewLine + ApplicationLocalizer.Language.Translate(106);
                        }
                        else if (!item.Found)
                        {
                            DataRow row4;
                            row["ITEMNAME"] = item.ItemId;
                            (row4 = row)["COMMENT"] = row4["COMMENT"] + Environment.NewLine + string.Format(ApplicationLocalizer.Language.Translate(107), item.ItemId);
                        }
                        else if (!item.Voided)
                        {
                            DataRow row5;
                            (row5 = row)["COMMENT"] = row5["COMMENT"] + this.GetRTComments(posTransaction, unitOfMeasureData, item);
                        }
                        //if (this.columnCustomizer != null)
                        //{
                        //    this.columnCustomizer.FillColumns(row, item, posTransaction);
                        //}
                        row["PROHIBITRETURN_RU"] = item.ProhibitReturn;
                        this.ItemTable.Rows.Add(row);
                    }
                }
                else
                {
                    this.selectedItemIndex = -1;
                }
                this.gridView2.MoveLast();
                this.selectedItemIndex = this.gridView2.RowCount;
                this.SelectRow(this.selectedItemIndex);
            }
            catch (Exception exception)
            {
                //MessageBox.Show(exception.Message);
                ApplicationExceptionHandler.HandleException(this.ToString(), exception);
            }
        }

        private void SelectRow(int rowIndex)
        {
            if (this.ItemTable.Rows.Count > rowIndex)
            {
                this.ItemTable.Rows[rowIndex]["SELECTED"] = true;
            }
        }
        //private string GetRTComments(PosTransaction posTransaction, UnitOfMeasureData unitOfMeasureData, SaleLineItem saleItem)
        //{
        //    string str = string.Empty;
        //    IDictionary<string, string> previewComments = new Dictionary<string, string>();
        //    if (Math.Abs(saleItem.Quantity) != 1M)
        //    {
        //        string str2 = posTransaction.IRounding.RoundForReceipt(posTransaction.IRounding.Round(saleItem.Quantity, unitOfMeasureData.GetUnitDecimals(saleItem.SalesOrderUnitOfMeasure)), unitOfMeasureData.GetUnitDecimals(saleItem.SalesOrderUnitOfMeasure));
        //        previewComments["Quantity"] = Environment.NewLine + string.Format(ApplicationLocalizer.Language.Translate(108), new object[] { str2, saleItem.Description, this.atSign, posTransaction.IRounding.Round(saleItem.Price, false) });
        //    }
        //    if (saleItem.PriceOverridden)
        //    {
        //        previewComments["PriceOverridden"] = Environment.NewLine + string.Format(ApplicationLocalizer.Language.Translate(6039), posTransaction.IRounding.Round(saleItem.OriginalPrice, false));
        //    }
        //    previewComments["Dimension"] = GetDimensionComment(saleItem.Dimension);
        //    previewComments["Discount"] = GetDiscountComment(posTransaction, saleItem);
        //    if (!string.IsNullOrEmpty(saleItem.SalesPersonId))
        //    {
        //        previewComments["SalesPerson"] = Environment.NewLine + saleItem.SalesPersonId + " - " + saleItem.SalespersonName;
        //    }
        //    if (!string.IsNullOrEmpty(saleItem.SerialId))
        //    {
        //        previewComments["SerialId"] = Environment.NewLine + saleItem.SerialId;
        //    }
        //    if (!string.IsNullOrEmpty(str) && str.StartsWith(Environment.NewLine))
        //    {
        //        str = str.Remove(0, Environment.NewLine.Length);
        //    }
        //    //previewComments = "";// PosApplication.Instance.Services.CustomField.PopulateItemReceiptPreviewText(previewComments, (Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity.IPosTransaction)posTransaction, (Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity.ISaleLineItem)saleItem);
        //    //if (previewComments.ContainsKey("Quantity"))
        //    //{
        //    //    str = str + previewComments["Quantity"];
        //    //}
        //    //if (previewComments.ContainsKey("PriceOverridden"))
        //    //{
        //    //    str = str + previewComments["PriceOverridden"];
        //    //}
        //    //str = str + previewComments["Dimension"] + previewComments["Discount"];
        //    //if (previewComments.ContainsKey("SalesPerson"))
        //    //{
        //    //    str = str + previewComments["SalesPerson"];
        //    //}
        //    //if (previewComments.ContainsKey("SerialId"))
        //    //{
        //    //    str = str + previewComments["SerialId"];
        //    //}
        //    return str;
        //}
        private string GetRTComments(PosTransaction posTransaction, UnitOfMeasureData unitOfMeasureData, SaleLineItem saleItem)
        {
            string str = string.Empty;
            if (Math.Abs(saleItem.Quantity) != 1M)
            {
                string str2 = posTransaction.IRounding.RoundForReceipt(posTransaction.IRounding.Round(saleItem.Quantity, unitOfMeasureData.GetUnitDecimals(saleItem.SalesOrderUnitOfMeasure)), unitOfMeasureData.GetUnitDecimals(saleItem.SalesOrderUnitOfMeasure));
                str = str + Environment.NewLine + string.Format(ApplicationLocalizer.Language.Translate(0x6c), new object[] { str2, saleItem.Description, this.atSign, posTransaction.IRounding.Round(saleItem.Price, false) });
            }
            if (saleItem.PriceOverridden)
            {
                str = str + Environment.NewLine + string.Format(ApplicationLocalizer.Language.Translate(0x1797), posTransaction.IRounding.Round(saleItem.OriginalPrice, false));
            }
            str = str + GetDimensionComment(saleItem.Dimension) + GetDiscountComment(posTransaction, saleItem);
            if (!string.IsNullOrEmpty(saleItem.SalesPersonId))
            {
                string str3 = str;
                str = str3 + Environment.NewLine + saleItem.SalesPersonId + " - " + saleItem.SalespersonName;
            }
            if (!string.IsNullOrEmpty(saleItem.SerialId))
            {
                str = str + Environment.NewLine + saleItem.SerialId;
            }
            if (!string.IsNullOrEmpty(str) && str.StartsWith(Environment.NewLine))
            {
                str = str.Remove(0, Environment.NewLine.Length);
            }
            return str;
        }
        private void CreateItemTable()
        {
            this.ItemTable = new DataTable();
            this.ItemTable.TableName = "ITEMTABLE";
            this.ItemTable.Columns.Add(new DataColumn("CHECKITEM", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("BARCODEID", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("ITEMID", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("ITEMNAME", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("ITEMNAMEALIAS", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("AMOUNT", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("QUANTITY", typeof(decimal)));
            this.ItemTable.Columns.Add(new DataColumn("TOTALAMOUNT", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("TOTALAMOUNTWITHOUTTAX", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("COMMENT", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("TAXCODE", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("TAXAMOUNT", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("TAXRATEPCT", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("VOIDED", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("LINEID", typeof(short)));
            this.ItemTable.Columns.Add(new DataColumn("ORIGINALPRICEWITHTAX", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("STANDARDRETAILPRICEWITHTAX", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("COLORNAME", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("SIZENAME", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("STYLENAME", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("CONFIGNAME", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("OFFERID", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("LINKEDITEM", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("INFOCODEITEM", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("CORPORATECARD", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("ITEMGROUP", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("BATCHID", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("BATCHEXPDATE", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("PAYMENTINDEX", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("PAYMENTNAME", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("SHOULDBEMANUALLYREMOVED", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("SERIALID", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("FOUND", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("BLOCKED", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("ACTIVATED", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("RFID", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("UOM", typeof(string)));
            this.ItemTable.Columns.Add(new DataColumn("SELECTED", typeof(bool)));
            this.ItemTable.Columns.Add(new DataColumn("PROHIBITRETURN_RU", typeof(bool)));
            this.grdItems.DataSource = this.ItemTable;
        }

        private static string GetDimensionComment(Dimensions dimension)
        {
            string colorName = string.Empty;
            if (dimension != null)
            {
                if (!string.IsNullOrEmpty(dimension.ColorName))
                {
                    colorName = dimension.ColorName;
                }
                if (!string.IsNullOrEmpty(dimension.SizeName))
                {
                    if (string.IsNullOrEmpty(colorName))
                    {
                        colorName = dimension.SizeName;
                    }
                    else
                    {
                        colorName = colorName + " - " + dimension.SizeName;
                    }
                }
                if (!string.IsNullOrEmpty(dimension.StyleName))
                {
                    if (string.IsNullOrEmpty(colorName))
                    {
                        colorName = dimension.StyleName;
                    }
                    else
                    {
                        colorName = colorName + " - " + dimension.StyleName;
                    }
                }
                if (!string.IsNullOrEmpty(dimension.ConfigName))
                {
                    if (string.IsNullOrEmpty(colorName))
                    {
                        colorName = dimension.ConfigName;
                    }
                    else
                    {
                        colorName = colorName + " - " + dimension.ConfigName;
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(colorName))
            {
                colorName = Environment.NewLine + colorName;
            }
            return colorName;
        }

        private static string GetDiscountComment(PosTransaction posTransaction, SaleLineItem saleItem)
        {
            string str = string.Empty;
            decimal lineDiscount = 0M;
            if (saleItem.PeriodicDiscountLines.Count != 0)
            {
                if (saleItem.PeriodicDiscountLines.Count == 1)
                {
                    PeriodicDiscountItem item = saleItem.PeriodicDiscountLines.First<DiscountItem>() as PeriodicDiscountItem;
                    str = str + Environment.NewLine + string.Format(ApplicationLocalizer.Language.Translate(99300), new object[] { item.OfferName, ApplicationLocalizer.Language.Translate(62), posTransaction.IRounding.Round(saleItem.PeriodicDiscount, true), posTransaction.IRounding.Round(saleItem.PeriodicPctDiscount, false) });
                    if (!string.IsNullOrEmpty(item.DiscountCode))
                    {
                        str = str + Environment.NewLine + string.Format(ApplicationLocalizer.Language.Translate(99503), item.DiscountCode);
                    }
                }
                else
                {
                    StringBuilder builder = new StringBuilder(Environment.NewLine);
                    builder.AppendFormat(ApplicationLocalizer.Language.Translate(99300), new object[] { ApplicationLocalizer.Language.Translate(99301), ApplicationLocalizer.Language.Translate(62), posTransaction.IRounding.Round(saleItem.PeriodicDiscount, true), posTransaction.IRounding.Round(saleItem.PeriodicPctDiscount, false) });
                    builder.Append(Environment.NewLine);
                    builder.Append(ApplicationLocalizer.Language.Translate(101942)).Append(": ");
                    bool flag = true;
                    foreach (PeriodicDiscountItem item2 in saleItem.PeriodicDiscountLines)
                    {
                        if (!flag)
                        {
                            builder.Append(",");
                        }
                        flag = false;
                        if (item2.Amount != 0M)
                        {
                            builder.Append(" ").Append(posTransaction.IRounding.Round(item2.Amount, true));
                        }
                        if (item2.Percentage != 0M)
                        {
                            builder.AppendFormat(ApplicationLocalizer.Language.Translate(99303), posTransaction.IRounding.Round(item2.Percentage, false));
                        }
                        if (!string.IsNullOrEmpty(item2.DiscountCode))
                        {
                            builder.Append(string.Empty + string.Format(ApplicationLocalizer.Language.Translate(99503), item2.DiscountCode));
                        }
                    }
                    str = str + builder.ToString();
                }
            }
            lineDiscount = saleItem.LineDiscount;
            if (lineDiscount != 0M)
            {
                string str2 = str;
                str = str2 + Environment.NewLine + ApplicationLocalizer.Language.Translate(63) + ": " + posTransaction.IRounding.Round(lineDiscount, true) + " (" + posTransaction.IRounding.Round(saleItem.LinePctDiscount, false) + " %)";
            }
            lineDiscount = saleItem.TotalDiscount;
            if (lineDiscount != 0M)
            {
                string str3 = str;
                str = str3 + Environment.NewLine + ApplicationLocalizer.Language.Translate(64) + ": " + posTransaction.IRounding.Round(lineDiscount, true) + " (" + posTransaction.IRounding.Round(saleItem.TotalPctDiscount, false) + " %)";
            }
            lineDiscount = saleItem.LoyaltyDiscount;
            if (lineDiscount != 0M)
            {
                str = str + string.Format(ApplicationLocalizer.Language.Translate(55607), new object[] { Environment.NewLine, ApplicationLocalizer.Language.Translate(55606), posTransaction.IRounding.Round(lineDiscount, true), posTransaction.IRounding.Round(saleItem.LoyaltyPercentageDiscount, false) });
            }
            return str;
        }

        public void ShowTransaction(PosTransaction transaction)
        {
            this.posApp_TransactionDataChanged(null, null, transaction);
        }

        protected virtual void posApp_TransactionDataChanged(object sender, EventArgs e, PosTransaction posTransaction)
        {
            try
            {
                int num = 0;
                //if (posTransaction is CustomerPaymentTransaction)
                //{
                //    this.receiptItems1.DisplayCPTDeposits(posTransaction);
                //    num = this.receiptPayments1.DisplayCPTPayments(posTransaction);
                //}
                //else if (posTransaction is SalesOrderTransaction)
                //{
                //    this.receiptItems1.DisplaySOTItems(posTransaction);
                //    num = this.receiptPayments1.DisplayRTPayments(posTransaction);
                //}
                //else if (posTransaction is SalesInvoiceTransaction)
                //{
                //    this.receiptItems1.DisplaySITItems(posTransaction);
                //    num = this.receiptPayments1.DisplayRTPayments(posTransaction);
                //}
                //else 
                
                if ((posTransaction == null) || (posTransaction is RetailTransaction))
                {
                    this.DisplayRTItems(posTransaction);
                 
                }
                
            }
            catch (Exception exception)
            {
                ApplicationExceptionHandler.HandleException(this.ToString(), exception);
                POSFormsManager.ShowPOSErrorDialog(new PosisException(4, exception));
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            this.gridView1.MovePrev();
            this.CheckRowPosition();
            this.gridView1_SelectionChanged(null, null);
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            this.gridView1.MoveNext();
            int topRowIndex = this.gridView1.TopRowIndex;
            if (this.gridView1.IsLastRow && (this.gridView1.RowCount > 0))
            {
                this.GetSuspendedTransactionResultList(string.Empty, this.nextPage, 200, this.sortBy, this.sortAsc, true);
                this.nextPage++;
                this.gridControl1.DataSource = this.suspendedTransactionResultList;
                this.gridControl1.RefreshDataSource();
                this.gridView1.TopRowIndex = topRowIndex;
            }
            //this.SetFormFocus();
            this.CheckRowPosition();
            this.gridView1_SelectionChanged(null, null);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            #region Impresión de ticket
            DAC odac = new DAC(ApplicationSettings.Database.LocalConnectionString);
            PosTransaction posTransaction = LoadTransaction(this.selectedTransactionId);

            if (posTransaction is RetailTransaction)
            {
                try
                {
                    RetailTransaction retailTransaction = posTransaction as RetailTransaction;
                    
                    if (retailTransaction.Comment.Trim() == "suspendetransaccion" && retailTransaction.EntryStatus == PosTransaction.TransactionStatus.OnHold)
                    {
                        string str = null;
                        string custid;
                        if (retailTransaction.Customer != null)
                        {
                            if (retailTransaction.Customer.CustomerId != null)
                            {
                                custid = retailTransaction.Customer.CustomerId;
                            }
                            else
                            {
                                custid = odac.getDeftCust(ApplicationSettings.Terminal.StoreId);
                            }
                        }
                        else
                        {
                            custid = odac.getDeftCust(ApplicationSettings.Terminal.StoreId);
                        }
                       
                        string vendedor = "                      ";
                        if (retailTransaction.SaleItems.First.Value.SalespersonName != null)
                        {
                            vendedor = retailTransaction.SaleItems.First.Value.SalespersonName;
                        }
                       
                        reportLayout = new StringBuilder(2500);
                        reportLayout.AppendLine(singleLine);
                        reportLayout.AppendLine("                    TICKET DE VENTA");
                        reportLayout.AppendLine(singleLine);
                       
                        reportLayout.AppendLine("        Fecha: " + DateTime.Now.ToShortDateString() + "       Importe: " + retailTransaction.AmountDue.ToString("#,#0.00"));
                        reportLayout.AppendLine();
                        reportLayout.AppendLine("   Transacción         Vendedor              Cliente");
                        reportLayout.AppendLine();
                        string salesPersonId = string.Empty;
                        string salesPersonName = string.Empty;
                        DataTable DtRetVal = new DataTable();
                        DtRetVal = odac.getTransactionSuspendedNum(selectedTransactionId);
                        if (DtRetVal.Rows.Count > 0)
                        {
                            
                            selectedTransactionId = DtRetVal.Rows[0]["SUSPENDEDTRAN"].ToString();
                            salesPersonId = DtRetVal.Rows[0]["SALESPERSONID"].ToString();
                            salesPersonName = DtRetVal.Rows[0]["SALESPERSONNAME"].ToString();
                            str = DtRetVal.Rows[0]["FOLIO"].ToString();
                            
                        }
                        //string suspendedtran = Application.BusinessLogic.SuspendRetrieveSystem.GetLastSuspendedTransactionId(Application.Shift.StoreId, Application.Shift.TerminalId);


                       
                        str = Convert.ToInt64(str).ToString("000000000000");
                        
                        
                        
                       
                        if (string.IsNullOrEmpty(str))
                        {
                            App.Services.Dialog.ShowMessage("Error al recuperar la transacción suspendida.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        reportLayout.AppendLine(str + "      " + salesPersonName + "        " + custid);
                        reportLayout.AppendLine("<B: " + str + ">");
                        reportLayout.AppendLine();
                        reportLayout.AppendLine(Convert.ToChar(27) + "|2C      ESTE TICKET NO ES UN COMPROBANTE DE PAGO" + Convert.ToChar(27) + "|1C");
                        reportLayout.AppendLine();
                        reportLayout.AppendLine();
                        reportLayout.AppendLine();
                        reportLayout.AppendLine();

                        for (int i = 0; i < 2; i++)
                        {
                            App.Services.Peripherals.Printer.PrintReceipt(reportLayout.ToString());
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    App.Services.Dialog.ShowMessage(ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            #endregion
            
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButtonEx1_Click(object sender, EventArgs e)
        {
            gridView2.MovePrev();
            CheckRowPositionItems();
        }

        private void CheckRowPositionItems()
        {

            this.simpleButtonEx2.Enabled = !this.gridView2.IsLastRow;
            this.simpleButtonEx1.Enabled = !this.gridView2.IsFirstRow;
        }

        private void simpleButtonEx2_Click(object sender, EventArgs e)
        {
            gridView2.MoveNext();
            CheckRowPositionItems();
        }
    }
}
