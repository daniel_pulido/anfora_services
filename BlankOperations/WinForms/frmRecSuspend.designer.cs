﻿namespace Microsoft.Dynamics.Retail.Pos.BlankOperations
{
    partial class frmRecSuspend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnCancel = new LSRetailPosis.POSProcesses.WinControls.SimpleButtonEx();
            this.BtnAceptar = new LSRetailPosis.POSProcesses.WinControls.SimpleButtonEx();
            this.lblAmt = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtSuspend = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.styleController)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(37)))), ((int)(((byte)(127)))));
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.tableLayoutPanel1.SetColumnSpan(this.BtnCancel, 3);
            this.BtnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnCancel.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCancel.Location = new System.Drawing.Point(3, 229);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 5, 10);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(1016, 29);
            this.BtnCancel.TabIndex = 15;
            this.BtnCancel.Text = "Cancelar";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnAceptar
            // 
            this.BtnAceptar.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAceptar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(37)))), ((int)(((byte)(127)))));
            this.BtnAceptar.Appearance.Options.UseFont = true;
            this.BtnAceptar.Appearance.Options.UseForeColor = true;
            this.tableLayoutPanel1.SetColumnSpan(this.BtnAceptar, 3);
            this.BtnAceptar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnAceptar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAceptar.Location = new System.Drawing.Point(3, 187);
            this.BtnAceptar.Margin = new System.Windows.Forms.Padding(3, 3, 5, 10);
            this.BtnAceptar.Name = "BtnAceptar";
            this.BtnAceptar.Size = new System.Drawing.Size(1016, 29);
            this.BtnAceptar.TabIndex = 14;
            this.BtnAceptar.Text = "OK";
            this.BtnAceptar.Click += new System.EventHandler(this.BtnAceptar_Click);
            // 
            // lblAmt
            // 
            this.lblAmt.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblAmt, 3);
            this.lblAmt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAmt.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.lblAmt.Location = new System.Drawing.Point(3, 100);
            this.lblAmt.Name = "lblAmt";
            this.lblAmt.Size = new System.Drawing.Size(1018, 42);
            this.lblAmt.TabIndex = 16;
            this.lblAmt.Text = "Introduzca la transacción a recuperar:";
            this.lblAmt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.lblAmt, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.BtnCancel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.BtnAceptar, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtSuspend, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1024, 768);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // txtSuspend
            // 
            this.txtSuspend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSuspend.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSuspend.Location = new System.Drawing.Point(412, 145);
            this.txtSuspend.Name = "txtSuspend";
            this.txtSuspend.Size = new System.Drawing.Size(198, 29);
            this.txtSuspend.TabIndex = 17;
            // 
            // frmRecSuspend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.tableLayoutPanel1);
            this.LookAndFeel.SkinName = "Money Twins";
            this.Name = "frmRecSuspend";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPromoc";
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.styleController)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private LSRetailPosis.POSProcesses.WinControls.SimpleButtonEx BtnCancel;
        private LSRetailPosis.POSProcesses.WinControls.SimpleButtonEx BtnAceptar;
        private System.Windows.Forms.Label lblAmt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox txtSuspend;
    }
}