﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using LSRetailPosis.Settings;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Collections;

namespace Microsoft.Dynamics.Retail.Pos.SalesOrder
{
    class DAC
    {
        private SqlConnection conn;
        public DAC(SqlConnection connData)
        {
            conn = connData;
        }

        public DataTable selectData(string consulta)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = consulta;
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public string getReturnReceipt(SqlConnection _sqlConn, string _store, string _terminal, string _transId)
        {
            string retVal = string.Empty;
            DataTable DtResults = new DataTable();
            try
            {
                if (_sqlConn.State != ConnectionState.Open)
                {
                    _sqlConn.Open();
                }
                string commText = "exec xsp_getReturnReceipt @store, @terminal, @transactionid";
                SqlCommand sqlComm = new SqlCommand(commText, _sqlConn);
                sqlComm.Parameters.Add(new SqlParameter("@store", (object)_store));
                sqlComm.Parameters.Add(new SqlParameter("@terminal", (object)_terminal));
                sqlComm.Parameters.Add(new SqlParameter("@transactionid", (object)_transId));
                SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlComm);
                sqlAdapt.Fill(DtResults);
                if (DtResults.Rows.Count > 0)
                {
                    retVal = DtResults.Rows[0]["ReceiptId"].ToString();
                }
            }
            finally
            {
                _sqlConn.Close();
            }
            return retVal;
        }

        public DataTable getDefaultSalesPerson(string storeId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT [GRWDEFAULTSALESMAN],[GRWASKSALESPERSON] FROM [ax].[RETAILSTORETABLE] WHERE STORENUMBER = @STOREID ";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@STOREID", SqlDbType.NVarChar, 10).Value = storeId;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getDefaultSalesPersonName(string StaffId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT [NAMEONRECEIPT] FROM [ax].[RETAILSTAFFTABLE] where [STAFFID] = @STAFF";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@STAFF", SqlDbType.NVarChar, 25).Value = StaffId;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }
    }
}
