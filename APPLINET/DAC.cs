﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using LSRetailPosis.Settings;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Collections;
using System.Windows.Forms;

namespace APPLINET
{
    class DAC
    {
        private SqlConnection conn;
        public DAC(SqlConnection connData)
        {
            conn = connData;
        }

        public DataTable selectData(string consulta)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = consulta;
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getCardPayments(string store, string DataAreaId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "select tt.TENDERTYPEID TenderId, tt.NAME DESCRIPCION from RETAILSTORETENDERTYPETABLE tt left join RETAILSTORETABLE s on tt.CHANNEL = s.RECID where tt.DATAAREAID = @DataAreaId and s.STORENUMBER = @Store and tt.FUNCTION_ = 1";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@Store", SqlDbType.NVarChar, 10).Value = store;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 10).Value = DataAreaId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public DataTable getCardTenderTypes(string DataAreaId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = " SELECT [CARDTYPE], [RETAILCARDTYPEID] FROM [ax].[GRW_CONFIGTARJBANC] WHERE DATAAREAID = @DataAreaId AND PROVIDER = 'APPLINET'";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 10).Value = DataAreaId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public bool GuardaPagoTarjeta(string numeroTarjeta, string nombreCliente, string currency, string nombreAplicacion, string tipoTarjeta, string referenciaFinanciera, string importe, string afiliacion, string autorizacion, string secuencia, string codTransaction, string terminalApplinet, string dataAreaId, string storeId, string terminalId, string transactionId, int lineId, long batchId, string staff)
        {
            SqlConnection connection = LSRetailPosis.Settings.ApplicationSettings.Database.LocalConnection;
            string sqlstr = string.Empty;

            sqlstr = "Exec [xsp_GRW_INSERTAPPLINETTRAN] @NUMEROTARJETA, @NOMBRECLIENTE, @CURRENCY, @NOMBREAPLICACION, @TIPOTARJETA, @C17_REFERENCIAFINAN, @IMPORTE, @C07_AFILIACION, @C06_NOAUTORIZACION, @C04_SECUENCIA, @C01_CODTRANSACTION, @TERMINALAPPLINET, @DATAAREAID, @STOREID, @TERMINAL, @TRANSACTIONID, @LINEID, @BATCHID, @STAFF";

            DataTable TranTable = new DataTable();
            try
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(sqlstr, conn))
                {
                    command.Parameters.Add("@NUMEROTARJETA", SqlDbType.NVarChar, 50).Value = numeroTarjeta;
                    command.Parameters.Add("@NOMBRECLIENTE", SqlDbType.NVarChar, 100).Value = nombreCliente;
                    command.Parameters.Add("@CURRENCY", SqlDbType.NVarChar, 3).Value = currency;
                    command.Parameters.Add("@NOMBREAPLICACION", SqlDbType.NVarChar, 50).Value = nombreAplicacion;
                    command.Parameters.Add("@TIPOTARJETA", SqlDbType.NVarChar, 50).Value = tipoTarjeta;
                    command.Parameters.Add("@C17_REFERENCIAFINAN", SqlDbType.NVarChar, 50).Value = referenciaFinanciera;
                    command.Parameters.Add("@IMPORTE", SqlDbType.NVarChar, 50).Value = importe;
                    command.Parameters.Add("@C07_AFILIACION", SqlDbType.NVarChar, 50).Value = afiliacion;
                    command.Parameters.Add("@C06_NOAUTORIZACION", SqlDbType.NVarChar, 50).Value = autorizacion;
                    command.Parameters.Add("@C04_SECUENCIA", SqlDbType.NVarChar, 50).Value = secuencia;
                    command.Parameters.Add("@C01_CODTRANSACTION", SqlDbType.NVarChar, 50).Value = codTransaction;
                    command.Parameters.Add("@TERMINALAPPLINET", SqlDbType.NVarChar, 50).Value = terminalApplinet;
                    command.Parameters.Add("@DATAAREAID", SqlDbType.NVarChar, 4).Value = dataAreaId;
                    command.Parameters.Add("@STOREID", SqlDbType.NVarChar, 10).Value = storeId;
                    command.Parameters.Add("@TERMINAL", SqlDbType.NVarChar, 10).Value = terminalId;
                    command.Parameters.Add("@TRANSACTIONID", SqlDbType.NVarChar, 44).Value = transactionId;
                    command.Parameters.Add("@LINEID", SqlDbType.Int).Value = lineId;
                    command.Parameters.Add("@BATCHID", SqlDbType.BigInt).Value = batchId;
                    command.Parameters.Add("@STAFF", SqlDbType.NVarChar, 25).Value = staff;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        TranTable.Load(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                CreaMensaje(ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return true;
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }

        public DataTable getCardDetailPayments(string store, string DataAreaId, string tenderTypeId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "Exec xsp_getCardDetailTenders @Store, @DataAreaId, @TenderTypeId";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@Store", SqlDbType.NVarChar, 10).Value = store;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 10).Value = DataAreaId;
                    command.Parameters.Add("@TenderTypeId", SqlDbType.NVarChar, 10).Value = tenderTypeId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        public void GuardaDetalletarjeta(string store, string DataAreaId, string Terminal, int lineNum, string tenderTypeId, string cardTypeId, string TransactionId)
        {
            try
            {
                string cmdText = "Exec xsp_insertDetailTenders @Store, @DataAreaId, @TenderTypeId, @LineNum, @CardTypeId, @TerminalId, @TransactionId";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@Store", SqlDbType.NVarChar, 10).Value = store;
                    command.Parameters.Add("@DataAreaId", SqlDbType.NVarChar, 10).Value = DataAreaId;
                    command.Parameters.Add("@TenderTypeId", SqlDbType.NVarChar, 10).Value = tenderTypeId;
                    command.Parameters.Add("@LineNum", SqlDbType.Int).Value = lineNum;
                    command.Parameters.Add("@CardTypeId", SqlDbType.NVarChar, 10).Value = cardTypeId;
                    command.Parameters.Add("@TerminalId", SqlDbType.NVarChar, 10).Value = Terminal;
                    command.Parameters.Add("@TransactionId", SqlDbType.NVarChar, 44).Value = TransactionId;
                    command.ExecuteReader();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }

        public DataTable getAPPLINETConfig(string StoreId, string TerminalId)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdText = "SELECT TOP 1 * FROM [ax].[GRW_APPLINETCONFIG] WHERE StoreId = @StoreId AND TerminalId = @TerminalId ";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    command.Parameters.Add("@StoreId", SqlDbType.NVarChar, 10).Value = StoreId;
                    command.Parameters.Add("@TerminalId", SqlDbType.NVarChar, 10).Value = TerminalId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }
    }
}
