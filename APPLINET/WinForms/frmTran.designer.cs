﻿namespace APPLINET.WinForms
{
    partial class frmTran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnAcept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransactionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerminal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vGridControl1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NUMEROTARJETA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IMPORTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.C06_NOAUTORIZACION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.C04_SECUENCIA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.C17_REFERENCIAFINAN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FECHAHORA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.styleController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1018, 42);
            this.label1.TabIndex = 5;
            this.label1.Text = "Seleccione la transacción a devolver";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAcept
            // 
            this.btnAcept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcept.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnAcept.Location = new System.Drawing.Point(3, 687);
            this.btnAcept.Name = "btnAcept";
            this.btnAcept.Size = new System.Drawing.Size(1018, 36);
            this.btnAcept.TabIndex = 6;
            this.btnAcept.Text = "Aceptar";
            this.btnAcept.UseVisualStyleBackColor = true;
            this.btnAcept.Click += new System.EventHandler(this.btnAcept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnCancel.Location = new System.Drawing.Point(3, 729);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(1018, 36);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransactionDate,
            this.colStaff,
            this.colTerminal,
            this.colReceiptID,
            this.colType,
            this.colNetAmount});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.OptionsView.ShowPreview = true;
            this.gridView1.RowHeight = 40;
            this.gridView1.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.Default;
            this.gridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // colTransactionDate
            // 
            this.colTransactionDate.Caption = "Date";
            this.colTransactionDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTransactionDate.FieldName = "CREATEDDATE";
            this.colTransactionDate.Name = "colTransactionDate";
            this.colTransactionDate.OptionsColumn.AllowEdit = false;
            this.colTransactionDate.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colTransactionDate.Visible = true;
            this.colTransactionDate.VisibleIndex = 0;
            this.colTransactionDate.Width = 165;
            // 
            // colStaff
            // 
            this.colStaff.Caption = "Staff";
            this.colStaff.FieldName = "STAFF";
            this.colStaff.Name = "colStaff";
            this.colStaff.Visible = true;
            this.colStaff.VisibleIndex = 1;
            this.colStaff.Width = 88;
            // 
            // colTerminal
            // 
            this.colTerminal.Caption = "Terminal";
            this.colTerminal.FieldName = "TERMINAL";
            this.colTerminal.Name = "colTerminal";
            this.colTerminal.Visible = true;
            this.colTerminal.VisibleIndex = 2;
            this.colTerminal.Width = 88;
            // 
            // colReceiptID
            // 
            this.colReceiptID.Caption = "Transaction";
            this.colReceiptID.FieldName = "RECEIPTID";
            this.colReceiptID.Name = "colReceiptID";
            this.colReceiptID.Visible = true;
            this.colReceiptID.VisibleIndex = 3;
            this.colReceiptID.Width = 164;
            // 
            // colType
            // 
            this.colType.Caption = "Type";
            this.colType.FieldName = "TYPE";
            this.colType.Name = "colType";
            this.colType.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colType.Visible = true;
            this.colType.VisibleIndex = 4;
            // 
            // colNetAmount
            // 
            this.colNetAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colNetAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNetAmount.Caption = "Amount";
            this.colNetAmount.DisplayFormat.FormatString = "c2";
            this.colNetAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetAmount.FieldName = "GROSSAMOUNT";
            this.colNetAmount.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colNetAmount.Name = "colNetAmount";
            this.colNetAmount.Visible = true;
            this.colNetAmount.VisibleIndex = 5;
            this.colNetAmount.Width = 80;
            // 
            // vGridControl1
            // 
            this.vGridControl1.Name = "vGridControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(3, 45);
            this.gridControl1.MainView = this.gridView3;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1018, 636);
            this.gridControl1.TabIndex = 8;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3,
            this.gridView2});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NUMEROTARJETA,
            this.IMPORTE,
            this.C06_NOAUTORIZACION,
            this.C04_SECUENCIA,
            this.C17_REFERENCIAFINAN,
            this.FECHAHORA});
            this.gridView3.GridControl = this.gridControl1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsCustomization.AllowColumnMoving = false;
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsCustomization.AllowGroup = false;
            this.gridView3.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView3.OptionsCustomization.AllowRowSizing = true;
            this.gridView3.OptionsMenu.EnableColumnMenu = false;
            this.gridView3.OptionsMenu.EnableFooterMenu = false;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gridView3.RowHeight = 40;
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView3_FocusedRowChanged);
            this.gridView3.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView3_ValidatingEditor);
            // 
            // NUMEROTARJETA
            // 
            this.NUMEROTARJETA.Caption = "Número de tarjeta";
            this.NUMEROTARJETA.FieldName = "NUMEROTARJETA";
            this.NUMEROTARJETA.Name = "NUMEROTARJETA";
            this.NUMEROTARJETA.OptionsColumn.AllowEdit = false;
            this.NUMEROTARJETA.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.NUMEROTARJETA.Visible = true;
            this.NUMEROTARJETA.VisibleIndex = 0;
            // 
            // IMPORTE
            // 
            this.IMPORTE.Caption = "Monto a devolver";
            this.IMPORTE.FieldName = "IMPORTE";
            this.IMPORTE.Name = "IMPORTE";
            this.IMPORTE.OptionsColumn.AllowEdit = false;
            this.IMPORTE.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.IMPORTE.Visible = true;
            this.IMPORTE.VisibleIndex = 1;
            // 
            // C06_NOAUTORIZACION
            // 
            this.C06_NOAUTORIZACION.Caption = "Número de autorización";
            this.C06_NOAUTORIZACION.FieldName = "C06_NOAUTORIZACION";
            this.C06_NOAUTORIZACION.Name = "C06_NOAUTORIZACION";
            this.C06_NOAUTORIZACION.OptionsColumn.AllowEdit = false;
            this.C06_NOAUTORIZACION.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.C06_NOAUTORIZACION.Visible = true;
            this.C06_NOAUTORIZACION.VisibleIndex = 2;
            // 
            // C04_SECUENCIA
            // 
            this.C04_SECUENCIA.Caption = "Secuencia de transacción";
            this.C04_SECUENCIA.FieldName = "C04_SECUENCIA";
            this.C04_SECUENCIA.Name = "C04_SECUENCIA";
            this.C04_SECUENCIA.OptionsColumn.AllowEdit = false;
            this.C04_SECUENCIA.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.C04_SECUENCIA.Visible = true;
            this.C04_SECUENCIA.VisibleIndex = 3;
            // 
            // C17_REFERENCIAFINAN
            // 
            this.C17_REFERENCIAFINAN.Caption = "Referencia";
            this.C17_REFERENCIAFINAN.FieldName = "C17_REFERENCIAFINAN";
            this.C17_REFERENCIAFINAN.Name = "C17_REFERENCIAFINAN";
            this.C17_REFERENCIAFINAN.OptionsColumn.AllowEdit = false;
            this.C17_REFERENCIAFINAN.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.C17_REFERENCIAFINAN.Visible = true;
            this.C17_REFERENCIAFINAN.VisibleIndex = 4;
            // 
            // FECHAHORA
            // 
            this.FECHAHORA.Caption = "Fecha";
            this.FECHAHORA.FieldName = "FECHAHORA";
            this.FECHAHORA.Name = "FECHAHORA";
            this.FECHAHORA.OptionsColumn.AllowEdit = false;
            this.FECHAHORA.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.FECHAHORA.Visible = true;
            this.FECHAHORA.VisibleIndex = 5;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.gridControl1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCancel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnAcept, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1024, 768);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // frmTran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.tableLayoutPanel1);
            this.LookAndFeel.SkinName = "Money Twins";
            this.Name = "frmTran";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmTran";
            this.Load += new System.EventHandler(this.frmTran_Load);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.styleController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAcept;
        private System.Windows.Forms.Button btnCancel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colTerminal;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptID;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colNetAmount;
        private DevExpress.XtraGrid.Views.Grid.GridView vGridControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn NUMEROTARJETA;
        private DevExpress.XtraGrid.Columns.GridColumn IMPORTE;
        private DevExpress.XtraGrid.Columns.GridColumn C06_NOAUTORIZACION;
        private DevExpress.XtraGrid.Columns.GridColumn C04_SECUENCIA;
        private DevExpress.XtraGrid.Columns.GridColumn C17_REFERENCIAFINAN;
        private DevExpress.XtraGrid.Columns.GridColumn FECHAHORA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}