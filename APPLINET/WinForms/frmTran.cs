﻿using System;
using System.Globalization;
using System.Windows.Forms;
using LSRetailPosis.POSProcesses;
using LSRetailPosis.POSProcesses.WinFormsTouch;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using LSRetailPosis.Settings;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Collections;
using LSRetailPosis.Transaction;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.DataAccess;
using System.Data.SqlClient;
using System.ComponentModel;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System.Xml;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Repository;
namespace APPLINET.WinForms
{
    public partial class frmTran : frmTouchBase
    {
        public string selectedReference = string.Empty;
        public string selectedSecuence = string.Empty;
        public string selectedAuth = string.Empty;
        public string selectedDate = string.Empty;
        private DataTable dtTrans;
        public string selectedAmount = string.Empty;
        decimal _amountDue;
        bool devolucion;

        public frmTran(DataTable dtTran, decimal amountDue, bool esDevolucion)
        {
            InitializeComponent();
            this.dtTrans = dtTran;
            _amountDue = amountDue;
            devolucion = esDevolucion;
        }

        private void frmTran_Load(object sender, EventArgs e)
        {
            this.gridControl1.DataSource = dtTrans;
            if (!devolucion)
            {
                this.IMPORTE.OptionsColumn.AllowEdit = false;
            }
            else
            {
                this.IMPORTE.OptionsColumn.AllowEdit = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
            return;
        }

        private void btnAcept_Click(object sender, EventArgs e)
        {
            System.Data.DataRow Row = gridView3.GetDataRow(gridView3.GetSelectedRows()[0]);
            selectedReference = (string)Row["C17_REFERENCIAFINAN"];
            selectedSecuence = (string)Row["C04_SECUENCIA"];
            selectedAuth = (string)Row["C06_NOAUTORIZACION"];
            selectedDate = (string)Row["FECHAHORA"].ToString();
            selectedAmount = (string)Row["IMPORTE"];
            if (decimal.Parse(selectedAmount) > decimal.Negate(_amountDue))
            {
                CreaMensaje("Favor de elegir un monto menor o igual a la deuda: " + decimal.Negate(_amountDue).ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                Close();
                return;
            }
        }

        private void gridView3_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            System.Data.DataRow Row = gridView3.GetDataRow(gridView3.GetSelectedRows()[0]);
            selectedReference = (string)Row["C17_REFERENCIAFINAN"];
            selectedSecuence = (string)Row["C04_SECUENCIA"];
            selectedAuth = (string)Row["C06_NOAUTORIZACION"];
            selectedDate = (string)Row["FECHAHORA"].ToString();
            selectedAmount = (string)Row["IMPORTE"];
        }

        private void gridView3_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            if (gridView3.FocusedColumn.Name == "IMPORTE")
            {
                if (decimal.Parse(e.Value.ToString()) <= decimal.Negate(_amountDue))
                {
                    e.Valid = true;
                }
                else
                {
                    e.ErrorText = "Favor de elegir un monto menor o igual a la deuda: " + decimal.Negate(_amountDue).ToString();
                    e.Valid = false;
                }
            }
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }
    }
}
