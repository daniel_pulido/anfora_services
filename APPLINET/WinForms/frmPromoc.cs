﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LSRetailPosis.Settings;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using Microsoft.Dynamics.Retail.Pos.DataEntity.Extensions;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using DE = Microsoft.Dynamics.Retail.Pos.DataEntity;
using DM = Microsoft.Dynamics.Retail.Pos.DataManager;
using LSRetailPosis.Transaction.Line.TenderItem;
using LSRetailPosis.DataAccess;
using LSRetailPosis.Transaction;
using LSRetailPosis.Settings.HardwareProfiles;
using LSRetailPosis;
using LSRetailPosis.POSProcesses;
using APPLINET.AppliNET;
using System.ComponentModel.Composition;

namespace APPLINET.WinForms
{
    public partial class frmPromoc : frmTouchBase
    {
        #region Atributos
        private IApplication apps;
        [Import]
        public IApplication Application
        {
            get
            {
                return this.apps;
            }
            set
            {
                this.apps = value;
                InternalApplication = value;
            }
        }

        internal static IApplication InternalApplication { get; private set; }

        public int promoMes { get; set; }
        #endregion

        #region Constructor
        public frmPromoc ()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!this.DesignMode)
            {
                TranslateLabels();
                this.cmbMeses.SelectedIndex = 0;
                //this.viewModel.PropertyChanged += new PropertyChangedEventHandler(OnViewModel_PropertyChanged);
            }
            base.OnLoad(e);
        }
        #endregion

        #region Eventos
        void OnViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void TranslateLabels()
        {
            this.Text = LSRetailPosis.ApplicationLocalizer.Language.Translate(51041); // Customer
        }

        protected override void OnHelpRequested(HelpEventArgs hevent)
        {
            if (hevent == null)
                throw new ArgumentNullException("hevent");

            //LSRetailPosis.POSControls.POSFormsManager.ShowHelp(this);

            hevent.Handled = true;
            base.OnHelpRequested(hevent);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        protected override void OnClosing(CancelEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.OK)
            {

            }
            base.OnClosing(e);
        }
        
        #endregion

        #region MetodosAuxiliares
        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }
        
        #endregion

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            if (this.cmbMeses.SelectedIndex > 0)
            {
                int validador;
                if (int.TryParse(this.cmbMeses.SelectedItem.ToString(), out validador))
                {
                    this.promoMes = int.Parse(this.cmbMeses.SelectedItem.ToString());
                }
            }
            this.DialogResult = DialogResult.OK;
            Close();
            return;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
            return;
        }

        private void cmbMeses_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbMeses.SelectedIndex > 0)
            {
                int validador;
                if (int.TryParse(this.cmbMeses.SelectedItem.ToString(), out validador))
                {
                    this.promoMes = int.Parse(this.cmbMeses.SelectedItem.ToString());
                }
            }
        }
    }
}
