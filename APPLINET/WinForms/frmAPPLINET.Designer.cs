﻿
namespace APPLINET.WinForms
{
    partial class frmAPPLINET
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.themedPanel = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTotalAmtDue = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtAmt = new System.Windows.Forms.TextBox();
            this.labelEstatusTRX = new System.Windows.Forms.Label();
            this.txtAutorizacion = new System.Windows.Forms.TextBox();
            this.lblForzada = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtSecCod = new System.Windows.Forms.TextBox();
            this.lblSecCod = new System.Windows.Forms.Label();
            this.lblExpDate = new System.Windows.Forms.Label();
            this.txtexpDate = new System.Windows.Forms.DateTimePicker();
            this.txtCarnbr = new System.Windows.Forms.TextBox();
            this.lblCardNumber = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblAmt = new System.Windows.Forms.Label();
            this.lblTipoPago = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTotalAmt = new System.Windows.Forms.Label();
            this.BtnAmt = new DevExpress.XtraEditors.SimpleButton();
            this.chkBoxDevolucion = new System.Windows.Forms.CheckBox();
            this.numAmount = new LSRetailPosis.POSProcesses.WinControls.NumPad();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.BtnCancel = new LSRetailPosis.POSProcesses.WinControls.SimpleButtonEx();
            this.BtnAceptar = new LSRetailPosis.POSProcesses.WinControls.SimpleButtonEx();
            this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.styleController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themedPanel)).BeginInit();
            this.themedPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // themedPanel
            // 
            this.themedPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.themedPanel.Controls.Add(this.tableLayoutPanel1);
            this.themedPanel.Location = new System.Drawing.Point(0, 0);
            this.themedPanel.Name = "themedPanel";
            this.themedPanel.Size = new System.Drawing.Size(902, 780);
            this.themedPanel.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblTotalAmtDue, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 482F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(900, 908);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblTotalAmtDue
            // 
            this.lblTotalAmtDue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalAmtDue.AutoSize = true;
            this.lblTotalAmtDue.Location = new System.Drawing.Point(3, 0);
            this.lblTotalAmtDue.Name = "lblTotalAmtDue";
            this.lblTotalAmtDue.Size = new System.Drawing.Size(894, 77);
            this.lblTotalAmtDue.TabIndex = 0;
            this.lblTotalAmtDue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 367F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 199F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.numAmount, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 80);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(893, 476);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.txtAmt, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.labelEstatusTRX, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this.txtAutorizacion, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this.lblForzada, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this.txtCarnbr, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this.lblCardNumber, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this.txtName, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.lblName, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.lblAmt, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblTipoPago, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.comboBox1, 0, 3);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 12;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(260, 470);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // txtAmt
            // 
            this.txtAmt.Location = new System.Drawing.Point(3, 37);
            this.txtAmt.Name = "txtAmt";
            this.txtAmt.Size = new System.Drawing.Size(254, 29);
            this.txtAmt.TabIndex = 1;
            // 
            // labelEstatusTRX
            // 
            this.labelEstatusTRX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelEstatusTRX.AutoSize = true;
            this.labelEstatusTRX.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEstatusTRX.Location = new System.Drawing.Point(3, 449);
            this.labelEstatusTRX.Name = "labelEstatusTRX";
            this.labelEstatusTRX.Size = new System.Drawing.Size(254, 21);
            this.labelEstatusTRX.TabIndex = 12;
            this.labelEstatusTRX.Text = "Label";
            this.labelEstatusTRX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelEstatusTRX.Visible = false;
            // 
            // txtAutorizacion
            // 
            this.txtAutorizacion.Location = new System.Drawing.Point(3, 414);
            this.txtAutorizacion.MaxLength = 6;
            this.txtAutorizacion.Name = "txtAutorizacion";
            this.txtAutorizacion.Size = new System.Drawing.Size(227, 29);
            this.txtAutorizacion.TabIndex = 7;
            // 
            // lblForzada
            // 
            this.lblForzada.AutoSize = true;
            this.lblForzada.Location = new System.Drawing.Point(3, 381);
            this.lblForzada.Name = "lblForzada";
            this.lblForzada.Size = new System.Drawing.Size(214, 21);
            this.lblForzada.TabIndex = 11;
            this.lblForzada.Text = "No. Autorización sólo forzada";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.txtSecCod, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblSecCod, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.lblExpDate, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtexpDate, 0, 1);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 309);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(254, 69);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // txtSecCod
            // 
            this.txtSecCod.Location = new System.Drawing.Point(130, 23);
            this.txtSecCod.MaxLength = 4;
            this.txtSecCod.Name = "txtSecCod";
            this.txtSecCod.PasswordChar = '*';
            this.txtSecCod.Size = new System.Drawing.Size(108, 29);
            this.txtSecCod.TabIndex = 5;
            // 
            // lblSecCod
            // 
            this.lblSecCod.AutoSize = true;
            this.lblSecCod.Location = new System.Drawing.Point(130, 0);
            this.lblSecCod.Name = "lblSecCod";
            this.lblSecCod.Size = new System.Drawing.Size(116, 20);
            this.lblSecCod.TabIndex = 1;
            this.lblSecCod.Text = "Cod. Seguridad";
            // 
            // lblExpDate
            // 
            this.lblExpDate.AutoSize = true;
            this.lblExpDate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpDate.Location = new System.Drawing.Point(3, 0);
            this.lblExpDate.Name = "lblExpDate";
            this.lblExpDate.Size = new System.Drawing.Size(91, 20);
            this.lblExpDate.TabIndex = 0;
            this.lblExpDate.Text = "Fecha Venc.";
            // 
            // txtexpDate
            // 
            this.txtexpDate.Location = new System.Drawing.Point(3, 23);
            this.txtexpDate.Name = "txtexpDate";
            this.txtexpDate.ShowUpDown = true;
            this.txtexpDate.Size = new System.Drawing.Size(121, 29);
            this.txtexpDate.TabIndex = 6;
            // 
            // txtCarnbr
            // 
            this.txtCarnbr.Location = new System.Drawing.Point(3, 264);
            this.txtCarnbr.Name = "txtCarnbr";
            this.txtCarnbr.Size = new System.Drawing.Size(254, 29);
            this.txtCarnbr.TabIndex = 3;
            // 
            // lblCardNumber
            // 
            this.lblCardNumber.AutoSize = true;
            this.lblCardNumber.Location = new System.Drawing.Point(3, 220);
            this.lblCardNumber.Name = "lblCardNumber";
            this.lblCardNumber.Size = new System.Drawing.Size(218, 21);
            this.lblCardNumber.TabIndex = 2;
            this.lblCardNumber.Text = "*Número de tarjeta (4 dígitos)";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(3, 169);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(254, 29);
            this.txtName.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(3, 140);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(117, 21);
            this.lblName.TabIndex = 10;
            this.lblName.Text = "Tarjetahabiente";
            // 
            // lblAmt
            // 
            this.lblAmt.AutoSize = true;
            this.lblAmt.Location = new System.Drawing.Point(3, 0);
            this.lblAmt.Name = "lblAmt";
            this.lblAmt.Size = new System.Drawing.Size(56, 21);
            this.lblAmt.TabIndex = 0;
            this.lblAmt.Text = "Monto";
            // 
            // lblTipoPago
            // 
            this.lblTipoPago.AutoSize = true;
            this.lblTipoPago.Location = new System.Drawing.Point(3, 75);
            this.lblTipoPago.Name = "lblTipoPago";
            this.lblTipoPago.Size = new System.Drawing.Size(52, 21);
            this.lblTipoPago.TabIndex = 13;
            this.lblTipoPago.Text = "Banco";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(3, 109);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(254, 29);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.lblTotalAmt, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.BtnAmt, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.chkBoxDevolucion, 0, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(697, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(193, 470);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // lblTotalAmt
            // 
            this.lblTotalAmt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalAmt.AutoSize = true;
            this.lblTotalAmt.Location = new System.Drawing.Point(3, 121);
            this.lblTotalAmt.Name = "lblTotalAmt";
            this.lblTotalAmt.Size = new System.Drawing.Size(187, 21);
            this.lblTotalAmt.TabIndex = 0;
            this.lblTotalAmt.Text = "Monto de Pago";
            this.lblTotalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnAmt
            // 
            this.BtnAmt.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnAmt.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.BtnAmt.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(37)))), ((int)(((byte)(127)))));
            this.BtnAmt.Appearance.Options.UseFont = true;
            this.BtnAmt.Appearance.Options.UseForeColor = true;
            this.BtnAmt.Location = new System.Drawing.Point(15, 145);
            this.BtnAmt.Name = "BtnAmt";
            this.BtnAmt.Size = new System.Drawing.Size(163, 60);
            this.BtnAmt.TabIndex = 9;
            this.BtnAmt.Text = "BtnAmt";
            this.BtnAmt.Click += new System.EventHandler(this.BtnAmt_Click_1);
            // 
            // chkBoxDevolucion
            // 
            this.chkBoxDevolucion.AutoSize = true;
            this.chkBoxDevolucion.Location = new System.Drawing.Point(3, 287);
            this.chkBoxDevolucion.Name = "chkBoxDevolucion";
            this.chkBoxDevolucion.Size = new System.Drawing.Size(138, 25);
            this.chkBoxDevolucion.TabIndex = 10;
            this.chkBoxDevolucion.Text = "¿Es devolución?";
            this.chkBoxDevolucion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBoxDevolucion.UseVisualStyleBackColor = true;
            // 
            // numAmount
            // 
            this.numAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numAmount.Appearance.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAmount.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(37)))), ((int)(((byte)(127)))));
            this.numAmount.Appearance.Options.UseFont = true;
            this.numAmount.Appearance.Options.UseForeColor = true;
            this.numAmount.AutoSize = true;
            this.numAmount.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.SetColumnSpan(this.numAmount, 2);
            this.numAmount.CurrencyCode = null;
            this.numAmount.EnteredQuantity = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numAmount.EnteredValue = "-";
            this.numAmount.EntryType = Microsoft.Dynamics.Retail.Pos.Contracts.UI.NumpadEntryTypes.Price;
            this.numAmount.Location = new System.Drawing.Point(269, 3);
            this.numAmount.MaskChar = "";
            this.numAmount.MaskInterval = 0;
            this.numAmount.MaxNumberOfDigits = 9;
            this.numAmount.MinimumSize = new System.Drawing.Size(248, 314);
            this.numAmount.Name = "numAmount";
            this.numAmount.NegativeMode = true;
            this.numAmount.NoOfTries = 0;
            this.numAmount.NumberOfDecimals = 2;
            this.numAmount.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.numAmount.PromptText = "Ingrese Monto:";
            this.numAmount.ShortcutKeysActive = false;
            this.numAmount.Size = new System.Drawing.Size(422, 470);
            this.numAmount.TabIndex = 8;
            this.numAmount.TimerEnabled = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.83203F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.16797F));
            this.tableLayoutPanel2.Controls.Add(this.BtnCancel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.BtnAceptar, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 562);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(894, 343);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(37)))), ((int)(((byte)(127)))));
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCancel.Location = new System.Drawing.Point(448, 138);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 5, 10);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(130, 60);
            this.BtnCancel.TabIndex = 13;
            this.BtnCancel.Text = "Cancelar";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click_1);
            // 
            // BtnAceptar
            // 
            this.BtnAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.BtnAceptar.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAceptar.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(37)))), ((int)(((byte)(127)))));
            this.BtnAceptar.Appearance.Options.UseFont = true;
            this.BtnAceptar.Appearance.Options.UseForeColor = true;
            this.BtnAceptar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAceptar.Location = new System.Drawing.Point(310, 138);
            this.BtnAceptar.Margin = new System.Windows.Forms.Padding(3, 3, 5, 10);
            this.BtnAceptar.Name = "BtnAceptar";
            this.BtnAceptar.Size = new System.Drawing.Size(130, 60);
            this.BtnAceptar.TabIndex = 10;
            this.BtnAceptar.Text = "Ok";
            this.BtnAceptar.Click += new System.EventHandler(this.BtnAceptar_Click);
            // 
            // frmAPPLINET
            // 
            this.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(902, 780);
            this.Controls.Add(this.themedPanel);
            this.LookAndFeel.SkinName = "Money Twins";
            this.Name = "frmAPPLINET";
            this.Text = "Pago Tarjeta de Credito";
            this.Load += new System.EventHandler(this.OnLoad);
            this.Controls.SetChildIndex(this.themedPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.styleController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themedPanel)).EndInit();
            this.themedPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl themedPanel;
        //private AddressUserControl addressUserControl1;
        private System.Windows.Forms.BindingSource bindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblTotalAmtDue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private LSRetailPosis.POSProcesses.WinControls.SimpleButtonEx BtnCancel;
        private LSRetailPosis.POSProcesses.WinControls.SimpleButtonEx BtnAceptar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblAmt;
        private System.Windows.Forms.Label lblCardNumber;
        private System.Windows.Forms.TextBox txtCarnbr;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblExpDate;
        private System.Windows.Forms.Label lblSecCod;
        private System.Windows.Forms.TextBox txtSecCod;
        private System.Windows.Forms.TextBox txtAutorizacion;
        private LSRetailPosis.POSProcesses.WinControls.NumPad numAmount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label lblTotalAmt;
        public System.Windows.Forms.TextBox txtAmt;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private DevExpress.XtraEditors.SimpleButton BtnAmt;
        private System.Windows.Forms.Label lblForzada;
        private System.Windows.Forms.Label labelEstatusTRX;
        private System.Windows.Forms.Label lblTipoPago;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DateTimePicker txtexpDate;
        private System.Windows.Forms.CheckBox chkBoxDevolucion;
    }
}
