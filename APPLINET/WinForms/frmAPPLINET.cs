﻿
using LSRetailPosis;
using LSRetailPosis.DataAccess;
using LSRetailPosis.POSProcesses.WinFormsTouch;
using LSRetailPosis.POSProcesses;
using LSRetailPosis.Settings;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.Transaction;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.ComponentModel.Composition;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System;
using System.IO;
using EglobalBBVA;
using APPLINET.AppliNET;
using System.Text;
using System.Linq;

namespace APPLINET.WinForms
{
    public partial class frmAPPLINET : frmTouchBase
    {
        #region PROPERTIES
        private RetailTransaction Transaction;
        private IApplication apps;
        private decimal amtDue;
        public string tendertype;
        private DAC odac;
        private DataTable DtFormas;
        private string referencia;
        private string secuenciaTransaccion;
        private string OperationNumber;
        private string AutNumber;
        private string tenderTypeId;
        private string cardTypeId = string.Empty;
        //Returned transactions
        private string ReturnTransId;
        private string ReturnTerminalId;
        private string ReturnStoreId;
        private string CANCEL_AMOUNT;
        private bool cargoCancelacion = false;
        private bool cargaPinPadCorrecta = false;
        private bool esTransaccionForzada = false;
        private DateTime fechaOriginalTransaccion;
        private string autorizacionForzada = string.Empty;
        int importe = 0;
        int tipoMoneda = 0;
        string cveUsuario = string.Empty;
        int mesesPagoParc = 0;
        string formaPagoCredito = string.Empty;
        string formaPagoDebito = string.Empty;
        //AppliNET
        PinPadSC Enviar = new PinPadSC();
        String venta = String.Empty;
        String devolucion = String.Empty;
        String cancelacion_venta = String.Empty;
        String cancelacion_devolucion = String.Empty;
        String venta_forzada = String.Empty;
        String consulta_puntos = String.Empty;
        String checkin = String.Empty;
        String checkout = String.Empty;
        String cargocuarto = String.Empty;
        String reautorizacion = String.Empty;
        String checkout_reautorizado = String.Empty;
        String postpropina = String.Empty;
        String cancelacion_postpropina = String.Empty;
        String cancelacion_checkin = String.Empty;
        String cancelacion_checkout = String.Empty;
        String carga_llaves = String.Empty;
        String telecarga = String.Empty;
        String sintrj_operador = String.Empty;
        String abrir_sesion = String.Empty;
        String cerrar_sesion = String.Empty;
        String pago_tarjeta_efectivo = String.Empty;
        String traspaso_tarjetas = String.Empty;
        String Pago_CIE = String.Empty;
        String Pago_con_Tarjeta = String.Empty;
        String cuenta_express = String.Empty;
        String celular = String.Empty;
        String venta_amex = String.Empty;
        String BCargaLlave = String.Empty;//Para Controlar la Carga de Llaves por Bandera 1
        String strVersion = String.Empty;//Para guardar el password del certificado
        String BACT = String.Empty;//Para Controlar Actualizacion de Version PinPad
        String SA = String.Empty;
        int sesion;
        int transaccion;
        FormatString oFormatString = new FormatString();
        //Impresion
        private const int paperWidth = 55;
        StringBuilder reportLayout;
        private static readonly string singleLine = string.Empty.PadLeft(paperWidth, '-');
        private static readonly string lineFormat = ApplicationLocalizer.Language.Translate(7060);
        private static readonly string currencyFormat = ApplicationLocalizer.Language.Translate(7061);
        private static readonly string typeFormat = ApplicationLocalizer.Language.Translate(7062);
        PinPadAppliNet oPinPad;
        bool esDevolucion = false;
        #endregion

        #region CONSTRUCTOR

        public frmAPPLINET(RetailTransaction tran, IApplication appli, string formaPago)
        {
            try
            {
                apps = appli;
                Transaction = tran;
                tenderTypeId = formaPago;
                if (string.IsNullOrEmpty(tenderTypeId))
                {
                    CreaMensaje("Error en la carga de configuración de la forma de pago. Contacte con sistemas. TenderTypeId = " + tenderTypeId, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }
                InitializeComponent();
                if (Transaction.AmountDue < 0)
                {
                    numAmount.NegativeMode = true;
                }
                else
                    numAmount.NegativeMode = false;

                ControlTextBox();
                //ValidaFormasPago();
                if(!cargaFormasPago())
                {
                    this.Close();
                    return;
                }
                oPinPad = new PinPadAppliNet(apps);
            }
            catch (Exception ex)
            {
                CreaMensaje("Error en la carga de configuración. Contacte con sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        #endregion

        #region MEMBERS

        private bool cargaFormasPago()
        {
            bool retval = false;
            try
            {
                odac = new DAC(apps.Settings.Database.Connection);
                DtFormas = odac.getCardTenderTypes(apps.Settings.Database.DataAreaID);
                #region Validate Voided TenderTypes
                if (DtFormas.Rows.Count > 0)
                {
                    foreach (DataRow dr in DtFormas.Rows)
                    {
                        if (dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("CREDIT"))
                        {
                            formaPagoCredito = dr["RETAILCARDTYPEID"].ToString();
                        }
                        else if (dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("DEBIT"))
                        {
                            formaPagoDebito = dr["RETAILCARDTYPEID"].ToString();
                        }
                    }
                    if (!string.IsNullOrEmpty(formaPagoCredito) && !string.IsNullOrEmpty(formaPagoDebito))
                    retval = true;
                    else
                    {
                        CreaMensaje("Error al cargar las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    CreaMensaje("Error al cargar las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                #endregion
            }
            catch (Exception ex)
            {
                CreaMensaje("Error al cargar las formas de pago.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retval;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                try
                {
                    if (amtDue == 0)
                        amtDue = Transaction.AmountDue - Transaction.Payment;
                    numAmount.EnterButtonPressed += numAmount_EnterPressed;
                    lblTotalAmtDue.Text = "Prueba";
                    lblTotalAmtDue.Text = "Monto total de la deuda " + apps.Services.Rounding.Round(amtDue, false);
                    BtnAmt.Text = apps.Services.Rounding.Round(amtDue, false);
                    txtAmt.Text = apps.Services.Rounding.Round(amtDue, false);
                    System.DateTime moment = DateTime.Today;
                    System.DateTime newDate = new System.DateTime(moment.Year, moment.Month, 1);
                    txtexpDate.Format = DateTimePickerFormat.Custom;
                    txtexpDate.CustomFormat = "MM yyyy";
                    txtexpDate.ShowUpDown = true;
                    txtexpDate.Value = newDate;

                    //Inicializar variables para AppliNET
                    try
                    {
                        sesion = Convert.ToInt32(Transaction.Shift.BatchId.ToString("000000"));
                    }
                    catch
                    {
                        sesion = 0;
                    }

                    this.chkBoxDevolucion.Visible = false;
                    this.txtAutorizacion.Visible = false;
                    this.lblForzada.Visible = false;
                    this.comboBox1.Visible = false;
                    this.lblTipoPago.Visible = false;

                    if (numAmount.NegativeMode)
                    {
                        this.txtAutorizacion.Enabled = false;
                        this.chkBoxDevolucion.Visible = true;
                    }
                    InicializaPinPad();
                }
                catch (Exception ex)
                {
                    CreaMensaje("Ocurrió un error al inicializar la aplicación, reporte a sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                }
            }
        }

        private void configurarControles()
        {
            this.BtnAceptar.Enabled = false;
            this.BtnCancel.Enabled = false;
            this.txtName.Text = "Procesando...";
            this.numAmount.Enabled = false;
            this.txtAmt.Enabled = false;
            this.BtnAmt.Enabled = false;
            this.comboBox1.Enabled = false;

            if (!string.IsNullOrEmpty(txtAutorizacion.Text) && txtAutorizacion.Text != "")
            {
                if (txtAutorizacion.Text.Trim().Length == 6)
                {
                    esTransaccionForzada = true;
                    autorizacionForzada = txtAutorizacion.Text;
                }
                else
                {
                    CreaMensaje("La cantidad de dígitos para una transacción forzada debe ser 6. La transacción se enviará sin número de autorización.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            this.txtAutorizacion.Enabled = false;
        }

        private void crearSecuenciaTransaccion()
        {
            try
            {
                transaccion = Convert.ToInt32(DateTime.Now.TimeOfDay.TotalSeconds);
            }
            catch (Exception ex)
            {
                transaccion = 0;
            }
        }

        private void definirCamposApplinet()
        {
            int validador;
            if (!numAmount.NegativeMode)
            {
                if (txtAmt.Text.Contains("."))
                {
                    int centavos = 0;
                    string pesos = string.Empty;
                    string[] monto;
                    string cetavosString = string.Empty;
                    monto = txtAmt.Text.Split('.');
                    pesos = monto[0].Replace(",", "");
                    if (monto[1].Length == 1)
                    {
                        centavos = Convert.ToInt32(monto[1]) * 10;
                        importe = int.Parse(pesos + centavos.ToString());
                    }
                    else
                    {
                        cetavosString = monto[1];
                        importe = int.Parse(pesos + cetavosString);
                    }
                }
                else
                {
                    if (int.TryParse(txtAmt.Text, out validador))
                    {
                        importe = int.Parse(txtAmt.Text) * 100;
                    }
                }
            }
            cveUsuario = formatStringZeroValues(Transaction.Shift.StaffId, 6);
            if (Transaction.StoreCurrencyCode == "MXN")
            {
                tipoMoneda = 0;
            }
            else if (Transaction.StoreCurrencyCode == "USD")
            {
                tipoMoneda = 1;
            }
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                crearSecuenciaTransaccion();
                definirCamposApplinet();
                decimal amt = 0;
                //bool realizaPago = validaObligatorios(); 
                bool realizaPago = true;
                if (realizaPago)
                {
                    if (decimal.TryParse(txtAmt.Text.Trim(), out amt))
                    {
                        configurarControles();
                        //Venta
                        if (!numAmount.NegativeMode)
                        {
                            if ((Transaction.TransSalePmtDiff - (amt)) < 0)
                                CreaMensaje("Monto capturado sobrepasa el monto total a pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            else
                            {
                                if (cargaPinPadCorrecta)
                                {
                                    //Valida si hizo una transacción con meses
                                    bool usoPromociones = false;
                                    if (!esTransaccionForzada)
                                    {
                                        venta = oFormatString.venta(ClsConfiguracion.Afiliacion, ClsConfiguracion.Terminal, sesion, transaccion, importe, tipoMoneda, cveUsuario, 0, 0, 0);
                                        Enviar.fncEglobalBBVA(venta, 0);
                                    }
                                    else
                                    {
                                        venta_forzada = oFormatString.ventaForzada(ClsConfiguracion.Afiliacion, ClsConfiguracion.Terminal, sesion, transaccion, importe, tipoMoneda, cveUsuario, 0, 0, 0, txtAutorizacion.Text);
                                        Enviar.fncEglobalBBVA(venta_forzada, 0);
                                    }

                                    if (Enviar.RespDLL != 0)
                                    {
                                        if (Enviar.ClsResponse.C05_CodigoRespuesta == String.Empty)
                                        {
                                            CreaMensaje(Enviar.ClsResponse.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else
                                        {
                                            CreaMensaje(Enviar.ClsResponse.C16_Leyenda.Trim().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        Cerrar();
                                    }
                                    else
                                    {
                                        if ((Enviar.CardTipo == 1) && (Enviar.TnxConPuntos == false))
                                        {
                                            if (Enviar.CreditoDebito == "Debito")
                                            {
                                                Enviar.fncTerminaTipoCard(true, "000000000000", "00", "00", "00"); //Regresamos el control a la Dll
                                                cardTypeId = formaPagoDebito;
                                            }
                                            else
                                            {
                                                cardTypeId = formaPagoCredito;
                                                DialogResult Respuesta;
                                                Respuesta = MessageBox.Show("¿Desea agregar una promoción a meses? ", "Punto Venta",
                                                    MessageBoxButtons.YesNoCancel,
                                                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                                                if (Respuesta == DialogResult.Yes)
                                                {
                                                    using (frmPromoc frmPro = new frmPromoc())
                                                    {
                                                        this.apps.ApplicationFramework.POSShowForm(frmPro);

                                                        if (frmPro.DialogResult == DialogResult.OK)
                                                        {
                                                            if (frmPro.promoMes > 0)
                                                            {
                                                                mesesPagoParc = frmPro.promoMes;
                                                                //Siempre debe ser en true, despues el importe cash, despues el financiamiento, despues la parcializacion y por ultimo la promocion
                                                                CreaMensaje("Promoción: " + frmPro.promoMes + " Meses sin intereses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                                Enviar.fncTerminaTipoCard(true, "000000000000", "00", formatZeroValues(frmPro.promoMes, 2), "03"); //Regresamos el control a la Dll
                                                                usoPromociones = true;
                                                            }
                                                            else
                                                            {
                                                                CreaMensaje("Pago sin promociones de meses sin intereses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                                Enviar.fncTerminaTipoCard(true, "000000000000", "00", "00", "00"); //Regresamos el control a la Dll
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (Respuesta == DialogResult.No)
                                                {
                                                    Enviar.fncTerminaTipoCard(true, "000000000000", "00", "00", "00"); //Regresamos el control a la Dll
                                                }
                                                else
                                                {
                                                    Enviar.fncTerminaTipoCard(true, "000000000000", "00", "00", "00"); //Regresamos el control a la Dll
                                                }
                                            }
                                        }
                                        if (Enviar.TnxConPuntos == true && !usoPromociones)
                                        {
                                            DialogResult Respuesta;
                                            Respuesta = MessageBox.Show("¿Desea usar sus puntos para esta operación? ", "Punto Venta",
                                                MessageBoxButtons.YesNoCancel,
                                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                                            if (Respuesta == DialogResult.Yes)
                                            {
                                                Enviar.fncTerminaProceso(true);

                                            }
                                            else if (Respuesta == DialogResult.No)
                                            {
                                                Enviar.fncTerminaProceso(false);
                                            }
                                            else
                                            {
                                                Enviar.fncTerminaProceso(false);
                                                //Enviar.SincronizacionFinal();
                                            }
                                        }
                                        else
                                        {
                                            if (Enviar.CreditoDebito != "Debito")
                                            {
                                                Enviar.fncTerminaProceso(false);
                                            }
                                        }
                                        if ((Enviar.TBines != "") && (Enviar.TBines != null))
                                        {
                                            CreaMensaje("Enviar TBines = "+Enviar.TBines, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        }
                                        //Operación autorizada
                                        if ((Enviar.RespDLL == 0) && (Enviar.ClsResponse.C05_CodigoRespuesta == "00") && !string.IsNullOrEmpty(Enviar.ClsResponse.C06_NoAutorizacion) && Enviar.ClsResponse.C06_NoAutorizacion.Trim() != "")
                                        {
                                            txtAutorizacion.Text = Enviar.ClsResponse.C06_NoAutorizacion;
                                            txtCarnbr.Text = Enviar.NumeroTarjeta;
                                            txtName.Text = Enviar.NombreCliente;
                                            CreaMensaje(Enviar.ClsResponse.C16_Leyenda, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            imprimeTicketComercioVenta();
                                            imprimeTicketClienteVenta();
                                            DialogResult dialogResult = apps.Services.Dialog.ShowMessage("¿Su ticket se imprimió correctamente?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                            if (dialogResult == DialogResult.No)
                                            {
                                                imprimeTicketComercioVenta();
                                                imprimeTicketClienteVenta();
                                            }
                                            decimal totalPayed = decimal.Parse(this.txtAmt.Text.ToString().Replace(",", ""));
                                            guardaTransaccion(totalPayed);
                                            Cerrar();
                                        }
                                        //Operación declinada
                                        else
                                        {
                                            if (Enviar.ClsResponse.C05_CodigoRespuesta == String.Empty)
                                            {
                                                CreaMensaje("Operación AppliNET Declinada: " + Enviar.ClsResponse.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                            else
                                            {
                                                CreaMensaje("Operación AppliNET Declinada: " + Enviar.ClsResponse.C16_Leyenda.Trim().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                            imprimeError();
                                            Cerrar();
                                        }
                                    }
                                    Cerrar();
                                }
                                else
                                {
                                    Cerrar();
                                }
                            }
                        }
                        //Cancelación o Devolución
                        else
                        {
                            if (this.chkBoxDevolucion.Checked)
                            {
                                esDevolucion = true;
                            }
                            if (cargaPinPadCorrecta)
                            {
                                CargaCancelacion();
                                if (cargoCancelacion)
                                {
                                    if (!esDevolucion)
                                    {
                                        cancelacion_venta = oFormatString.cancelacion(ClsConfiguracion.Afiliacion, ClsConfiguracion.Terminal, sesion, int.Parse(secuenciaTransaccion), importe, tipoMoneda, cveUsuario, AutNumber, referencia);
                                        Enviar.fncEglobalBBVA(cancelacion_venta, 0);
                                    }
                                    else
                                    {
                                        devolucion = oFormatString.devolucion(ClsConfiguracion.Afiliacion, ClsConfiguracion.Terminal, sesion, int.Parse(secuenciaTransaccion), importe, tipoMoneda, cveUsuario, AutNumber, referencia);
                                        Enviar.fncEglobalBBVA(devolucion, 0);
                                    }
                                    //Operación autorizada
                                    if ((Enviar.RespDLL == 0) && (Enviar.ClsResponse.C05_CodigoRespuesta == "00") && !string.IsNullOrEmpty(Enviar.ClsResponse.C06_NoAutorizacion) && Enviar.ClsResponse.C06_NoAutorizacion.Trim() != "")
                                    {
                                        cardTypeId = formaPagoCredito;
                                        if (Enviar.CreditoDebito == "Debito")
                                        {
                                            cardTypeId = formaPagoDebito;
                                        }
                                        else
                                        {
                                            cardTypeId = formaPagoCredito;
                                        }
                                        txtAutorizacion.Text = Enviar.ClsResponse.C06_NoAutorizacion;
                                        txtCarnbr.Text = Enviar.NumeroTarjeta;
                                        txtName.Text = Enviar.NombreCliente;
                                        CreaMensaje(Enviar.ClsResponse.C16_Leyenda, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        imprimeTicketComercioCancelacion();
                                        imprimeTicketClienteCancelacion();
                                        DialogResult dialogResult = apps.Services.Dialog.ShowMessage("¿Su ticket se imprimió correctamente?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                        if (dialogResult == DialogResult.No)
                                        {
                                            imprimeTicketComercioCancelacion();
                                            imprimeTicketClienteCancelacion();
                                        }
                                        decimal totalPayed = decimal.Parse(this.txtAmt.Text.ToString().Replace(",", ""));
                                        guardaTransaccion(totalPayed);
                                        Cerrar();
                                    }
                                    //Operación declinada
                                    else
                                    {
                                        if (Enviar.ClsResponse.C05_CodigoRespuesta == String.Empty)
                                        {
                                            CreaMensaje("Operación AppliNET Declinada: " + Enviar.ClsResponse.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else
                                        {
                                            CreaMensaje("Operación AppliNET Declinada: " + Enviar.ClsResponse.C16_Leyenda.Trim().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        imprimeError();
                                        Cerrar();
                                    }
                                }
                                else
                                {
                                    Cerrar();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("Ha ocurrido un error durante la operación, favor de contactar con sistemas. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                CancelarOperacion();
            }
        }

        private void CargaObjetos()
        {
        }

        /// <summary>
        /// Inicializa la PinPad y hace el proceso de atualización automática o carga de llaves si es requerido
        /// </summary>
        private void InicializaPinPad()
        {
            try
            {
                crearSecuenciaTransaccion();
                //Enviar.SincronicacionInicial();
                if (!File.Exists(Application.StartupPath + "\\PinpadConfig.txt"))
                {
                    throw new Exception("No existe el archivo " + Application.StartupPath + "\\PinpadConfig.txt");
                }
                ClsConfiguracion.LeeDatosFromTXT();
                if (ClsConfiguracion.CargaLlave == 1)
                {
                    //Formato de la cadena
                    carga_llaves = oFormatString.cargaLlaves(ClsConfiguracion.Afiliacion, ClsConfiguracion.Terminal, sesion, transaccion);
                    Enviar.fncEglobalBBVA(carga_llaves, 0);
                    if (Enviar.RespDLL != 0)
                    {
                        if (Enviar.ClsResponse.C05_CodigoRespuesta == String.Empty)
                        {
                            CreaMensaje(Enviar.Response, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if (Enviar.ClsResponse.C05_CodigoRespuesta != "00")
                        {
                            CreaMensaje(Enviar.ClsResponse.C16_Leyenda.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        if (Enviar.ClsResponse.ToString() != "")
                        {
                            CreaMensaje(Enviar.ClsResponse.C16_Leyenda, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else if (ClsConfiguracion.BACTVersion == 1) //Para Actualizar la versión del PinPad Forma Automatica
                {
                    String CadEnviar = String.Empty;
                    DialogResult Respuesta;

                    Respuesta = MessageBox.Show("Se realizará actualización de versión");
                    if (Respuesta == DialogResult.OK)
                    {
                        //Formato de la cadena
                        CadEnviar = oFormatString.telecarga(ClsConfiguracion.Afiliacion, ClsConfiguracion.Terminal, sesion, transaccion);
                        Enviar.fncEglobalBBVA(CadEnviar, 0);
                        if (Enviar.RespDLL != 0)
                        {
                            if (Enviar.ClsResponse.C05_CodigoRespuesta == String.Empty)
                            {
                                CreaMensaje(Enviar.Response, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else if (Enviar.ClsResponse.C05_CodigoRespuesta != "00")
                            {
                                CreaMensaje(Enviar.ClsResponse.C16_Leyenda.Trim(), MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            if (Enviar.ClsResponse.ToString() != "")
                            {
                                String Mensaje = Enviar.ClsResponse.ToString();
                                CreaMensaje(Mensaje, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                    else if (Respuesta == DialogResult.Cancel)
                    {
                        //No enviamos la solicitud al PinPad
                        //y continua con el proceso normal
                    }
                }
                SA = "0";
                BCargaLlave = "0";
                BACT = "0";
                File.Delete(Application.StartupPath + "/PinpadConfig.txt");
                using (StreamWriter w = File.AppendText(Application.StartupPath + "/PinpadConfig.txt"))
                {
                    w.WriteLine("IPHOST:" + ClsConfiguracion.IpHost);
                    w.WriteLine("SOCKETHOST:" + ClsConfiguracion.PuertoHost);
                    w.WriteLine("PINPADTIMEOUT:" + ClsConfiguracion.PinpadTimeOut);
                    w.WriteLine("NOMBREPUERTO:" + ClsConfiguracion.PuertoSerie);
                    w.WriteLine("PINPADID:" + ClsConfiguracion.PinPadID);
                    w.WriteLine("AFILIACION:" + ClsConfiguracion.Afiliacion);
                    w.WriteLine("TERMINAL:" + ClsConfiguracion.Terminal);
                    w.WriteLine("TIMEOUTHOST:" + ClsConfiguracion.HostTimeOut);
                    w.WriteLine("NOMBRECOMERCIO:" + ClsConfiguracion.Comercio);
                    w.WriteLine("HEADERCONSECUTIVO:" + ClsConfiguracion.HeaderConsecutivo);
                    w.WriteLine("FOLIO:" + ClsConfiguracion.IsFolio);
                    w.WriteLine("LOG:" + ClsConfiguracion.IsLog);
                    w.WriteLine("DIRUSERDB:" + ClsConfiguracion.DirUserDB);
                    w.WriteLine("BINESCOMERCIO:" + ClsConfiguracion.IDBinesComercio);
                    w.WriteLine("SESIONABRIR:" + SA);
                    w.WriteLine("CARGALLAVE:" + BCargaLlave);
                    w.WriteLine("VERSION:" + ClsConfiguracion.Password); //Solo si la DLL es por Internet
                    w.WriteLine("IMPRESIOND:" + ClsConfiguracion.Impresion);
                    w.WriteLine("ACTUALIZACION:" + BACT);
                    w.WriteLine("DIRACTPINPAD:" + ClsConfiguracion.DIRACTVersion);
                    w.WriteLine("CONAUX:" + ClsConfiguracion.ConAMEX);
                    w.WriteLine("CASH:" + ClsConfiguracion.Cash);
                    w.Flush();
                    w.Close();
                }
                cargaPinPadCorrecta = true;
            }
            catch (Exception ex)
            {
                CreaMensaje("Ocurrió un error al inicializar la PinPad, reporte a sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                CancelarOperacion();
            }
        }

        private void VerificaRespuestaVenta()
        {

        }

        private void VerificaRespuestaCancelacion()
        {

        }

        /// <summary>
        /// Controlar los campos que se llenarán automáticamente con MIT
        /// </summary>
        private void ControlTextBox()
        {
            txtName.Enabled = false;
            txtCarnbr.Enabled = false;
            txtAutorizacion.Enabled = true;
            txtSecCod.Enabled = false;
            txtexpDate.Enabled = false;
        }

        /// <summary>
        /// Método para restringir formas de pago
        /// </summary>
        private void ValidaFormasPago()
        {
            try
            {
                odac = new DAC(apps.Settings.Database.Connection);
                DtFormas = odac.getCardPayments(apps.Shift.StoreId, apps.Settings.Database.DataAreaID);
                #region Validate Voided TenderTypes
                if (DtFormas.Rows.Count > 0)
                {
                    foreach (DataRow dr in DtFormas.Rows)
                    {
                        if (dr["DESCRIPCION"].ToString().ToUpperInvariant().Contains("MONEDERO") || dr["DESCRIPCION"].ToString().ToUpperInvariant().Contains("REGALO"))
                        {
                            dr.Delete();
                        }
                    }
                    comboBox1.DisplayMember = "DESCRIPCION";
                    comboBox1.ValueMember = "TenderId";
                    comboBox1.DataSource = DtFormas;
                }
                else
                {
                    CreaMensaje("Error al cargar las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                }
                #endregion
            }
            catch (Exception ex)
            {
                CreaMensaje("Error al cargar las formas de pago.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cerrar();
            }
        }

        private void numAmount_EnterPressed()
        {
            txtAmt.Text = numAmount.EnteredDecimalValue.ToString();
        }

        private bool ValidaObligatorios()
        {
            if (txtCarnbr.Text.Trim() == "" || txtAutorizacion.Text.Trim() == "")
            {
                CreaMensaje("Por favor llenar los campos obligatorios", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (!ValidaDigitos())
            {
                return false;
            }
            else
                return true;
        }

        private bool ValidaDigitos()
        {
            if (txtCarnbr.Text.Trim() != "")
            {
                string tempString = string.Empty;
                long IntegerTemp = 0;
                tempString = txtCarnbr.Text.Trim();
                if (tempString.Length != 4 || !long.TryParse(tempString, out IntegerTemp))
                {
                    CreaMensaje("El número de dígitos del número de la tarjeta no es válido. Escriba únicamente los últimos 4 dígitos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return true;
        }

        private void AsignaFechaTarjeta()
        {

        }

        private void BtnAmt_Click_1(object sender, EventArgs e)
        {
            txtAmt.Text = apps.Services.Rounding.Round(amtDue, false);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView DrV;
            string TempValue = string.Empty;
            DrV = (DataRowView)comboBox1.SelectedItem;
            TempValue = DrV.Row.ItemArray[1].ToString().ToUpperInvariant();
        }

        private void txtCarnbr_TextChanged(object sender, EventArgs e)
        {

        }

        private void CargaCancelacion()
        {
            try
            {
                AutNumber = string.Empty;
                OperationNumber = string.Empty;
                ReturnTransId = string.Empty;
                ReturnTerminalId = string.Empty;
                ReturnStoreId = string.Empty;
                if (Transaction.SaleItems.Count > 0)
                {
                    foreach (SaleLineItem item in Transaction.SaleItems)
                    {
                        if (!item.Voided)
                        {
                            if (!string.IsNullOrEmpty(item.ReturnTransId) && !string.IsNullOrEmpty(item.ReturnTerminalId) && !string.IsNullOrEmpty(item.ReturnStoreId))
                            {
                                ReturnTransId = item.ReturnTransId;
                                ReturnTerminalId = Transaction.Shift.TerminalId;
                                ReturnStoreId = item.ReturnStoreId;
                                continue;
                            }
                        }
                    }
                }
                DAC odac = new DAC(new SqlConnection(apps.Settings.Database.Connection.ConnectionString));
                DataTable DtConfig = odac.selectData("Select [NUMEROTARJETA],[IMPORTE],[C06_NOAUTORIZACION],[C04_SECUENCIA],[C17_REFERENCIAFINAN],[FECHAHORA] from [ax].[GRW_APPLINETTRAN] where DataAreaId = '" + apps.Settings.Database.DataAreaID + "' AND TRANSACIONID = '" + ReturnTransId + "' AND STOREID = '" + ReturnStoreId + "' AND TERMINAL = '" + ReturnTerminalId + "'");
                if (DtConfig.Rows.Count > 0)
                {
                    using (frmTran nfrmTran = new frmTran(DtConfig, decimal.Parse(this.txtAmt.Text.ToString().Replace(",", "")), esDevolucion))
                    {
                        LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(nfrmTran);
                        if (nfrmTran.DialogResult == DialogResult.OK)
                        {
                            if (!string.IsNullOrEmpty(nfrmTran.selectedReference))
                            {
                                AutNumber = nfrmTran.selectedAuth;
                                referencia = nfrmTran.selectedReference;
                                secuenciaTransaccion = nfrmTran.selectedSecuence;

                                CANCEL_AMOUNT = nfrmTran.selectedAmount;
                                #region formatoMonto
                                //Guarda el monto en la variable para la transacción con AppliNET
                                CANCEL_AMOUNT = CANCEL_AMOUNT.Replace(",", "");
                                int validador;
                                if (CANCEL_AMOUNT.Contains("."))
                                {
                                    int centavos = 0;
                                    string pesos = string.Empty;
                                    string[] monto;
                                    string cetavosString = string.Empty;

                                    monto = CANCEL_AMOUNT.Split('.');
                                    pesos = monto[0];
                                    if (monto[1].Length == 1)
                                    {
                                        centavos = Convert.ToInt32(monto[1]) * 10;
                                        importe = int.Parse(pesos + centavos.ToString());
                                    }
                                    else
                                    {
                                        cetavosString = monto[1];
                                        importe = int.Parse(pesos + cetavosString);
                                    }
                                }
                                else
                                {
                                    if (int.TryParse(CANCEL_AMOUNT, out validador))
                                    {
                                        importe = int.Parse(CANCEL_AMOUNT) * 100;
                                    }
                                }
                                #endregion
                                CANCEL_AMOUNT = decimal.Negate(decimal.Parse(CANCEL_AMOUNT.Replace(",", ""))).ToString();
                                this.txtAmt.Text = CANCEL_AMOUNT;
                                amtDue = decimal.Parse(CANCEL_AMOUNT);
                                lblTotalAmtDue.Text = "Monto total de la deuda " + CANCEL_AMOUNT;
                                BtnAmt.Text = CANCEL_AMOUNT;
                                cargoCancelacion = true;
                            }
                        }
                    }
                }
                else
                {
                    CreaMensaje("No se pudo cargar la transacción de cancelación, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.BtnAceptar.Enabled = false;
                    CancelarOperacion();
                    Cerrar();
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("No se pudo cargar la transacción de cancelación.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.BtnAceptar.Enabled = false;
                CancelarOperacion();
                Cerrar();
            }
        }

        private void BtnCancel_Click_1(object sender, EventArgs e)
        {
            Cerrar();
        }

        private void imprimeTicketClienteVenta()
        {
            reportLayout = new StringBuilder(2500);
            reportLayout.AppendLine("              B B V A - B A N C O M E R");
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine(oPinPad.linea1);
            reportLayout.AppendLine(oPinPad.linea2);
            reportLayout.AppendLine(oPinPad.linea3);
            reportLayout.AppendLine(oPinPad.linea4);
            reportLayout.AppendLine();
            reportLayout.AppendLine("         AFILIACIÓN: " + ClsConfiguracion.Afiliacion);
            reportLayout.AppendLine();
            reportLayout.AppendLine("FECHA : " + DateTime.Now.ToShortDateString() + "        HORA: " + DateTime.Now.ToShortTimeString());
            if (Enviar.NumeroTarjeta.Trim() != "")
            {
                string tarjetaTrunc = string.Empty;
                for (int x = 0; x < Enviar.NumeroTarjeta.Length; x++)
                {
                    if (x < Enviar.NumeroTarjeta.Length - 4)
                    {
                        tarjetaTrunc += "*";
                    }
                    else
                    {
                        tarjetaTrunc += Enviar.NumeroTarjeta[x];
                    }
                }
                reportLayout.AppendLine("        Tarjeta:" + tarjetaTrunc);
            }
            reportLayout.AppendLine("             " + Enviar.NombreAplicacion);

            if (mesesPagoParc > 0)
            {
                reportLayout.AppendLine("VENTA " + mesesPagoParc + " MESES SIN INTERESES");
            }
            else
            {
                reportLayout.AppendLine("VENTA");
            }
            reportLayout.AppendLine("TOTAL M. N.");
            reportLayout.AppendLine("$" + txtAmt.Text);
            if (Enviar.Puntos.PesosRedimidos.Trim() != "" && int.Parse(Enviar.Puntos.PesosRedimidos.Trim()) > 0)
            {
                reportLayout.AppendLine("Pagando con puntos: $" + (Convert.ToDecimal(Enviar.Puntos.PesosRedimidos) / 100).ToString("##.##"));
                reportLayout.AppendLine("Total a Pagar:      $" + ((Convert.ToDecimal(txtAmt.Text)) - (Convert.ToDecimal((Convert.ToDecimal(Enviar.Puntos.PesosRedimidos) / 100).ToString("##.##")))).ToString());
                reportLayout.AppendLine();
                reportLayout.AppendLine("PUNTOS BANCOMER");
                reportLayout.AppendLine("Saldo anterior: " + Int32.Parse(Enviar.Puntos.PuntosSaldoAnterior).ToString() + " Pts, $" + (Convert.ToDecimal(Enviar.Puntos.PesosSaldoAnterior) / 100).ToString("##.##") + " Pesos");
                reportLayout.AppendLine("Saldo redimido: " + Int32.Parse(Enviar.Puntos.PuntosRedimidos).ToString() + " Pts, $" + (Convert.ToDecimal(Enviar.Puntos.PesosRedimidos) / 100).ToString("##.##") + " Pesos");
                reportLayout.AppendLine("Saldo actual: " + Int32.Parse(Enviar.Puntos.PuntosSaldoDisponible).ToString() + " Pts, $" + (Convert.ToDecimal(Enviar.Puntos.PesosSaldoDisponible) / 100).ToString("##.##") + " Pesos");
            }
            reportLayout.AppendLine();
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine("            APROBADA: " + Enviar.ClsResponse.C06_NoAutorizacion);
            reportLayout.AppendLine();
            if (Enviar.ModoIngreso.Trim() != "")
            {
                string ModoIngresoImpresion = string.Empty;
                if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
                {
                    ModoIngresoImpresion = "D@1";
                }
                else if (Enviar.ModoIngreso == "05")
                {
                    ModoIngresoImpresion = "I@1";
                }
                else if (Enviar.ModoIngreso == "01")
                {
                    ModoIngresoImpresion = "T@1";
                }
                reportLayout.AppendLine("              " + ModoIngresoImpresion + " " + Enviar.Criptograma);
            }
            reportLayout.AppendLine(" ");
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine();
            reportLayout.AppendLine("SECTRX: " + Enviar.ClsResponse.C04_secuencia);
            if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
            {

            }
            else
            {
                reportLayout.AppendLine("AID: " + Enviar.AID);
            }
            
            reportLayout.AppendLine("REF: " + Enviar.ClsResponse.C17_referenciafinan);
            reportLayout.AppendLine("TERMINAL: " + Transaction.TerminalId);
            reportLayout.AppendLine("TIENDA: " + Transaction.StoreId);
            reportLayout.AppendLine("POSTRAN: " + Transaction.TransactionId);
            reportLayout.AppendLine();
            reportLayout.AppendLine("Por este pagaré me obligo incondicionalmente");
            reportLayout.AppendLine("a pagar a la orden del banco acreditante el");
            reportLayout.AppendLine("importe de este título. Este pagaré procede del");
            reportLayout.AppendLine("contrato de apertura de crédito que el banco");
            reportLayout.AppendLine("acreditante y el tarjetahabiente tienen celebrado");
            reportLayout.AppendLine();
            if (Enviar.FirmaQPS)
            {
                reportLayout.AppendLine("               AUTORIZADO SIN FIRMA");
            }
            if (Enviar.FirmaAutografa)
            {
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine(singleLine);
                reportLayout.AppendLine("                       FIRMA");
            }
            if (Enviar.FirmaElectronica)
            {
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine("AUTORIZADO MEDIANTE FIRMA ELECTRÓNICA");
            }
            reportLayout.AppendLine();
            reportLayout.AppendLine("    Pagaré negociable únicamente en instituciones");
            reportLayout.AppendLine("                   de crédito");
            reportLayout.AppendLine();
            reportLayout.AppendLine("                **COPIA CLIENTE**");
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            if (!ApplicationSettings.Terminal.TrainingMode)
            {
                apps.Services.Peripherals.Printer.PrintReceipt(reportLayout.ToString());
            }
        }

        private void imprimeTicketComercioVenta()
        {
            reportLayout = new StringBuilder(2500);
            reportLayout.AppendLine("              B B V A - B A N C O M E R");
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine(oPinPad.linea1);
            reportLayout.AppendLine(oPinPad.linea2);
            reportLayout.AppendLine(oPinPad.linea3);
            reportLayout.AppendLine(oPinPad.linea4);
            reportLayout.AppendLine();
            reportLayout.AppendLine("         AFILIACIÓN: " + ClsConfiguracion.Afiliacion);
            reportLayout.AppendLine();
            reportLayout.AppendLine("FECHA : " + DateTime.Now.ToShortDateString() + "        HORA: " + DateTime.Now.ToShortTimeString());
            if (Enviar.NumeroTarjeta.Trim() != "")
            {
                string tarjetaTrunc = string.Empty;
                for (int x = 0; x < Enviar.NumeroTarjeta.Length; x++)
                {
                    if (x < Enviar.NumeroTarjeta.Length - 4)
                    {
                        tarjetaTrunc += "*";
                    }
                    else
                    {
                        tarjetaTrunc += Enviar.NumeroTarjeta[x];
                    }
                }
                reportLayout.AppendLine("        Tarjeta:" + tarjetaTrunc);
            }
            reportLayout.AppendLine("             " + Enviar.NombreAplicacion);
            if (mesesPagoParc > 0)
            {
                reportLayout.AppendLine("VENTA " + mesesPagoParc + " MESES SIN INTERESES");
            }
            else
            {
                reportLayout.AppendLine("VENTA");
            }
            reportLayout.AppendLine("TOTAL M. N.");
            reportLayout.AppendLine("$" + txtAmt.Text);
            reportLayout.AppendLine();
            if (Enviar.Puntos.PesosRedimidos.Trim() != "" && int.Parse(Enviar.Puntos.PesosRedimidos.Trim()) > 0)
            {
                reportLayout.AppendLine("Pagando con puntos: $" + (Convert.ToDecimal(Enviar.Puntos.PesosRedimidos) / 100).ToString("##.##"));
                reportLayout.AppendLine("Total a Pagar:      $" + ((Convert.ToDecimal(txtAmt.Text)) - (Convert.ToDecimal((Convert.ToDecimal(Enviar.Puntos.PesosRedimidos) / 100).ToString("##.##")))).ToString());
                reportLayout.AppendLine();
            }
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine("                 APROBADA: " + Enviar.ClsResponse.C06_NoAutorizacion);
            reportLayout.AppendLine();
            if (Enviar.ModoIngreso.Trim() != "")
            {
                string ModoIngresoImpresion = string.Empty;
                if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
                {
                    ModoIngresoImpresion = "D@1";
                }
                else if (Enviar.ModoIngreso == "05")
                {
                    ModoIngresoImpresion = "I@1";
                }
                else if (Enviar.ModoIngreso == "01")
                {
                    ModoIngresoImpresion = "T@1";
                }
                reportLayout.AppendLine("              " + ModoIngresoImpresion + " " + Enviar.Criptograma);
            }
            reportLayout.AppendLine(" ");
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine();
            reportLayout.AppendLine("SECTRX: " + Enviar.ClsResponse.C04_secuencia);
            if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
            {

            }
            else
            {
                reportLayout.AppendLine("AID: " + Enviar.AID);
            }
            reportLayout.AppendLine("REF: " + Enviar.ClsResponse.C17_referenciafinan);
            reportLayout.AppendLine("TERMINAL: " + Transaction.TerminalId);
            reportLayout.AppendLine("TIENDA: " + Transaction.StoreId);
            reportLayout.AppendLine("POSTRAN: " + Transaction.TransactionId);
            reportLayout.AppendLine();
            reportLayout.AppendLine("Por este pagaré me obligo incondicionalmente");
            reportLayout.AppendLine("a pagar a la orden del banco acreditante el");
            reportLayout.AppendLine("importe de este título. Este pagaré procede del");
            reportLayout.AppendLine("contrato de apertura de crédito que el banco");
            reportLayout.AppendLine("acreditante y el tarjetahabiente tienen celebrado");
            reportLayout.AppendLine();
            if (Enviar.FirmaQPS)
            {
                reportLayout.AppendLine("                 AUTORIZADO SIN FIRMA");
            }
            if (Enviar.FirmaAutografa)
            {
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine(singleLine);
                reportLayout.AppendLine("                       FIRMA");
            }
            if (Enviar.FirmaElectronica)
            {
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine("     AUTORIZADO MEDIANTE FIRMA ELECTRÓNICA");
            }

            reportLayout.AppendLine();
            reportLayout.AppendLine("     Pagaré negociable únicamente en instituciones");
            reportLayout.AppendLine("                     de crédito");
            reportLayout.AppendLine();
            reportLayout.AppendLine("                  ORIGINAL COMERCIO");
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            if (!ApplicationSettings.Terminal.TrainingMode)
            {
                apps.Services.Peripherals.Printer.PrintReceipt(reportLayout.ToString());
            }
        }

        private void imprimeTicketComercioCancelacion()
        {
            reportLayout = new StringBuilder(2500);
            reportLayout.AppendLine("              B B V A - B A N C O M E R");
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine(oPinPad.linea1);
            reportLayout.AppendLine(oPinPad.linea2);
            reportLayout.AppendLine(oPinPad.linea3);
            reportLayout.AppendLine(oPinPad.linea4);
            reportLayout.AppendLine();
            reportLayout.AppendLine("         AFILIACIÓN: " + ClsConfiguracion.Afiliacion);
            reportLayout.AppendLine();
            reportLayout.AppendLine("FECHA : " + DateTime.Now.ToShortDateString() + "        HORA: " + DateTime.Now.ToShortTimeString());
            if (Enviar.NumeroTarjeta.Trim() != "")
            {
                string tarjetaTrunc = string.Empty;
                for (int x = 0; x < Enviar.NumeroTarjeta.Length; x++)
                {
                    if (x < Enviar.NumeroTarjeta.Length - 4)
                    {
                        tarjetaTrunc += "*";
                    }
                    else
                    {
                        tarjetaTrunc += Enviar.NumeroTarjeta[x];
                    }
                }
                reportLayout.AppendLine("        Tarjeta:" + tarjetaTrunc);
            }
            reportLayout.AppendLine("             " + Enviar.NombreAplicacion);
            if (esDevolucion)
            {
                reportLayout.AppendLine("DEVOLUCION");
            }
            else
            {
                reportLayout.AppendLine("CANCELACION");
            }
            reportLayout.AppendLine("TOTAL M. N.");
            reportLayout.AppendLine("$" + txtAmt.Text);
            reportLayout.AppendLine();
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine("                 APROBADA: " + Enviar.ClsResponse.C06_NoAutorizacion);
            reportLayout.AppendLine();
            if (Enviar.ModoIngreso.Trim() != "")
            {
                string ModoIngresoImpresion = string.Empty;
                if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
                {
                    ModoIngresoImpresion = "D@1";
                }
                else if (Enviar.ModoIngreso == "05")
                {
                    ModoIngresoImpresion = "I@1";
                }
                else if (Enviar.ModoIngreso == "01")
                {
                    ModoIngresoImpresion = "T@1";
                }
                reportLayout.AppendLine("              " + ModoIngresoImpresion + " " + Enviar.Criptograma);
            }
            reportLayout.AppendLine(" ");
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine();
            reportLayout.AppendLine("SECTRX: " + Enviar.ClsResponse.C04_secuencia);
            if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
            {

            }
            else
            {
                reportLayout.AppendLine("AID: " + Enviar.AID);
            }
            reportLayout.AppendLine("REF: " + Enviar.ClsResponse.C17_referenciafinan);
            reportLayout.AppendLine("TERMINAL: " + Transaction.TerminalId);
            reportLayout.AppendLine("TIENDA: " + Transaction.StoreId);
            reportLayout.AppendLine("POSTRAN: " + Transaction.TransactionId);
            reportLayout.AppendLine();
            reportLayout.AppendLine("Por este pagaré me obligo incondicionalmente");
            reportLayout.AppendLine("a pagar a la orden del banco acreditante el");
            reportLayout.AppendLine("importe de este título. Este pagaré procede del");
            reportLayout.AppendLine("contrato de apertura de crédito que el banco");
            reportLayout.AppendLine("acreditante y el tarjetahabiente tienen celebrado");
            reportLayout.AppendLine();
            if (Enviar.FirmaQPS)
            {
                reportLayout.AppendLine("                 AUTORIZADO SIN FIRMA");
            }
            if (Enviar.FirmaAutografa)
            {
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine(singleLine);
                reportLayout.AppendLine("                       FIRMA");
            }
            if (Enviar.FirmaElectronica)
            {
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine("     AUTORIZADO MEDIANTE FIRMA ELECTRÓNICA");
            }

            reportLayout.AppendLine();
            reportLayout.AppendLine("     Pagaré negociable únicamente en instituciones");
            reportLayout.AppendLine("                     de crédito");
            reportLayout.AppendLine();
            reportLayout.AppendLine("                  ORIGINAL COMERCIO");
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            if (!ApplicationSettings.Terminal.TrainingMode)
            {
                apps.Services.Peripherals.Printer.PrintReceipt(reportLayout.ToString());
            }
        }

        private void imprimeTicketClienteCancelacion()
        {
            reportLayout = new StringBuilder(2500);
            reportLayout.AppendLine("              B B V A - B A N C O M E R");
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine(oPinPad.linea1);
            reportLayout.AppendLine(oPinPad.linea2);
            reportLayout.AppendLine(oPinPad.linea3);
            reportLayout.AppendLine(oPinPad.linea4);
            reportLayout.AppendLine();
            reportLayout.AppendLine("         AFILIACIÓN: " + ClsConfiguracion.Afiliacion);
            reportLayout.AppendLine();
            reportLayout.AppendLine("FECHA : " + DateTime.Now.ToShortDateString() + "        HORA: " + DateTime.Now.ToShortTimeString());
            if (Enviar.NumeroTarjeta.Trim() != "")
            {
                string tarjetaTrunc = string.Empty;
                for (int x = 0; x < Enviar.NumeroTarjeta.Length; x++)
                {
                    if (x < Enviar.NumeroTarjeta.Length - 4)
                    {
                        tarjetaTrunc += "*";
                    }
                    else
                    {
                        tarjetaTrunc += Enviar.NumeroTarjeta[x];
                    }
                }
                reportLayout.AppendLine("        Tarjeta:" + tarjetaTrunc);
            }
            reportLayout.AppendLine("             " + Enviar.NombreAplicacion);
            if (esDevolucion)
            {
                reportLayout.AppendLine("DEVOLUCION");
            }
            else
            {
                reportLayout.AppendLine("CANCELACION");
            }
            reportLayout.AppendLine("TOTAL M. N.");
            reportLayout.AppendLine("$" + txtAmt.Text);
            reportLayout.AppendLine();
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine("                 APROBADA: " + Enviar.ClsResponse.C06_NoAutorizacion);
            reportLayout.AppendLine();
            if (Enviar.ModoIngreso.Trim() != "")
            {
                string ModoIngresoImpresion = string.Empty;
                if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
                {
                    ModoIngresoImpresion = "D@1";
                }
                else if (Enviar.ModoIngreso == "05")
                {
                    ModoIngresoImpresion = "I@1";
                }
                else if (Enviar.ModoIngreso == "01")
                {
                    ModoIngresoImpresion = "T@1";
                }
                reportLayout.AppendLine("              " + ModoIngresoImpresion + " " + Enviar.Criptograma);
            }
            reportLayout.AppendLine(" ");
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine();
            reportLayout.AppendLine("SECTRX: " + Enviar.ClsResponse.C04_secuencia);
            if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
            {

            }
            else
            {
                reportLayout.AppendLine("AID: " + Enviar.AID);
            }
            reportLayout.AppendLine("REF: " + Enviar.ClsResponse.C17_referenciafinan);
            reportLayout.AppendLine("TERMINAL: " + Transaction.TerminalId);
            reportLayout.AppendLine("TIENDA: " + Transaction.StoreId);
            reportLayout.AppendLine("POSTRAN: " + Transaction.TransactionId);
            reportLayout.AppendLine();
            reportLayout.AppendLine("Por este pagaré me obligo incondicionalmente");
            reportLayout.AppendLine("a pagar a la orden del banco acreditante el");
            reportLayout.AppendLine("importe de este título. Este pagaré procede del");
            reportLayout.AppendLine("contrato de apertura de crédito que el banco");
            reportLayout.AppendLine("acreditante y el tarjetahabiente tienen celebrado");
            reportLayout.AppendLine();
            if (Enviar.FirmaQPS)
            {
                reportLayout.AppendLine("                 AUTORIZADO SIN FIRMA");
            }
            if (Enviar.FirmaAutografa)
            {
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine(singleLine);
                reportLayout.AppendLine("                       FIRMA");
            }
            if (Enviar.FirmaElectronica)
            {
                reportLayout.AppendLine();
                reportLayout.AppendLine();
                reportLayout.AppendLine("     AUTORIZADO MEDIANTE FIRMA ELECTRÓNICA");
            }

            reportLayout.AppendLine();
            reportLayout.AppendLine("     Pagaré negociable únicamente en instituciones");
            reportLayout.AppendLine("                     de crédito");
            reportLayout.AppendLine();
            reportLayout.AppendLine("                  COPIA CLIENTE");
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            if (!ApplicationSettings.Terminal.TrainingMode)
            {
                apps.Services.Peripherals.Printer.PrintReceipt(reportLayout.ToString());
            }
        }

        private void imprimeError()
        {
            reportLayout = new StringBuilder(2500);
            reportLayout.AppendLine("              B B V A - B A N C O M E R");
            reportLayout.AppendLine();
            reportLayout.AppendLine(oPinPad.linea1);
            reportLayout.AppendLine(oPinPad.linea2);
            reportLayout.AppendLine(oPinPad.linea3);
            reportLayout.AppendLine(oPinPad.linea4);
            reportLayout.AppendLine();
            reportLayout.AppendLine("         AFILIACIÓN: " + ClsConfiguracion.Afiliacion);
            reportLayout.AppendLine();
            reportLayout.AppendLine("FECHA : " + DateTime.Now.ToShortDateString() + "        HORA: " + DateTime.Now.ToShortTimeString());
            if (Enviar.NumeroTarjeta.Trim() != "")
            {
                string tarjetaTrunc = string.Empty;
                for (int x = 0; x < Enviar.NumeroTarjeta.Length; x++)
                {
                    if (x < Enviar.NumeroTarjeta.Length - 4)
                    {
                        tarjetaTrunc += "*";
                    }
                    else
                    {
                        tarjetaTrunc += Enviar.NumeroTarjeta[x];
                    }
                }
                reportLayout.AppendLine("        Tarjeta:" + tarjetaTrunc);
            }
            reportLayout.AppendLine("             " + Enviar.NombreAplicacion);
            if (!numAmount.NegativeMode)
            {
                reportLayout.AppendLine("VENTA");
            }
            else
            {
                if (esDevolucion)
                {
                    reportLayout.AppendLine("DEVOLUCION");
                }
                else
                {
                    reportLayout.AppendLine("CANCELACION");
                }
            }
            reportLayout.AppendLine();
            reportLayout.AppendLine("                 DECLINADA");
            reportLayout.AppendLine(centrarTexto(Enviar.ClsResponse.C16_Leyenda.Trim()));
            if (Enviar.ModoIngreso.Trim() != "")
            {
                string ModoIngresoImpresion = string.Empty;
                if (Enviar.ModoIngreso == "90" || Enviar.ModoIngreso == "80")
                {
                    ModoIngresoImpresion = "D@1";
                }
                else if (Enviar.ModoIngreso == "05")
                {
                    ModoIngresoImpresion = "I@1";
                }
                else if (Enviar.ModoIngreso == "01")
                {
                    ModoIngresoImpresion = "T@1";
                }

                reportLayout.AppendLine("       " + ModoIngresoImpresion + " " + Enviar.Criptograma);
            }
            reportLayout.AppendLine(" ");
            reportLayout.AppendLine(singleLine);
            reportLayout.AppendLine();
            reportLayout.AppendLine("SECTRX: " + Enviar.ClsResponse.C04_secuencia);
            reportLayout.AppendLine("REF: " + Enviar.ClsResponse.C17_referenciafinan);
            reportLayout.AppendLine("TERMINAL: " + Transaction.TerminalId);
            reportLayout.AppendLine("TIENDA: " + Transaction.StoreId);
            reportLayout.AppendLine("POSTRAN: " + Transaction.TransactionId);
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            if (!ApplicationSettings.Terminal.TrainingMode)
            {
                apps.Services.Peripherals.Printer.PrintReceipt(reportLayout.ToString());
            }
        }

         private string centrarTexto(string _linea)
         {
             string retVal = string.Empty;
             int MEDIDAPAPEL = 51;
             try
             {
                 int conteoPalabras = 0;
                 int conteoLetras = 0;
                 string[] parameters;
                 parameters = _linea.Split(' ');
                 conteoPalabras = parameters.Count();
                 for (int i = 0; i < parameters.Count(); i++)
                 {
                     conteoLetras += parameters[i].Length;
                 }
                 if (conteoLetras > 0 && conteoPalabras > 0)
                 {
                     string espaciosString = string.Empty;
                     int espacios = (MEDIDAPAPEL - conteoLetras) / (2);
                     string palabrasunidas = string.Empty;
                     string retornoFinal = string.Empty;
                     for (int i = 0; i < espacios; i++)
                     {
                         espaciosString += " ";
                     }
                     for (int j = 0; j < conteoPalabras; j++)
                     {
                         if (parameters[j] != string.Empty)
                             palabrasunidas += parameters[j] + " ";
                     }
                     retornoFinal = espaciosString + palabrasunidas;
                     retVal = retornoFinal;
                 }
             }
             catch (Exception ex)
             {
                 CreaMensaje("Error al centrar el texto del ticket AppliNET. " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
             }
             return retVal;
         }
         
        private void guardaTransaccion(decimal amount)
        {
            Tender t = new Tender();
            //tendertype = comboBox1.SelectedValue.ToString();
            t = new TenderData(ApplicationSettings.Database.LocalConnection, ApplicationSettings.Database.DATAAREAID).GetTender(tenderTypeId, ApplicationSettings.Terminal.StoreId);
            LSRetailPosis.Transaction.Line.TenderItem.TenderLineItem lt = new LSRetailPosis.Transaction.Line.TenderItem.TenderLineItem();
            lt.Amount = amount;
            lt.Comment = "Pago AppliNET";
            lt.Description = t.TenderName;
            lt.AboveMinimumTenderId = t.AboveMinimumTenderId;
            lt.ChangeTenderID = t.ChangeTenderID;
            lt.TenderTypeId = t.TenderID;

            Transaction.Add(lt, true);
            Transaction.CalcTotals();
            Transaction.CalculateAmountDue();
            Transaction.Save();
            bool retval = false;
            retval = odac.GuardaPagoTarjeta(apps.BusinessLogic.Utility.MaskCardNumber(Enviar.NumeroTarjeta), Enviar.NombreCliente, ApplicationSettings.Terminal.StoreCurrency, Enviar.NombreAplicacion, Enviar.CreditoDebito, Enviar.ClsResponse.C17_referenciafinan, txtAmt.Text.Replace(",", ""), Enviar.ClsResponse.C07_Afiliacion, Enviar.ClsResponse.C06_NoAutorizacion, Enviar.ClsResponse.C04_secuencia, Enviar.ClsResponse.C01_CodTransaction, Enviar.ClsResponse.C02_Teminal, apps.Settings.Database.DataAreaID, Transaction.StoreId, Transaction.Shift.TerminalId, Transaction.TransactionId, lt.LineId, Transaction.Shift.BatchId, Transaction.Shift.StaffId);
            if (retval == false)
            {
                CreaMensaje("Error al guardar la transacción, favor de contactar con sistemas, la operación continuará.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            odac.GuardaDetalletarjeta(ApplicationSettings.Database.StoreID, ApplicationSettings.Database.DATAAREAID, Transaction.Shift.TerminalId, Transaction.TenderLines.Last.Value.LineId, tenderTypeId, cardTypeId, Transaction.TransactionId);

            if ((Transaction.TransSalePmtDiff) == 0)
            {
                LSRetailPosis.POSProcesses.PayCash payC = new LSRetailPosis.POSProcesses.PayCash();
                payC.Amount = 0;
                payC.OperationID = PosisOperations.PayCash;
                payC.OperationInfo = new LSRetailPosis.POSProcesses.OperationInfo();
                payC.POSTransaction = Transaction;
                payC.RunOperation();
            }
        }

        #endregion

        #region COMPLEMENTARY METHODS

        private void CancelarOperacion()
        {
            Cerrar();
        }

        private void Cerrar()
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }

        /// <summary>
        /// Método que recibe el valor a formatear con ceros a la izquierda y la cantidad de caracteres que debe contener, el resultado es un string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cZero"></param>
        /// <returns></returns>
        private string formatZeroValues(int value, int cZero)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cZero; i++)
                {
                    zeroCharacters += "0";
                }

                formatedValue = zeroCharacters + value.ToString();
                formatedValue = formatedValue.Substring(formatedValue.Length - cZero);

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
            }
            return formatedValue;
        }

        private string formatStringZeroValues(string value, int cZero)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cZero; i++)
                {
                    zeroCharacters += "0";
                }

                formatedValue = zeroCharacters + value;
                formatedValue = formatedValue.Substring(formatedValue.Length - cZero);

            }
            catch (Exception ex)
            {
                // Send ex to error control or LOG in AX
            }
            return formatedValue;
        }

        #endregion
    }
}
