﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using LSRetailPosis.Settings;
using System.Data;
using LSRetailPosis.Transaction;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
namespace APPLINET
{
    public class ConfigClass
    {
        private string user;
        private string pass;
        private string url;
        private string usrTrax;
        private string company;
        private string branch;
        private string country;
        private string nb_User;
        private string nb_Company;
        private string nb_companystreet;
        private string nb_Branch;
        private bool chkReverso;
        private string _pagomVMC;
        private string _pagomAMEX;
        private string _pagobVMC;
        private string _pagobAMEX;
        private string _pagobSIP;
        private bool _FacturaE;
        private bool _Points2;
        private string currency;
        private string tp_operacion;
        private int timerResp;
        private string timerOut;

        public string User
        {
            get
            {
                return user;
            }

            set
            {
                user = value;
            }
        }

        public string Pass
        {
            get
            {
                return pass;
            }

            set
            {
                pass = value;
            }
        }

        public string Url
        {
            get
            {
                return url;
            }

            set
            {
                url = value;
            }
        }

        public string UsrTrax
        {
            get
            {
                return usrTrax;
            }

            set
            {
                usrTrax = value;
            }
        }

        public string Company
        {
            get
            {
                return company;
            }

            set
            {
                company = value;
            }
        }

        public string Branch
        {
            get
            {
                return branch;
            }

            set
            {
                branch = value;
            }
        }

        public string Country
        {
            get
            {
                return country;
            }

            set
            {
                country = value;
            }
        }

        public string Nb_User
        {
            get
            {
                return nb_User;
            }

            set
            {
                nb_User = value;
            }
        }

        public string Nb_Company
        {
            get
            {
                return nb_Company;
            }

            set
            {
                nb_Company = value;
            }
        }

        public string Nb_companystreet
        {
            get
            {
                return nb_companystreet;
            }

            set
            {
                nb_companystreet = value;
            }
        }

        public string Nb_Branch
        {
            get
            {
                return nb_Branch;
            }

            set
            {
                nb_Branch = value;
            }
        }

        public bool ChkReverso
        {
            get
            {
                return chkReverso;
            }

            set
            {
                chkReverso = value;
            }
        }

        public string pagomVMC
        {
            get { return _pagomVMC; }
            set { _pagomVMC = value; }
        }

        public string pagomAMEX
        {
            get { return _pagomAMEX; }
            set { _pagomAMEX = value; }
        }

        public string pagobVMC
        {
            get { return _pagobVMC; }
            set { _pagobVMC = value; }
        }

        public string pagobAMEX
        {
            get { return _pagobAMEX; }
            set { _pagobAMEX = value; }
        }

        public string pagobSIP
        {
            get { return _pagobSIP; }
            set { _pagobSIP = value; }
        }

        public bool FacturaE
        {
            get { return _FacturaE; }
            set { _FacturaE = value; }
        }

        public bool Points2
        {
            get { return _Points2; }
            set { _Points2 = value; }
        }

        public string Currency
        {
            get
            {
                return currency;
            }

            set
            {
                currency = value;
            }
        }

        public string Tp_operacion
        {
            get
            {
                return tp_operacion;
            }

            set
            {
                tp_operacion = value;
            }
        }

        public string TimerOut
        {
            get
            {
                return timerOut;
            }

            set
            {
                timerOut = value;
            }
        }

        public int TimerResp
        {
            get
            {
                return timerResp;
            }

            set
            {
                timerResp = value;
            }
        }
    }

    class PrintClass
    {
        private bool isXML;
        public bool IsXML
        {
            get { return isXML; }
            set { isXML = value; }
        }

        private IApplication apps;
        private const int MEDIDAPAPEL = 53;

        public PrintClass(IApplication appli)
        {
            apps = appli;
        }

        public void ImprimeTicketReporte(string cuerpoTicket)
        {
            string textoFormateadoTicket = string.Empty;
            foreach (var myString in cuerpoTicket.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (!string.IsNullOrEmpty(FormatoLineaXML(myString)))
                {
                    textoFormateadoTicket += FormatoLineaXML(myString);
                    textoFormateadoTicket += "\n";
                }
            }
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            apps.Services.Peripherals.Printer.PrintReceipt(textoFormateadoTicket);
        }

        public void ImprimeTicket(string cuerpoTicket)
        {
            //reportLayout = new StringBuilder(2500);
            string textoFormateadoTicket = string.Empty;
            //textoFormateadoTicket += "\n";
            int validador = 0;
            bool validaLeyenda = false;
            foreach (var myString in cuerpoTicket.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (myString.Contains("OBLIGO"))
                {
                    validaLeyenda = true;
                }
                if (string.IsNullOrEmpty(FormatoLinea(myString)))
                {
                    validador++;
                }
                else
                {
                    if (!validaLeyenda)
                    {
                        if (myString.Contains("FECHA:"))
                        {
                            textoFormateadoTicket += FormatoLinea(myString);
                            textoFormateadoTicket += Environment.NewLine;
                            textoFormateadoTicket += Environment.NewLine;
                            textoFormateadoTicket += "\n";
                            validador = 0;
                        }
                        else
                        {
                            textoFormateadoTicket += FormatoLinea(myString);
                            textoFormateadoTicket += "\n";
                            validador = 0;
                        }
                    }
                    else
                    {
                        textoFormateadoTicket += FormatoLinea(myString);
                        validador = 0;
                    }
                }
                if (validador == 3 && string.IsNullOrEmpty(FormatoLinea(myString)))
                {
                    textoFormateadoTicket += "\n";
                    validador = 0;
                }
                
            }
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            apps.Services.Peripherals.Printer.PrintReceipt(textoFormateadoTicket);
        }

        public void ImprimeTicketFromXML(string cuerpoTicket)
        {
            //reportLayout = new StringBuilder(2500);
            string textoFormateadoTicket = string.Empty;
            //textoFormateadoTicket += "\n";
            int validador = 0;
            bool validaLeyenda = false;
            foreach (var myString in cuerpoTicket.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (myString.Contains("OBLIGO"))
                {
                    validaLeyenda = true;
                }
                if (string.IsNullOrEmpty(FormatoLineaXML(myString)))
                {
                    validador++;
                }
                else
                {
                    if (!validaLeyenda)
                    {
                        if (myString.Contains("FECHA:"))
                        {
                            textoFormateadoTicket += FormatoLineaXML(myString);
                            textoFormateadoTicket += Environment.NewLine;
                            textoFormateadoTicket += Environment.NewLine;
                            textoFormateadoTicket += "\n";
                            validador = 0;
                        }
                        else
                        {
                            textoFormateadoTicket += FormatoLineaXML(myString);
                            textoFormateadoTicket += "\n";
                            validador = 0;
                        }
                    }
                    else
                    {
                        textoFormateadoTicket += FormatoLineaXML(myString);
                        validador = 0;
                    }
                }
                if (validador == 3 && string.IsNullOrEmpty(FormatoLineaXML(myString)))
                {
                    textoFormateadoTicket += "\n";
                    validador = 0;
                }

            }
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            textoFormateadoTicket += "\n";
            apps.Services.Peripherals.Printer.PrintReceipt(textoFormateadoTicket);
        }

        private string FormatoLinea(string _linea)
        {
            string linea = string.Empty;
            string[] parameters;
            string comandoImpresion = string.Empty;
            int conteoPalabras = 0;
            int conteoLetras = 0;
            if (_linea.Contains("@"))
            {
                parameters = _linea.Split(' ');
                comandoImpresion = parameters[1];
                conteoPalabras = parameters.Count();

                for (int i = 2; i < parameters.Count(); i++)
                {
                    if (parameters[i].Contains("@"))
                    {
                        parameters[i] = string.Empty;
                    }
                    conteoLetras += parameters[i].Length;
                }

                if (comandoImpresion == "@br")
                {
                    return string.Empty;
                }
                else if (comandoImpresion.Contains("@c") || comandoImpresion.Contains("@bc"))
                {
                    if (conteoLetras > 0 && conteoPalabras > 0)
                    {
                        string espaciosString = string.Empty;
                        int espacios = (MEDIDAPAPEL - conteoLetras) / (2);
                        string palabrasunidas = string.Empty;
                        string retornoFinal = string.Empty;
                        for (int i = 0; i < espacios; i++)
                        {
                            espaciosString += " ";
                        }
                        for (int j = 2; j < conteoPalabras; j++)
                        {
                            if (parameters[j] != string.Empty)
                                palabrasunidas += parameters[j] + " ";
                        }
                        retornoFinal = espaciosString + palabrasunidas;
                        return retornoFinal;
                    }
                }
                else if (comandoImpresion.Contains("@l"))
                {
                    if (conteoLetras > 0 && conteoPalabras > 0)
                    {
                        string palabrasunidas = string.Empty;
                        string retornoFinal = string.Empty;

                        for (int j = 2; j < conteoPalabras; j++)
                        {
                            if (parameters[j] != string.Empty)
                                palabrasunidas += parameters[j] + " ";
                        }
                        retornoFinal = palabrasunidas;
                        return retornoFinal;
                    }
                }
                else if (comandoImpresion.Contains("@r"))
                {
                    if (conteoLetras > 0 && conteoPalabras > 0)
                    {
                        string palabrasunidas = string.Empty;
                        string retornoFinal = string.Empty;

                        for (int j = 2; j < conteoPalabras; j++)
                        {
                            if (parameters[j] != string.Empty)
                                palabrasunidas += parameters[j] + " ";
                        }
                        retornoFinal = palabrasunidas;
                        return retornoFinal;
                    }
                }
            }
            return linea;
        }

        public string FormatoLineaXML(string _linea)
        {
            string linea = string.Empty;
            string[] parameters;
            string comandoImpresion = string.Empty;
            int conteoPalabras = 0;
            int conteoLetras = 0;
            if (_linea.Contains("@"))
            {
                parameters = _linea.Split(' ');
                comandoImpresion = parameters[0];
                conteoPalabras = parameters.Count();

                for (int i = 1; i < parameters.Count(); i++)
                {
                    if (parameters[i].Contains("@lnn"))
                    {
                        parameters[i] = parameters[i].Replace("@lnn","");
                    }
                    if (parameters[i].Contains("@"))
                    {
                        parameters[i] = string.Empty;
                    }
                    conteoLetras += parameters[i].Length;
                }

                if (comandoImpresion == "@br")
                {
                    return string.Empty;
                }
                else if (comandoImpresion.Contains("@c") || comandoImpresion.Contains("@bc"))
                {
                    if (conteoLetras > 0 && conteoPalabras > 0)
                    {
                        string espaciosString = string.Empty;
                        int espacios = (MEDIDAPAPEL - conteoLetras) / (2);
                        string palabrasunidas = string.Empty;
                        string retornoFinal = string.Empty;
                        for (int i = 0; i < espacios; i++)
                        {
                            espaciosString += " ";
                        }
                        for (int j = 1; j < conteoPalabras; j++)
                        {
                            if (parameters[j] != string.Empty)
                                palabrasunidas += parameters[j] + " ";
                        }
                        retornoFinal = espaciosString + palabrasunidas;
                        return retornoFinal;
                    }
                }
                else if (comandoImpresion.Contains("@l"))
                {
                    if (conteoLetras > 0 && conteoPalabras > 0)
                    {
                        string palabrasunidas = string.Empty;
                        string retornoFinal = string.Empty;

                        for (int j = 1; j < conteoPalabras; j++)
                        {
                            if (parameters[j] != string.Empty)
                                palabrasunidas += parameters[j] + " ";
                        }
                        retornoFinal = palabrasunidas;
                        return retornoFinal;
                    }
                }
                else if (comandoImpresion.Contains("@r"))
                {
                    if (conteoLetras > 0 && conteoPalabras > 0)
                    {
                        string palabrasunidas = string.Empty;
                        string retornoFinal = string.Empty;

                        for (int j = 1; j < conteoPalabras; j++)
                        {
                            if (parameters[j] != string.Empty)
                                palabrasunidas += parameters[j] + " ";
                        }
                        retornoFinal = palabrasunidas;
                        return retornoFinal;
                    }
                }
            }
            return linea;
        }

        private string LeftStringEmptyValues(string value, int cEmpty)
        {
            string formatedValue = string.Empty;
            string emptyCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cEmpty; i++)
                {
                    emptyCharacters += " ";
                }

                formatedValue = value + emptyCharacters;
                formatedValue = formatedValue.Substring(0, cEmpty);

            }
            catch (Exception ex)
            {
                apps.Services.Dialog.ShowMessage(ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return formatedValue;
        }

        private string RightStringEmptyValues(string value, int cEmpty)
        {
            string formatedValue = string.Empty;
            string emptyCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cEmpty; i++)
                {
                    emptyCharacters += " ";
                }

                formatedValue = emptyCharacters + value;
                formatedValue = formatedValue.Substring(formatedValue.Length - cEmpty);

            }
            catch (Exception ex)
            {
                apps.Services.Dialog.ShowMessage(ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return formatedValue;
        }
    }
}
