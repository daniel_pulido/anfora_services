
using System.ComponentModel.Composition;
using System.Text;
using System.Windows.Forms;
using LSRetailPosis;
using LSRetailPosis.Settings.FunctionalityProfiles;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.BusinessObjects;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using Microsoft.Dynamics.Retail.Pos.Contracts.Services;
using LSRetailPosis.Transaction;
using System;
using APPLINET.WinForms;
using APPLINET.AppliNET;
using EglobalBBVA;
using System.Data;
using System.Data.SqlClient;

namespace APPLINET
{

    [Export(typeof(IBlankOperations))]
    public sealed class BlankOperations : IBlankOperations
    {
        [Import]
        public IApplication Application { get; set; }
        private const string payApplinet = "payapplinet";
        private const string CargarLlaves = "cargarllaves";
        private const string ConsultaPuntosBancomer = "consultaptos";
        private const string InicializaPinPadAppliNET = "applinetini";
        // Get all text through the Translation function in the ApplicationLocalizer
        // TextID's for BlankOperations are reserved at 50700 - 50999

        #region IBlankOperations Members
        /// <summary>
        /// Displays an alert message according operation id passed.
        /// </summary>
        /// <param name="operationInfo"></param>
        /// <param name="posTransaction"></param>        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Grandfather")]
        public void BlankOperation(IBlankOperationInfo operationInfo, IPosTransaction posTransaction)
        {
            #region Default Code
            //// This country check can be removed when customizing the BlankOperations service.
            //if (Functions.CountryRegion == SupportedCountryRegion.BR || 
            //    Functions.CountryRegion == SupportedCountryRegion.HU ||
            //    Functions.CountryRegion == SupportedCountryRegion.RU)
            //{
            //    if (Application.Services.Peripherals.FiscalPrinter.FiscalPrinterEnabled())
            //    {
            //        Application.Services.Peripherals.FiscalPrinter.BlankOperations(operationInfo, posTransaction);
            //    }
            //    return;
            //}

            //StringBuilder comment = new StringBuilder(128);
            //comment.AppendFormat(ApplicationLocalizer.Language.Translate(50700), operationInfo.OperationId);
            //comment.AppendLine();
            //comment.AppendFormat(ApplicationLocalizer.Language.Translate(50701), operationInfo.Parameter);
            //comment.AppendLine();
            //comment.Append(ApplicationLocalizer.Language.Translate(50702));

            //using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(comment.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error))
            //{
            //    LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            //}
            #endregion
            RetailTransaction retailTransaction = null;
            switch (operationInfo.OperationId)
            {
                case payApplinet:
                    #region payApplinet
                    retailTransaction = posTransaction as RetailTransaction;
                    if (posTransaction is RetailTransaction && retailTransaction.SaleItems.Count > 0 && retailTransaction.AmountDue != 0)
                    {
                        using (frmAPPLINET nfrmAPPL = new frmAPPLINET((RetailTransaction)posTransaction, Application, operationInfo.Parameter.ToString()))
                        {
                            LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(nfrmAPPL);
                        }
                    }
                    else
                    {
                        Application.Services.Dialog.ShowMessage("No hay ning�n art�culo para pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    operationInfo.OperationHandled = true;
                    break;
                    #endregion
                case CargarLlaves:
                    #region Carga de Llaves Manual
                    retailTransaction = posTransaction as RetailTransaction;

                    OperacionesAplliNET oper = new OperacionesAplliNET(retailTransaction, Application);
                    oper.CargaLLavesManual();

                    operationInfo.OperationHandled = true;
                    break;
                    #endregion
                case ConsultaPuntosBancomer:
                    #region Consulta de puntos Bancomer
                    retailTransaction = posTransaction as RetailTransaction;

                    OperacionesAplliNET operaciones = new OperacionesAplliNET(retailTransaction, Application);
                    operaciones.ConsultaPuntos();

                    operationInfo.OperationHandled = true;
                    break;
                    #endregion
                case InicializaPinPadAppliNET:
                    #region InicializaPinPadAppliNET
                    operationInfo.OperationHandled = true;
                    try
                    {
                        DAC odac = new DAC(new SqlConnection(Application.Settings.Database.Connection.ConnectionString));
                        DataTable DtConfig = odac.getAPPLINETConfig(posTransaction.StoreId,posTransaction.TerminalId );
                        if (DtConfig.Rows.Count > 0)
                        {
                            if (DtConfig.Rows[0]["ACTIVO"].ToString() == "0")
                            {
                                return;
                            }
                            else if (DtConfig.Rows[0]["ACTIVO"].ToString() == "1")
                            {
                             
                                RetailTransaction tran = posTransaction as RetailTransaction;
                                OperacionesAplliNET inet3 = new OperacionesAplliNET(tran, this.Application);
                                PinPadSC Enviar = new PinPadSC();
                                Enviar.SincronicacionInicial();
                                inet3.InicializaPinPad();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Application.Services.Dialog.ShowMessage("Error al cargar la configuraci�n APPLINET. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                    #endregion
            }
        }
        #endregion

    }
}
