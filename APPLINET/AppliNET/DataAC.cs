﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
//using LSRetailPosis.Settings;

namespace APPLINET.AppliNET
{
    class DataAC
    {
        #region CAMPOS DINAMICOS TICKET APPLINET

        public DataTable getLeyendasTicket(string DataAreaId, SqlConnection conn)
        {
            DataTable DtResults = new DataTable();
            try
            {
                string cmdTxt = "SELECT TOP 1 * FROM [ax].[GRW_APPLINET] WHERE DATAAREAID = '" + DataAreaId + "'";
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (SqlCommand command = new SqlCommand(cmdTxt, conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DtResults.Load(reader);
                        }
                    }
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return DtResults;
        }

        #endregion
    }
}
