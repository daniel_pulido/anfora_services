﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Windows.Forms;
using EglobalBBVA;
using Microsoft.Dynamics.Retail.Pos.Contracts;

namespace APPLINET.AppliNET
{
    public class PinPadAppliNet
    {
        #region "MIEMBROS DE CLASE"

        private IApplication apps;

        //4 lineas del ticket de Impresión
        public string linea1 { get; set; }
        public string linea2 { get; set; }
        public string linea3 { get; set; }
        public string linea4 { get; set; }

        #endregion

        #region "CONSTRUCTORES Y DESTRUCTOR"

        public PinPadAppliNet(IApplication appli)
        {
            try
            {
                apps = appli;
                //Lineas del encabezado del ticket
                string[] parameters;
                parameters = CargaLineasTicket();
                linea1 = parameters[0];
                linea2 = parameters[1];
                linea3 = parameters[2];
                linea4 = parameters[3];
            }
            catch (Exception ex)
            {
                CreaMensaje("Error al cargar la configuración. " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region "MÉTODOS"

        public string[] CargaLineasTicket()
        {
            string[] parameters = { "            ", "   ", "  ", "      " };
            try
            {
                DataTable DtResults = new DataTable();

                DataAC newDAC = new DataAC();
                DtResults = newDAC.getLeyendasTicket(apps.Settings.Database.DataAreaID, apps.Settings.Database.Connection);
                if (DtResults.Rows.Count > 0)
                {
                    parameters[0] = centrarTexto(DtResults.Rows[0]["LINEA1"].ToString());
                    parameters[1] = centrarTexto(DtResults.Rows[0]["LINEA2"].ToString());
                    parameters[2] = centrarTexto(DtResults.Rows[0]["LINEA3"].ToString());
                    parameters[3] = centrarTexto(DtResults.Rows[0]["LINEA4"].ToString());
                }
                else
                {
                    CreaMensaje("Error al cargar la información del ticket. " , MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
               /* parameters[0] = centrarTexto("ALMACENES ANFORA");
                parameters[1] = centrarTexto("LOPEZ No. 50 COLONIA CENTRO");
                parameters[2] = centrarTexto("DELEGACION CUAUHTEMOC");
                parameters[3] = centrarTexto("C.P. 06050");*/
            }
            catch (Exception ex)
            {
                CreaMensaje("Error al cargar la información del ticket. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return parameters;
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }

        private string centrarTexto(string _linea)
        {
            string retVal = string.Empty;
            int MEDIDAPAPEL = 51;
            try
            {
                int conteoPalabras = 0;
                int conteoLetras = 0;
                string[] parameters;
                parameters = _linea.Split(' ');
                conteoPalabras = parameters.Count();
                for (int i = 0; i < parameters.Count(); i++)
                {
                    conteoLetras += parameters[i].Length;
                }
                if (conteoLetras > 0 && conteoPalabras > 0)
                {
                    string espaciosString = string.Empty;
                    int espacios = (MEDIDAPAPEL - conteoLetras) / (2);
                    string palabrasunidas = string.Empty;
                    string retornoFinal = string.Empty;
                    for (int i = 0; i < espacios; i++)
                    {
                        espaciosString += " ";
                    }
                    for (int j = 0; j < conteoPalabras; j++)
                    {
                        if (parameters[j] != string.Empty)
                            palabrasunidas += parameters[j] + " ";
                    }
                    retornoFinal = espaciosString + palabrasunidas;
                    retVal = retornoFinal;
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("Error al centrar el texto del ticket AppliNET. " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retVal;
        }

        #endregion
    }
}
