
using System.ComponentModel.Composition;
using System.Text;
using System.Windows.Forms;
using LSRetailPosis;
using LSRetailPosis.Settings.FunctionalityProfiles;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.BusinessObjects;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using Microsoft.Dynamics.Retail.Pos.Contracts.Services;
using LSRetailPosis.Transaction;
using System;
using MIT.WinForms;
using System.Data;
using System.Data.SqlClient;

namespace MIT
{

    [Export(typeof(IBlankOperations))]
    public sealed class BlankOperations : IBlankOperations
    {
        [Import]
        public IApplication Application { get; set; }
        private const string tccPayMIT = "tccpaymit";
        private const string reporteMIT = "reportmit";
        private const string QueryMIT = "querymit";
        // Get all text through the Translation function in the ApplicationLocalizer
        // TextID's for BlankOperations are reserved at 50700 - 50999

        #region IBlankOperations Members
        /// <summary>
        /// Displays an alert message according operation id passed.
        /// </summary>
        /// <param name="operationInfo"></param>
        /// <param name="posTransaction"></param>        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Grandfather")]
        public void BlankOperation(IBlankOperationInfo operationInfo, IPosTransaction posTransaction)
        {
            #region Default Code
            //// This country check can be removed when customizing the BlankOperations service.
            //if (Functions.CountryRegion == SupportedCountryRegion.BR || 
            //    Functions.CountryRegion == SupportedCountryRegion.HU ||
            //    Functions.CountryRegion == SupportedCountryRegion.RU)
            //{
            //    if (Application.Services.Peripherals.FiscalPrinter.FiscalPrinterEnabled())
            //    {
            //        Application.Services.Peripherals.FiscalPrinter.BlankOperations(operationInfo, posTransaction);
            //    }
            //    return;
            //}

            //StringBuilder comment = new StringBuilder(128);
            //comment.AppendFormat(ApplicationLocalizer.Language.Translate(50700), operationInfo.OperationId);
            //comment.AppendLine();
            //comment.AppendFormat(ApplicationLocalizer.Language.Translate(50701), operationInfo.Parameter);
            //comment.AppendLine();
            //comment.Append(ApplicationLocalizer.Language.Translate(50702));

            //using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(comment.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error))
            //{
            //    LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            //}
            #endregion
            RetailTransaction retailTransaction = null;
            switch (operationInfo.OperationId)
            {
                case tccPayMIT:
                    #region tccPayMIT
                    operationInfo.OperationHandled = true;
                    retailTransaction = posTransaction as RetailTransaction;
                    if (posTransaction is RetailTransaction || posTransaction is CustomerOrderTransaction && retailTransaction.SaleItems.Count > 0)
                    {
                        if (retailTransaction.AmountDue != 0)
                        {
                            if (retailTransaction.AmountDue < 0)
                            {
                                DAC odac = new DAC(new SqlConnection(Application.Settings.Database.Connection.ConnectionString));
                                DataTable DtConfig = odac.getMITConfigCobranza(retailTransaction.StoreId, retailTransaction.TerminalId);
                                if (DtConfig.Rows.Count > 0)
                                {
                                    if (DtConfig.Rows[0]["Devolucion"].ToString() == "0")
                                    {
                                        Application.Services.Dialog.ShowMessage("No se permiten hacer devoluciones con MIT en esta terminal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
                                }
                                else
                                {
                                    Application.Services.Dialog.ShowMessage("No se han configurado los par�metros MIT", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                            }
                            using (frmMIT nfrmMIT = new frmMIT((RetailTransaction)posTransaction, Application, operationInfo.Parameter.ToString()))
                            {
                                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(nfrmMIT);
                            }
                        }
                    }
                    else
                    {
                        Application.Services.Dialog.ShowMessage("No hay ning�n art�culo para pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                    #endregion

                case QueryMIT:
                    #region QueryMIT
                    try
                    {
                        QueryMIT queryMIT = new QueryMIT(posTransaction, Application);
                        queryMIT.consultaMIT();
                    }
                    catch (Exception ex)
                    {
                        Application.Services.Dialog.ShowMessage("Ha ocurrido un error en la impresi�n del reporte, contacte con sistemas: " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    operationInfo.OperationHandled = true;
                    break;
                    #endregion

            }
        }
        #endregion

    }
}
