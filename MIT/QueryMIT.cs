﻿using cpIntegracionEMV;
using LSRetailPosis.DataAccess;
using LSRetailPosis.POSProcesses.WinFormsTouch;
using LSRetailPosis.POSProcesses;
using LSRetailPosis.Settings;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.Transaction;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.BusinessObjects;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using Microsoft.Dynamics.Retail.Pos.Contracts.Services;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.ComponentModel.Composition;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System;
using MIT.WinForms;

namespace MIT
{
    class QueryMIT
    {
        #region PROPERTIES
        private IPosTransaction Transaction;
        private IApplication apps;
        private const int paperWidth = 55;
        private static readonly string singleLine = string.Empty.PadLeft(paperWidth, '-');
        private ConfigClass configClass;
        public static cpIntegracionEMV.clsCpIntegracionEMV cpIntegraEMV = new clsCpIntegracionEMV();
        public static cpIntegracionEMV.clsPrePagoTrx PPOperacion = new clsPrePagoTrx();
        public static cpIntegracionEMV.clsServicios Servicios = new clsServicios();
        private string Url;
        private string respuestaMerchant;
        private bool LogueoUsuario = false;
        private string referencia = string.Empty;
        #endregion

        #region CONSTRUCTOR

        public QueryMIT(IPosTransaction tran, IApplication appli)
        {
            apps = appli;
            Transaction = tran;
        }

        #endregion

        #region MEMBERS

        // Diseña la tabla de resultados de acuerdo a la especificación proporcionada por MIT
        // MANUAL  Integración CDP Dll EMV RETAIL ver 7 3 1 doc 1 0 4 E.pdf
        private DataTable DesignDataTable()
        {
            DataTable dataResults = new DataTable();
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nu_operaion";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cd_usuario";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cd_empresa";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nu_sucursal";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nu_afiliacion";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nb_referencia";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cc_nombre";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cc_num";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cc_tp";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nu_importe";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cd_tipopago";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cd_tipocobro";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cd_instrumento";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nb_response";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nu_auth";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "fh_registro";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "fh_bank";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cd_usrtransaccion";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "tp_operacion";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nb_currency";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "cd_resp";
            dataResults.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "nb_resp";
            dataResults.Columns.Add(column);

            return dataResults;
        }

        public void consultaMIT()
        {
            try
            {
                InitConfigParamSuperAdmin();
                respuestaMerchant = string.Empty;
                DataTable queryResults = DesignDataTable();
                LogUsuario();
                if (LogueoUsuario)
                {
                    string dateTimeItem = string.Empty;
                    using (procesando nfrmProcesando = new procesando())
                    {
                        LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(nfrmProcesando);
                        if (nfrmProcesando.DialogResult == DialogResult.OK)
                        {
                            dateTimeItem = nfrmProcesando.fechaSelecionada;
                        }
                        else if (nfrmProcesando.DialogResult == DialogResult.Cancel)
                        {
                            return;
                        }
                    }
                    string retVal = string.Empty;
                    retVal = cpIntegraEMV.sndConsulta(configClass.User, configClass.Pass, configClass.Company, configClass.Branch, Convert.ToDateTime(dateTimeItem).ToString("dd/MM/yyyy"), referencia);
                    if (!string.IsNullOrEmpty(retVal))
                    {
                        try
                        {
                            retVal = retVal.Substring(retVal.IndexOf('<'), retVal.IndexOf("</transacciones>") + 16);
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(retVal);
                            VerificaRespuestaXML(xmlDoc, ref queryResults);
                        }
                        catch (Exception ex)
                        {
                            CreaMensaje("Ha ocurrido un error de conexión. Favor de intentar nuevamente." + "\n" + ex.Message + "\n" + retVal, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            CancelarOperacion();
                            return;
                        }
                    }
                    if (queryResults.Rows.Count > 0)
                    {
                        using (frmQuery nFrmQuery = new frmQuery(queryResults, cpIntegraEMV, configClass, apps))
                        {
                            LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(nFrmQuery);
                        }
                    }
                    else
                    {
                        CreaMensaje("No se han encontrado registros", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    Cerrar();
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("Ha ocurrido un error durante la operación, favor de contactar con sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                CancelarOperacion();
            }
        }

        private void VerificaRespuestaXML(XmlDocument xmlRetVal, ref DataTable _DataTableResults)
        {
            foreach (XmlNode xmlNode in xmlRetVal.DocumentElement.ChildNodes)
            {
                XmlNode nu_operaion = xmlNode.SelectSingleNode("nu_operaion");
                XmlNode cd_usuario = xmlNode.SelectSingleNode("cd_usuario");
                XmlNode cd_empresa = xmlNode.SelectSingleNode("cd_empresa");
                XmlNode nu_sucursal = xmlNode.SelectSingleNode("nu_sucursal");
                XmlNode nu_afiliacion = xmlNode.SelectSingleNode("nu_afiliacion");
                XmlNode nb_referencia = xmlNode.SelectSingleNode("nb_referencia");
                XmlNode cc_nombre = xmlNode.SelectSingleNode("cc_nombre");
                XmlNode cc_num = xmlNode.SelectSingleNode("cc_num");
                XmlNode cc_tp = xmlNode.SelectSingleNode("cc_tp");
                XmlNode nu_importe = xmlNode.SelectSingleNode("nu_importe");
                XmlNode cd_tipopago = xmlNode.SelectSingleNode("cd_tipopago");
                XmlNode cd_tipocobro = xmlNode.SelectSingleNode("cd_tipocobro");
                XmlNode cd_instrumento = xmlNode.SelectSingleNode("cd_instrumento");
                XmlNode nb_response = xmlNode.SelectSingleNode("nb_response");
                XmlNode nu_auth = xmlNode.SelectSingleNode("nu_auth");
                XmlNode fh_registro = xmlNode.SelectSingleNode("fh_registro");
                XmlNode fh_bank = xmlNode.SelectSingleNode("fh_bank");
                XmlNode cd_usrtransaccion = xmlNode.SelectSingleNode("cd_usrtransaccion");
                XmlNode tp_operacion = xmlNode.SelectSingleNode("tp_operacion");
                XmlNode nb_currency = xmlNode.SelectSingleNode("nb_currency");
                XmlNode cd_resp = xmlNode.SelectSingleNode("cd_resp");
                XmlNode nb_resp = xmlNode.SelectSingleNode("nb_resp");

                switch (nb_response.InnerText)
                {
                    case "approved":
                        DataRow row;
                        row = _DataTableResults.NewRow();
                        row["nu_operaion"] = nu_operaion.InnerText;
                        row["cd_usuario"] = cd_usuario.InnerText;
                        row["cd_empresa"] = cd_empresa.InnerText;
                        row["nu_sucursal"] = nu_sucursal.InnerText;
                        row["nu_afiliacion"] = nu_afiliacion.InnerText;
                        row["nb_referencia"] = nb_referencia.InnerText;
                        row["cc_nombre"] = cc_nombre.InnerText;
                        row["cc_num"] = cc_num.InnerText;
                        row["cc_tp"] = cc_tp.InnerText;
                        row["nu_importe"] = nu_importe.InnerText;
                        row["cd_tipopago"] = cd_tipopago.InnerText;
                        row["cd_tipocobro"] = cd_tipocobro.InnerText;
                        row["cd_instrumento"] = cd_instrumento.InnerText;
                        row["nb_response"] = nb_response.InnerText;
                        row["nu_auth"] = nu_auth.InnerText;
                        row["fh_registro"] = fh_registro.InnerText;
                        row["fh_bank"] = fh_bank.InnerText;
                        row["cd_usrtransaccion"] = cd_usrtransaccion.InnerText;
                        row["tp_operacion"] = tp_operacion.InnerText;
                        row["nb_currency"] = nb_currency.InnerText;
                        row["cd_resp"] = cd_resp.InnerText;
                        row["nb_resp"] = nb_resp.InnerText;
                        _DataTableResults.Rows.Add(row);
                        break;
                }
            }
        }

        public void InitConfigParamSuperAdmin()
        {
            configClass = new ConfigClass();
            try
            {
                DAC odac = new DAC(new SqlConnection(apps.Settings.Database.Connection.ConnectionString));
                DataTable DtConfig = odac.getMITConfigSuperAdmin(Transaction.StoreId);
                configClass = new ConfigClass();
                if (DtConfig.Rows.Count > 0)
                {
                    configClass.User = DtConfig.Rows[0]["Bs_User"].ToString();
                    configClass.Pass = RijndaelDecrypt.Decrypt(DtConfig.Rows[0]["Bs_Pwd"].ToString());
                    configClass.Url = DtConfig.Rows[0]["UrlMIT"].ToString();
                    configClass.Currency = DtConfig.Rows[0]["Tx_Currency"].ToString();
                    configClass.Tp_operacion = DtConfig.Rows[0]["Tp_operacion"].ToString();
                    configClass.TimerOut = DtConfig.Rows[0]["TimerOut"].ToString();
                    configClass.TimerResp = int.Parse(DtConfig.Rows[0]["TimerResp"].ToString());

                    Url = configClass.Url;
                    cpIntegraEMV.dbgSetUrl(ref Url);
                    cpIntegraEMV.dbgSetCurrencyType(configClass.Currency);
                }
                else
                {
                    CreaMensaje("No se pudo cargar la configuración del reporte, favor de contactar con sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("No se pudo cargar la configuración del reporte, favor de contactar con sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Cerrar();
            }
        }

        private void CargaObjetos()
        {
            configClass.User = cpIntegraEMV.dbgGetUser();
            configClass.Pass = cpIntegraEMV.dbgGetPass();
            configClass.Company = cpIntegraEMV.dbgGetId_Company();
            configClass.Branch = cpIntegraEMV.dbgGetId_Branch();
            configClass.Country = cpIntegraEMV.dbgGetCountry();
            configClass.Nb_User = cpIntegraEMV.dbgGetNb_User();
            configClass.Nb_Company = cpIntegraEMV.dbgGetNb_Company();
            configClass.Nb_companystreet = cpIntegraEMV.dbgGetNb_companystreet();
            configClass.Nb_Branch = cpIntegraEMV.dbgGetNb_Branch();

            configClass.ChkReverso = false;
            if (cpIntegraEMV.dbgGetActivateReverse() == "0")
            {
                configClass.ChkReverso = false;
            }
            else
            {
                configClass.ChkReverso = true;
            }
            //tiempo que tiene el usuario para deslizar o insertar la tarjeta antes de que la terminal vuelva a su estado de reposo.
            cpIntegraEMV.dbgSetTimeOut(configClass.TimerOut);

            configClass.pagomVMC = cpIntegraEMV.dbgGetpagomVMC();
            configClass.pagomAMEX = cpIntegraEMV.dbgGetpagomAMEX();
            configClass.pagobVMC = cpIntegraEMV.dbgGetpagobVMC();
            configClass.pagobAMEX = cpIntegraEMV.dbgGetpagobAMEX();
            configClass.pagobSIP = cpIntegraEMV.dbgGetpagobSIP();
            configClass.FacturaE = cpIntegraEMV.dbgGetFacturaE();
            configClass.Points2 = cpIntegraEMV.dbgGetPoints2();
        }

        /// <summary>
        /// Loguea al usuario y setea los datos que obtiene de Centro de Pagos
        /// </summary>
        private void LogUsuario()
        {
            try
            {
                //if (cpIntegraEMV.dbgSetReader() == true)
                //{
                if (cpIntegraEMV.dbgLoginUser(configClass.User, configClass.Pass))
                {
                    //Habilitar para desarrollo BEGIN
                    // cpIntegraEMV.dbgEnabledLog(true);
                    //Habilitar para desarrollo END
                    //Cargar la clase ConfigClass con los valores del Centro de Pagos MIT
                    CargaObjetos();
                    LogueoUsuario = true;
                }
                else
                {
                    string respuesta =
                       "               Error \n" +
                       "Error de autenticación de usuario \n" +
                       cpIntegraEMV.dbgGetRspError();
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                }
                //}
                //else
                //{
                //    string respuesta =
                //           "               Error \n" +
                //           "Verifique la instalación de la TPV \n";
                //    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    CancelarOperacion();
                //}
            }
            catch
            {
                CancelarOperacion();
            }
        }

        private void ImprimeVoucher(DataTable _queryResults)
        {
            PrintClass print = new PrintClass(apps);
            string textoTicket = string.Empty;
            textoTicket = GeneraLogicaImpresion(_queryResults);

            print.ImprimeTicketReporte(textoTicket);

            DialogResult dialogResult = apps.Services.Dialog.ShowMessage(
                       "¿Su ticket se imprimió correctamente?",
                       MessageBoxButtons.YesNo,
                       MessageBoxIcon.Question);
            if (dialogResult == DialogResult.No)
            {
                ImprimeVoucher(_queryResults);
            }
        }

        private string GeneraLogicaImpresion(DataTable _queryResults)
        {
            string retVal = string.Empty;

            //Leyendas ticket
            retVal += "@cnn ------------------------------------------------\n";
            retVal += "@br \n";
            retVal += "@cnn Resumen transacciones Pin Pad \n";
            retVal += "@br \n";
            retVal += "@cnn ------------------------------------------------\n";
            retVal += "@br \n";
            retVal += "@lnn Id. de tienda: " + Transaction.StoreId + "      Fecha: " + DateTime.Today.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "\n";
            retVal += "@lnn Numero de registro: " + Transaction.TerminalId + " Hora: " + DateTime.Now.ToString("hh:mm tt") + "\n";
            retVal += "@lnn Empleado: " + Transaction.Shift.StaffId + "\n";
            retVal += "@br \n";
            retVal += "@lnn Número de turno: " + Transaction.TerminalId + ":" + Transaction.Shift.BatchId.ToString() + "\n";
            retVal += "@lnn Fecha inicial: " + Transaction.Shift.StartDateTime.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "\n";
            retVal += "@lnn Hora inicial: " + Transaction.Shift.StartDateTime.ToString("hh:mm tt") + "\n";
            retVal += "@br \n";
            retVal += "@cnn ------------------------------------------------\n";
            retVal += "@br \n";
            int _qNumeroTransacciones = 0;
            decimal _qImporteTotal = 0;
            foreach (DataRow dr in _queryResults.Rows)
            {
                try
                {
                    //referencia = Transaction.TerminalId + "/" + Transaction.Shift.BatchId.ToString() + "/" + Transaction.Shift.StaffId + "/" + totalSecondsInt.ToString();
                    string _qReferencia = dr["nb_referencia"].ToString();
                    if (!string.IsNullOrEmpty(_qReferencia))
                    {
                        string[] paramss = _qReferencia.Split('/');
                        string terminal = paramss[0];
                        string numeroTurno = paramss[1];
                        string cajero = paramss[2];
                        if (!string.IsNullOrEmpty(terminal) && !string.IsNullOrEmpty(numeroTurno) && !string.IsNullOrEmpty(cajero))
                        {
                            if (terminal == Transaction.TerminalId && Int64.Parse(numeroTurno) == Transaction.Shift.BatchId && Transaction.Shift.StaffId == cajero)
                            {
                                string _qNu_importe = dr["nu_importe"].ToString();
                                decimal _qImporte;
                                if (decimal.TryParse(_qNu_importe, out _qImporte))
                                {
                                    _qImporteTotal += _qImporte;
                                    _qNumeroTransacciones++;
                                }
                            }
                        }
                        else
                        {
                            CreaMensaje("Configuración de la referencia incorrecta, favor de contactar con sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                catch
                {
                    continue;
                }
            }
            retVal += "@lnn Número de transacciones: " + _qNumeroTransacciones.ToString() + "\n";
            retVal += "@lnn Importe total: $" + _qImporteTotal.ToString() + "\n";
            retVal += "@br \n";
            retVal += "@cnn ------------------------------------------------\n";
            return retVal;
        }

        #endregion

        #region COMPLEMENTARY METHODS

        private void CancelarOperacion()
        {
            cpIntegraEMV.dbgCancelOperation();
        }

        private void Cerrar()
        {
            cpIntegraEMV.dbgEndOperation();
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }

        private void TranslateLabels()
        {
            //this.Text = "Cliente"; // Customer
        }

        /// <summary>
        /// Método que recibe el valor a formatear con ceros a la izquierda y la cantidad de caracteres que debe contener, el resultado es un string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cZero"></param>
        /// <returns></returns>
        private string formatZeroValues(int value, int cZero)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cZero; i++)
                {
                    zeroCharacters += "0";
                }

                formatedValue = zeroCharacters + value.ToString();
                formatedValue = formatedValue.Substring(formatedValue.Length - cZero);

            }
            catch (Exception ex)
            {
                CreaMensaje("Ocurrió un error, favor de contactar con sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return formatedValue;
        }

        #endregion
    }
}
