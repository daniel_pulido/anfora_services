﻿using System;
using System.Globalization;
using System.Windows.Forms;
using LSRetailPosis.POSProcesses;
using LSRetailPosis.POSProcesses.WinFormsTouch;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using LSRetailPosis.Settings;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Collections;
using LSRetailPosis.Transaction;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.DataAccess;
using System.Data.SqlClient;
using System.ComponentModel;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System.Xml;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Repository;
using cpIntegracionEMV;

namespace MIT.WinForms
{
    public partial class frmQuery : frmTouchBase
    {
        private IApplication apps;
        public string selectedReference = string.Empty;
        public string selectedAuth = string.Empty;
        public string selectedOperationNum = string.Empty;
        private DataTable dtTrans;
        public string selectedAmount = string.Empty;
        public static cpIntegracionEMV.clsCpIntegracionEMV nCpIntegraEMV = new clsCpIntegracionEMV();
        private ConfigClass nConfigClass = new ConfigClass();

        public frmQuery(DataTable dtTran, clsCpIntegracionEMV cpIntegraEMV, ConfigClass configClass, IApplication appli)
        {
            InitializeComponent();
            this.dtTrans = dtTran;
            nCpIntegraEMV = cpIntegraEMV;
            nConfigClass = configClass;
            apps = appli;
        }

        private void frmTran_Load(object sender, EventArgs e)
        {
            this.gridControl1.DataSource = dtTrans;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
            return;
        }

        private void btnAcept_Click(object sender, EventArgs e)
        {
            try
            {
                System.Data.DataRow Row = gridView3.GetDataRow(gridView3.GetSelectedRows()[0]);
                selectedReference = (string)Row["nb_referencia"];
                selectedAuth = (string)Row["nu_auth"];
                selectedOperationNum = (string)Row["nu_operaion"];
                selectedAmount = (string)Row["nu_importe"];

                Reimprimir(selectedOperationNum);
            }
            catch (Exception ex)
            {
                CreaMensaje(ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Reimprimir(string selectedOperationNum)
        {
            string respuesta = string.Empty;
            string txtVoucher = string.Empty;
            string XMLVoucher = string.Empty;
            string VComercio = string.Empty;
            string VCliente = string.Empty;

            if (nCpIntegraEMV.chkPp_Printer != "1")
            {
                string retval =
                      "               Error \n" +
                      "Verifique la instalación de la TPV \n";
                CreaMensaje(retval, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                nCpIntegraEMV.sndReimpresion(nConfigClass.User, nConfigClass.Pass, nConfigClass.Company, nConfigClass.Branch, nConfigClass.Country, selectedOperationNum);
                string voucherComercio = nCpIntegraEMV.getRspVoucherComercio();
                string voucherCliente = nCpIntegraEMV.getRspVoucherCliente();
                string vouchers = voucherComercio + voucherCliente;
                nCpIntegraEMV.dbgPrint(vouchers);
            }
            XMLVoucher = nCpIntegraEMV.sndReimpresion(nConfigClass.User, nConfigClass.Pass, nConfigClass.Company, nConfigClass.Branch, nConfigClass.Country, selectedOperationNum);
            System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + "/Impresora.txt");
            file.WriteLine(XMLVoucher);
            file.Close();
            XmlDocument xmlDocVoucherComercio = new XmlDocument();
            XmlDocument xmlDocVoucherCliente = new XmlDocument();
            if (!string.IsNullOrEmpty(XMLVoucher))
            {
                string VoucherComercio = XMLVoucher.Substring(XMLVoucher.IndexOf("<voucher_comercio>"), XMLVoucher.IndexOf("</voucher_comercio>") + 19);
                xmlDocVoucherComercio.LoadXml(VoucherComercio);
                string VoucherCliente = XMLVoucher.Substring(XMLVoucher.IndexOf("<voucher_cliente>"), (XMLVoucher.IndexOf("</voucher_cliente>") + 18 - (XMLVoucher.IndexOf("<voucher_cliente>"))));
                xmlDocVoucherCliente.LoadXml(VoucherCliente);

                VComercio = xmlDocVoucherComercio.InnerText;
                VCliente = xmlDocVoucherCliente.InnerText;
                DialogResult dialogResult = apps.Services.Dialog.ShowMessage(
                     "¿Desea imprimir el ticket en la impresora del POS?",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    ImprimeVoucher(VComercio, VCliente);
                }
            }
            else
            {
                CreaMensaje("No se ha obtenido respuesta del Centro de Pagos, contacte con Sistemas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ImprimeVoucher(string VComercio, string VCliente)
        {
            string vComercio = VComercio;
            string vCliente = VCliente;

            PrintClass print = new PrintClass(apps);
            if (VComercio != "")
            {
               print.ImprimeTicketFromXML(VComercio);
            }
            if (VCliente != "")
            {
                print.ImprimeTicketFromXML(VCliente);
            }
        }

        private void gridView3_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            System.Data.DataRow Row = gridView3.GetDataRow(gridView3.GetSelectedRows()[0]);
            selectedReference = (string)Row["nb_referencia"];
            selectedAuth = (string)Row["nu_auth"];
            selectedOperationNum = (string)Row["nu_operaion"];
            selectedAmount = (string)Row["nu_importe"];
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }
    }
}
