﻿namespace MIT.WinForms
{
    partial class frmTran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnAcept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransactionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerminal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceiptID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.vGridControl1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.CC_NUMBER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TX_AMOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TX_AUTH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TX_OPERATIONNUMBER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TX_REFERENCE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FECHAHORA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.styleController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1018, 42);
            this.label1.TabIndex = 5;
            this.label1.Text = "Seleccione la transacción a devolver";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAcept
            // 
            this.btnAcept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcept.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnAcept.Location = new System.Drawing.Point(3, 687);
            this.btnAcept.Name = "btnAcept";
            this.btnAcept.Size = new System.Drawing.Size(1018, 36);
            this.btnAcept.TabIndex = 6;
            this.btnAcept.Text = "Aceptar";
            this.btnAcept.UseVisualStyleBackColor = true;
            this.btnAcept.Click += new System.EventHandler(this.btnAcept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 14F);
            this.btnCancel.Location = new System.Drawing.Point(3, 729);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(1018, 36);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransactionDate,
            this.colStaff,
            this.colTerminal,
            this.colReceiptID,
            this.colType,
            this.colNetAmount});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.OptionsView.ShowPreview = true;
            this.gridView1.RowHeight = 40;
            this.gridView1.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.Default;
            this.gridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // colTransactionDate
            // 
            this.colTransactionDate.Caption = "Date";
            this.colTransactionDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTransactionDate.FieldName = "CREATEDDATE";
            this.colTransactionDate.Name = "colTransactionDate";
            this.colTransactionDate.OptionsColumn.AllowEdit = false;
            this.colTransactionDate.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colTransactionDate.Visible = true;
            this.colTransactionDate.VisibleIndex = 0;
            this.colTransactionDate.Width = 165;
            // 
            // colStaff
            // 
            this.colStaff.Caption = "Staff";
            this.colStaff.FieldName = "STAFF";
            this.colStaff.Name = "colStaff";
            this.colStaff.Visible = true;
            this.colStaff.VisibleIndex = 1;
            this.colStaff.Width = 88;
            // 
            // colTerminal
            // 
            this.colTerminal.Caption = "Terminal";
            this.colTerminal.FieldName = "TERMINAL";
            this.colTerminal.Name = "colTerminal";
            this.colTerminal.Visible = true;
            this.colTerminal.VisibleIndex = 2;
            this.colTerminal.Width = 88;
            // 
            // colReceiptID
            // 
            this.colReceiptID.Caption = "Transaction";
            this.colReceiptID.FieldName = "RECEIPTID";
            this.colReceiptID.Name = "colReceiptID";
            this.colReceiptID.Visible = true;
            this.colReceiptID.VisibleIndex = 3;
            this.colReceiptID.Width = 164;
            // 
            // colType
            // 
            this.colType.Caption = "Type";
            this.colType.FieldName = "TYPE";
            this.colType.Name = "colType";
            this.colType.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colType.Visible = true;
            this.colType.VisibleIndex = 4;
            // 
            // colNetAmount
            // 
            this.colNetAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.colNetAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNetAmount.Caption = "Amount";
            this.colNetAmount.DisplayFormat.FormatString = "c2";
            this.colNetAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNetAmount.FieldName = "GROSSAMOUNT";
            this.colNetAmount.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colNetAmount.Name = "colNetAmount";
            this.colNetAmount.Visible = true;
            this.colNetAmount.VisibleIndex = 5;
            this.colNetAmount.Width = 80;
            // 
            // vGridControl1
            // 
            this.vGridControl1.Name = "vGridControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(3, 45);
            this.gridControl1.MainView = this.gridView3;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1018, 636);
            this.gridControl1.TabIndex = 8;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3,
            this.gridView2});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.CC_NUMBER,
            this.TX_AMOUNT,
            this.TX_AUTH,
            this.TX_OPERATIONNUMBER,
            this.TX_REFERENCE,
            this.FECHAHORA});
            this.gridView3.GridControl = this.gridControl1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsCustomization.AllowColumnMoving = false;
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsCustomization.AllowGroup = false;
            this.gridView3.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView3.OptionsCustomization.AllowRowSizing = true;
            this.gridView3.OptionsMenu.EnableColumnMenu = false;
            this.gridView3.OptionsMenu.EnableFooterMenu = false;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gridView3.RowHeight = 40;
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView3_FocusedRowChanged);
            this.gridView3.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView3_ValidatingEditor);
            // 
            // CC_NUMBER
            // 
            this.CC_NUMBER.Caption = "Número de tarjeta";
            this.CC_NUMBER.FieldName = "CC_NUMBER";
            this.CC_NUMBER.Name = "CC_NUMBER";
            this.CC_NUMBER.OptionsColumn.AllowEdit = false;
            this.CC_NUMBER.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.CC_NUMBER.Visible = true;
            this.CC_NUMBER.VisibleIndex = 0;
            // 
            // TX_AMOUNT
            // 
            this.TX_AMOUNT.Caption = "Monto a devolver";
            this.TX_AMOUNT.FieldName = "TX_AMOUNT";
            this.TX_AMOUNT.Name = "TX_AMOUNT";
            this.TX_AMOUNT.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.TX_AMOUNT.Visible = true;
            this.TX_AMOUNT.VisibleIndex = 1;
            // 
            // TX_AUTH
            // 
            this.TX_AUTH.Caption = "Número de autorización";
            this.TX_AUTH.FieldName = "TX_AUTH";
            this.TX_AUTH.Name = "TX_AUTH";
            this.TX_AUTH.OptionsColumn.AllowEdit = false;
            this.TX_AUTH.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.TX_AUTH.Visible = true;
            this.TX_AUTH.VisibleIndex = 2;
            // 
            // TX_OPERATIONNUMBER
            // 
            this.TX_OPERATIONNUMBER.Caption = "Número de operación";
            this.TX_OPERATIONNUMBER.FieldName = "TX_OPERATIONNUMBER";
            this.TX_OPERATIONNUMBER.Name = "TX_OPERATIONNUMBER";
            this.TX_OPERATIONNUMBER.OptionsColumn.AllowEdit = false;
            this.TX_OPERATIONNUMBER.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.TX_OPERATIONNUMBER.Visible = true;
            this.TX_OPERATIONNUMBER.VisibleIndex = 3;
            // 
            // TX_REFERENCE
            // 
            this.TX_REFERENCE.Caption = "Referencia";
            this.TX_REFERENCE.FieldName = "TX_REFERENCE";
            this.TX_REFERENCE.Name = "TX_REFERENCE";
            this.TX_REFERENCE.OptionsColumn.AllowEdit = false;
            this.TX_REFERENCE.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.TX_REFERENCE.Visible = true;
            this.TX_REFERENCE.VisibleIndex = 4;
            // 
            // FECHAHORA
            // 
            this.FECHAHORA.Caption = "Fecha";
            this.FECHAHORA.FieldName = "FECHAHORA";
            this.FECHAHORA.Name = "FECHAHORA";
            this.FECHAHORA.OptionsColumn.AllowEdit = false;
            this.FECHAHORA.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.FECHAHORA.Visible = true;
            this.FECHAHORA.VisibleIndex = 5;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCancel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.gridControl1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnAcept, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1024, 768);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // frmTran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.tableLayoutPanel1);
            this.LookAndFeel.SkinName = "Money Twins";
            this.Name = "frmTran";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmTran";
            this.Load += new System.EventHandler(this.frmTran_Load);
            this.Controls.SetChildIndex(this.tableLayoutPanel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.styleController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAcept;
        private System.Windows.Forms.Button btnCancel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colTerminal;
        private DevExpress.XtraGrid.Columns.GridColumn colReceiptID;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
        private DevExpress.XtraGrid.Columns.GridColumn colNetAmount;
        private DevExpress.XtraGrid.Views.Grid.GridView vGridControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn CC_NUMBER;
        private DevExpress.XtraGrid.Columns.GridColumn TX_AMOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn TX_AUTH;
        private DevExpress.XtraGrid.Columns.GridColumn TX_OPERATIONNUMBER;
        private DevExpress.XtraGrid.Columns.GridColumn TX_REFERENCE;
        private DevExpress.XtraGrid.Columns.GridColumn FECHAHORA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}