﻿using System;
using System.Globalization;
using System.Windows.Forms;
using LSRetailPosis.POSProcesses;
using LSRetailPosis.POSProcesses.WinFormsTouch;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using LSRetailPosis.Settings;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Collections;
using LSRetailPosis.Transaction;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.DataAccess;
using System.Data.SqlClient;
using System.ComponentModel;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System.Xml;
using Microsoft.Dynamics.Retail.Pos.Contracts.DataEntity;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Repository;
namespace MIT.WinForms
{
    public partial class frmTran : frmTouchBase
    {
        public string selectedReference = string.Empty;
        public string selectedAuth = string.Empty;
        public string selectedOperationNum = string.Empty;
        private DataTable dtTrans;
        public string selectedAmount = string.Empty;
        decimal _amountDue;

        public frmTran(DataTable dtTran, decimal amountDue)
        {
            InitializeComponent();
            this.dtTrans = dtTran;
            _amountDue = amountDue;
        }

        private void frmTran_Load(object sender, EventArgs e)
        {
            this.gridControl1.DataSource = dtTrans;

            //editorForDisplay = new RepositoryItemProgressBar();
            //editorForEditing = new RepositoryItemTextEdit();
            //gridView3.GridControl.RepositoryItems.AddRange(
            //  new RepositoryItem[] { editorForDisplay, editorForEditing });

            //gridView3.Columns["TX_AMOUNT"].ColumnEdit = new RepositoryItemTextEdit();
            //DataColumn column = gridView3.Columns["TX_AMOUNT"].ColumnEdit.e
            //System.Data.DataRow Row = gridView3.GetDataRow(gridView3.GetSelectedRows()[0]);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
            return;
        }

        private void btnAcept_Click(object sender, EventArgs e)
        {
            System.Data.DataRow Row = gridView3.GetDataRow(gridView3.GetSelectedRows()[0]);
            selectedReference = (string)Row["TX_REFERENCE"];
            selectedAuth = (string)Row["TX_AUTH"];
            selectedOperationNum = (string)Row["TX_OPERATIONNUMBER"];
            selectedAmount = (string)Row["TX_AMOUNT"];
            if (decimal.Parse(selectedAmount) > decimal.Negate(_amountDue))
            {
                CreaMensaje("Favor de elegir un monto menor o igual a la deuda: " + decimal.Negate(_amountDue).ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                Close();
                return;
            }
        }

        private void gridView3_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            System.Data.DataRow Row = gridView3.GetDataRow(gridView3.GetSelectedRows()[0]);
            selectedReference = (string)Row["TX_REFERENCE"];
            selectedAuth = (string)Row["TX_AUTH"];
            selectedOperationNum = (string)Row["TX_OPERATIONNUMBER"];
            selectedAmount = (string)Row["TX_AMOUNT"];
        }

        private void gridView3_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            if (gridView3.FocusedColumn.Name == "TX_AMOUNT")
            {
                if (decimal.Parse(e.Value.ToString()) <= decimal.Negate(_amountDue))
                {
                    e.Valid = true;
                }
                else
                {
                    e.ErrorText = "Favor de elegir un monto menor o igual a la deuda: " + decimal.Negate(_amountDue).ToString();
                    e.Valid = false;
                }
            }
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }
    }
}
