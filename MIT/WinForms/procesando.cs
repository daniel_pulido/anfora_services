﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LSRetailPosis.POSProcesses.WinFormsTouch;
using LSRetailPosis.POSProcesses;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using Microsoft.Dynamics.Retail.Pos.Contracts;

namespace MIT.WinForms
{
    public partial class procesando : frmTouchBase
    {

        public string fechaSelecionada = string.Empty;

        public procesando()
        {
            InitializeComponent();
        }

        private void dateTimeConsulta_CloseUp(object sender, System.EventArgs e)
        {
            string month = "0" + dateTimeConsulta.Value.Month.ToString();
            fechaSelecionada = Convert.ToString(dateTimeConsulta.Value.Day + "/" + month.Substring(month.Length - 2) + "/" + dateTimeConsulta.Value.Year);
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
            return;
        }

        private void btnAcept_Click(object sender, System.EventArgs e)
        {
            string month = "0" + dateTimeConsulta.Value.Month.ToString();
            fechaSelecionada = Convert.ToString(dateTimeConsulta.Value.Day + "/" + month.Substring(month.Length - 2) + "/" + dateTimeConsulta.Value.Year);
            this.DialogResult = DialogResult.OK;
            Close();
            return;
        }

        private void procesando_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string month = "0" + dateTimeConsulta.Value.Month.ToString();
                fechaSelecionada = Convert.ToString(dateTimeConsulta.Value.Day + "/" + month.Substring(month.Length - 2) + "/" + dateTimeConsulta.Value.Year);
                this.DialogResult = DialogResult.OK;
                e.Handled = true;
                Close();
                return;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.DialogResult = DialogResult.Cancel;
                e.Handled = true;
                Close();
                return;
            }
        }

        private void procesando_Enter(object sender, EventArgs e)
        {
            string month = "0" + dateTimeConsulta.Value.Month.ToString();
            fechaSelecionada = Convert.ToString(dateTimeConsulta.Value.Day + "/" + month.Substring(month.Length - 2) + "/" + dateTimeConsulta.Value.Year);
            this.DialogResult = DialogResult.OK;
            Close();
            return;
        }

        private void procesando_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void procesando_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void procesando_Leave(object sender, EventArgs e)
        {

        }

        private void dateTimeConsulta_KeyUp(object sender, KeyEventArgs e)
        {
            string month = "0" + dateTimeConsulta.Value.Month.ToString();
            fechaSelecionada = Convert.ToString(dateTimeConsulta.Value.Day + "/" + month.Substring(month.Length - 2) + "/" + dateTimeConsulta.Value.Year);
            this.DialogResult = DialogResult.OK;
            Close();
            return;
        }
    }
}
