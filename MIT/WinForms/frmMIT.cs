﻿
using cpIntegracionEMV;
using LSRetailPosis.DataAccess;
using LSRetailPosis.POSProcesses.WinFormsTouch;
using LSRetailPosis.POSProcesses;
using LSRetailPosis.Settings;
using LSRetailPosis.Transaction.Line.SaleItem;
using LSRetailPosis.Transaction;
using Microsoft.Dynamics.Retail.Notification.Contracts;
using Microsoft.Dynamics.Retail.Pos.Contracts.UI;
using Microsoft.Dynamics.Retail.Pos.Contracts;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.ComponentModel.Composition;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System;


namespace MIT.WinForms
{
    public partial class frmMIT : frmTouchBase
    {
        #region PROPERTIES
        private RetailTransaction Transaction;
        private IApplication apps;
        private decimal amtDue;
        public string tendertype;
        private DAC odac;
        private DataTable DtFormas;
        private ConfigClass configClass;
        public static cpIntegracionEMV.clsCpIntegracionEMV cpIntegraEMV = new clsCpIntegracionEMV();
        public static cpIntegracionEMV.clsPrePagoTrx PPOperacion = new clsPrePagoTrx();
        public static cpIntegracionEMV.clsServicios Servicios = new clsServicios();
        private string referencia;
        private bool lecturaCorrecta = false;
        private string Url;
        private string cvvAmex;
        private string cbxType;
        private string respuestaMerchant;
        private string OperationNumber;
        private string AutNumber;
        string formaPagoCredito = string.Empty;
        string formaPagoDebito = string.Empty;
        string formaPagoAmex = string.Empty;
        //Returned transactions
        private string ReturnTransId;
        private string ReturnTerminalId;
        private string ReturnStoreId;
        private string CANCEL_AMOUNT;
        private bool cargoCancelacion = false;
        //Returned transactions
        private string VComercio;
        private string VCliente;
        private string operatorUserName;
        private string operatorUserPassword;
        private bool formatedXML;
        private bool LogueoUsuario = false;
        private int status;
        Timer timer01 = new Timer();
        private int tiempoTimer;
        private string tenderTypeId;
        private string cardTypeId;
        #endregion

        #region CONSTRUCTOR

        public frmMIT(RetailTransaction tran, IApplication appli, string formaPago)
        {
            try
            {
                apps = appli;
                Transaction = tran;
                tenderTypeId = formaPago;
                InitializeComponent();
                if (Transaction.AmountDue < 0)
                {
                    numAmount.NegativeMode = true;
                }
                else
                    numAmount.NegativeMode = false;

                ControlTextBox();
                //CargaFormasPago(appli);
                //ValidaFormasPago();
                if (!cargaFormasPago())
                {
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("Error en la carga de configuración. Contacte con sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        #endregion

        #region MEMBERS

        private void OnLoad(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                TranslateLabels();
                if (amtDue == 0)
                    amtDue = Transaction.AmountDue - Transaction.Payment;
                numAmount.EnterButtonPressed += numAmount_EnterPressed;
                lblTotalAmtDue.Text = "Prueba";
                lblTotalAmtDue.Text = "Monto total de la deuda " + apps.Services.Rounding.Round(amtDue, false);
                BtnAmt.Text = apps.Services.Rounding.Round(amtDue, false);
                txtAmt.Text = apps.Services.Rounding.Round(amtDue, false);
                System.DateTime moment = DateTime.Today;
                System.DateTime newDate = new System.DateTime(moment.Year, moment.Month, 1);
                txtexpDate.Format = DateTimePickerFormat.Custom;
                txtexpDate.CustomFormat = "MM yyyy";
                txtexpDate.ShowUpDown = true; // to prevent the calendar from being displayed
                txtexpDate.Value = newDate;
                int totalSecondsInt;
                totalSecondsInt = Convert.ToInt32(DateTime.Now.TimeOfDay.TotalSeconds);
                referencia = Transaction.Shift.OpenedAtTerminal + "/" + Transaction.Shift.BatchId.ToString() + "/" + Transaction.Shift.StaffId + "/" + totalSecondsInt.ToString();
                InitConfigParamCobranza();


                this.comboBox1.Visible = false;
                this.lblTipoPago.Visible = false;
                //if (numAmount.NegativeMode)
                //{
                //    this.BtnAceptar.Enabled = false;
                //    this.BtnAmt.Enabled = false;
                //    this.numAmount.Enabled = false;
                //    BtnAceptar_Click(this.BtnAceptar, EventArgs.Empty);
                //}
            }
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                decimal amt = 0;
                //bool realizaPago = validaObligatorios(); 
                bool realizaPago = true;
                if (realizaPago)
                {
                    if (decimal.TryParse(txtAmt.Text.Trim(), out amt))
                    {
                        this.BtnAceptar.Enabled = false;
                        this.BtnCancel.Enabled = false;
                        this.txtName.Text = "Procesando...";
                        this.numAmount.Enabled = false;
                        this.txtAmt.Enabled = false;
                        this.BtnAmt.Enabled = false;
                        this.comboBox1.Enabled = false;

                        if (!numAmount.NegativeMode)
                        {
                            if ((Transaction.TransSalePmtDiff - (amt)) < 0)
                                CreaMensaje("Monto capturado sobrepasa el monto total a pagar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            else
                            {
                                status = 0;
                                respuestaMerchant = string.Empty;
                                LogUsuario();
                                if (LogueoUsuario)
                                {
                                    LecturaTarjeta();
                                    if (!string.IsNullOrEmpty(respuestaMerchant) && lecturaCorrecta)
                                    {
                                        EjecutaCobroBanda(respuestaMerchant);
                                        tiempoTimer = 0;
                                        timer01.Interval = 3000;
                                        timer01.Enabled = true;
                                        timer01.Tick += new EventHandler(timerprocVenta);
                                        timer01.Start();
                                    }
                                    else
                                    {
                                        Cerrar();
                                    }
                                }
                            }
                        }
                        //Cancelación
                        else
                        {
                            //ManagerAccessConfirmation managerAccessInteraction = new ManagerAccessConfirmation();
                            //InteractionRequestedEventArgs request = new InteractionRequestedEventArgs(managerAccessInteraction, () => { });
                            //apps.Services.Interaction.InteractionRequest(request);

                            //if (managerAccessInteraction.Confirmed)
                            //{
                            // Manager auth was successful
                            status = 0;
                            InitConfigParamAdmin();
                            LogUsuario();
                            if (LogueoUsuario)
                            {
                                CargaCancelacion();
                                EjecutaCancelacion();
                                if (cargoCancelacion)
                                {
                                    tiempoTimer = 0;
                                    timer01.Interval = 3000;
                                    timer01.Enabled = true;
                                    timer01.Tick += new EventHandler(timerprocCancela);
                                    timer01.Start();
                                }
                                else
                                {
                                    Cerrar();
                                }
                            }
                            //}
                            //else
                            //{
                            //    CreaMensaje("La operación requiere privilegios de supervisor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            //    CancelarOperacion();
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("Ha ocurrido un error durante la operación, favor de contactar con sistemas. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                CancelarOperacion();
            }
        }

        private void timerprocVenta(object o1, EventArgs e1)
        {
            if (tiempoTimer < configClass.TimerResp)
            {
                if (cpIntegraEMV.getRspDsResponse() != "")
                {
                    timer01.Enabled = false;
                    formatedXML = false;
                    VerificaRespuestaVenta();
                }
                else
                {

                    tiempoTimer += timer01.Interval;
                }
            }
            else
            {
                formatedXML = true;
                string retVal = string.Empty;
                timer01.Enabled = false;
                retVal = cpIntegraEMV.sndConsulta(configClass.User, configClass.Pass, configClass.Company, configClass.Branch, DateTime.Today.ToString("dd/MM/yyyy"), referencia);
                if (!string.IsNullOrEmpty(retVal))
                {
                    retVal = retVal.Substring(retVal.IndexOf('<'), retVal.IndexOf("</transacciones>") + 16);
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(retVal);
                    XmlNode responseVenta = xmlDoc.DocumentElement.SelectSingleNode("transaccion/nb_response");
                    if (!string.IsNullOrEmpty(responseVenta.InnerText))
                    {
                        VerificaRespuestaVentaXML(xmlDoc);
                    }
                    else
                    {
                        cpIntegraEMV.dbgCancelOperation();
                        CreaMensaje("No hay respuesta del emisor intente más tarde." + "\n" + "No se realizó ningún cargo a su tarjeta.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cerrar();
                    }
                }
                else
                {
                    cpIntegraEMV.dbgCancelOperation();
                    CreaMensaje("No hay respuesta del emisor intente más tarde." + "\n" + "No se realizó ningún cargo a su tarjeta.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                }
            }
        }

        private void timerprocCancela(object o1, EventArgs e1)
        {
            if (tiempoTimer < configClass.TimerResp)
            {
                if (cpIntegraEMV.getRspDsResponse() != "")
                {
                    timer01.Enabled = false;
                    formatedXML = false;
                    VerificaRespuestaCancelacion();
                }
                else
                {
                    tiempoTimer += timer01.Interval;
                }
            }
            else
            {
                formatedXML = true;
                timer01.Enabled = false;
                string retVal = string.Empty;
                retVal = cpIntegraEMV.sndConsulta(configClass.User, configClass.Pass, configClass.Company, configClass.Branch, DateTime.Today.ToString("dd/MM/yyyy"), referencia);
                if (!string.IsNullOrEmpty(retVal))
                {
                    retVal = retVal.Substring(retVal.IndexOf('<'), retVal.IndexOf("</transacciones>") + 16);
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(retVal);
                    XmlNode responseVenta = xmlDoc.DocumentElement.SelectSingleNode("transaccion/nb_response");
                    if (!string.IsNullOrEmpty(responseVenta.InnerText))
                    {
                        VerificaRespuestaCancelacionXML(xmlDoc);
                    }
                    else
                    {
                        cpIntegraEMV.dbgCancelOperation();
                        CreaMensaje("No hay respuesta del emisor intente más tarde", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cerrar();
                    }
                }
                else
                {
                    cpIntegraEMV.dbgCancelOperation();
                    CreaMensaje("No hay respuesta del emisor intente más tarde", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                }
            }
        }

        /// <summary>
        /// Carga la clase ConfigClass con los parámetros extraidos de AX
        /// </summary>
        public void InitConfigParamCobranza()
        {
            configClass = new ConfigClass();
            try
            {
                DAC odac = new DAC(new SqlConnection(apps.Settings.Database.Connection.ConnectionString));
                DataTable DtConfig = odac.getMITConfigCobranza(Transaction.StoreId, Transaction.TerminalId);
                configClass = new ConfigClass();
                if (DtConfig.Rows.Count > 0)
                {
                    configClass.User = DtConfig.Rows[0]["Bs_User"].ToString();
                    configClass.Pass = RijndaelDecrypt.Decrypt(DtConfig.Rows[0]["Bs_Pwd"].ToString());
                    configClass.Url = DtConfig.Rows[0]["UrlMIT"].ToString();
                    configClass.Currency = DtConfig.Rows[0]["Tx_Currency"].ToString();
                    configClass.Tp_operacion = DtConfig.Rows[0]["Tp_operacion"].ToString();
                    configClass.TimerOut = DtConfig.Rows[0]["TimerOut"].ToString();
                    configClass.TimerResp = int.Parse(DtConfig.Rows[0]["TimerResp"].ToString());

                    operatorUserName = configClass.User;
                    operatorUserPassword = configClass.Pass;

                    Url = configClass.Url;
                    cpIntegraEMV.dbgSetUrl(ref Url);
                    cpIntegraEMV.dbgSetCurrencyType(configClass.Currency);
                }
                else
                {
                    CreaMensaje("No se pudo cargar la configuración de la Terminal, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("No se pudo cargar la configuración de la Terminal, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Cerrar();
            }
        }

        public void InitConfigParamAdmin()
        {
            configClass = new ConfigClass();
            try
            {
                DAC odac = new DAC(new SqlConnection(apps.Settings.Database.Connection.ConnectionString));
                DataTable DtConfig = odac.getMITConfigAdmin(Transaction.StoreId, Transaction.TerminalId);
                configClass = new ConfigClass();
                if (DtConfig.Rows.Count > 0)
                {
                    configClass.User = DtConfig.Rows[0]["Bs_User"].ToString();
                    configClass.Pass = RijndaelDecrypt.Decrypt(DtConfig.Rows[0]["Bs_Pwd"].ToString());
                    configClass.Url = DtConfig.Rows[0]["UrlMIT"].ToString();
                    configClass.Currency = DtConfig.Rows[0]["Tx_Currency"].ToString();
                    configClass.Tp_operacion = DtConfig.Rows[0]["Tp_operacion"].ToString();
                    configClass.TimerOut = DtConfig.Rows[0]["TimerOut"].ToString();
                    configClass.TimerResp = int.Parse(DtConfig.Rows[0]["TimerResp"].ToString());

                    Url = configClass.Url;
                    cpIntegraEMV.dbgSetUrl(ref Url);
                    cpIntegraEMV.dbgSetCurrencyType(configClass.Currency);
                }
                else
                {
                    CreaMensaje("No se pudo cargar la configuración de la Terminal, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("No se pudo cargar la configuración de la Terminal, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Cerrar();
            }
        }

        private void CargaObjetos()
        {
            configClass.User = cpIntegraEMV.dbgGetUser();
            configClass.Pass = cpIntegraEMV.dbgGetPass();
            configClass.Company = cpIntegraEMV.dbgGetId_Company();
            configClass.Branch = cpIntegraEMV.dbgGetId_Branch();
            configClass.Country = cpIntegraEMV.dbgGetCountry();
            configClass.Nb_User = cpIntegraEMV.dbgGetNb_User();
            configClass.Nb_Company = cpIntegraEMV.dbgGetNb_Company();
            configClass.Nb_companystreet = cpIntegraEMV.dbgGetNb_companystreet();
            configClass.Nb_Branch = cpIntegraEMV.dbgGetNb_Branch();

            configClass.ChkReverso = false;
            if (cpIntegraEMV.dbgGetActivateReverse() == "0")
            {
                configClass.ChkReverso = false;
            }
            else
            {
                configClass.ChkReverso = true;
            }
            //tiempo que tiene el usuario para deslizar o insertar la tarjeta antes de que la terminal vuelva a su estado de reposo.
            cpIntegraEMV.dbgSetTimeOut(configClass.TimerOut);

            configClass.pagomVMC = cpIntegraEMV.dbgGetpagomVMC();
            configClass.pagomAMEX = cpIntegraEMV.dbgGetpagomAMEX();
            configClass.pagobVMC = cpIntegraEMV.dbgGetpagobVMC();
            configClass.pagobAMEX = cpIntegraEMV.dbgGetpagobAMEX();
            configClass.pagobSIP = cpIntegraEMV.dbgGetpagobSIP();
            configClass.FacturaE = cpIntegraEMV.dbgGetFacturaE();
            configClass.Points2 = cpIntegraEMV.dbgGetPoints2();
        }

        /// <summary>
        /// Loguea al usuario y setea los datos que obtiene de Centro de Pagos
        /// </summary>
        private void LogUsuario()
        {
            try
            {
                if (cpIntegraEMV.dbgSetReader() == true)
                {
                    if (cpIntegraEMV.dbgLoginUser(configClass.User, configClass.Pass))
                    {
                        //Habilitar para desarrollo BEGIN
                        // cpIntegraEMV.dbgEnabledLog(true);
                        //Habilitar para desarrollo END
                        //Cargar la clase ConfigClass con los valores del Centro de Pagos MIT
                        CargaObjetos();
                        LogueoUsuario = true;
                    }
                    else
                    {
                        string respuesta =
                           "               Error \n" +
                           "Error de autenticación de usuario \n" +
                           "Verifique la instalación de la TPV \n" +
                           cpIntegraEMV.dbgGetRspError();
                        CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        CancelarOperacion();
                    }
                }
                else
                {
                    string respuesta =
                           "               Error \n" +
                           "Verifique la instalación de la TPV \n";
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                }
            }
            catch
            {
                CancelarOperacion();
            }
        }

        private void VerificaRespuestaVenta()
        {
            string respuesta = string.Empty;
            string txtVoucher = string.Empty;
            switch (cpIntegraEMV.getRspDsResponse())
            {
                case "approved":  //Transacción Aprobada
                    respuesta = "          Cobro aprobado" + "\n" +
                    "Núm. de Referencia: " + cpIntegraEMV.getTx_Reference() + "\n" +
                    "Núm. de Operación: " + cpIntegraEMV.getRspOperationNumber() + "\n" +
                    "Núm. de Autorización: " + cpIntegraEMV.getRspAuth() + "\n" +
                    "Tipo de Tarjeta: " + cpIntegraEMV.getCc_Type() + "\n" +
                    "Nombre TH: " + cpIntegraEMV.getCc_Name() + "\n" +
                    "Núm. Tarjeta: " + cpIntegraEMV.getCc_Number() + "\n" +
                    "Vencimiento: " + cpIntegraEMV.getCc_ExpMonth() + "/" + cpIntegraEMV.getCc_ExpYear() + "\n" +
                    "Monto Cobro: $" + cpIntegraEMV.getTx_Amount() + " " + configClass.Currency + "\n" +
                    "Fecha Operación: " + cpIntegraEMV.getRspDate();
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtVoucher = cpIntegraEMV.getRspVoucher();
                    if (cpIntegraEMV.getRspVoucherCliente() != "")
                    {
                        VComercio = cpIntegraEMV.getRspVoucherComercio();
                        VCliente = cpIntegraEMV.getRspVoucherCliente() + cpIntegraEMV.getRspFeTxLeyenda();
                        //+ cpIntegraEMV.getRspFeTxLeyenda(); //(Este parametro se usa solamente si se requiere factura electronica)
                    }
                    ImprimeVoucher();
                    status = 1;
                    decimal totalPayed = decimal.Parse(this.txtAmt.Text.ToString().Replace(",", ""));

                    string validadorTarjeta = string.Empty;
                    validadorTarjeta = cpIntegraEMV.getCc_Type();
                    asignaCardTypeId(validadorTarjeta);

                    SaveTransaction(totalPayed, status);

                    ControlTextBox();
                    this.DialogResult = DialogResult.OK;
                    Cerrar();
                    break;

                case "denied":
                    respuesta =
                        "            Cobro declinado" + "\n" +
                        "No se realizó ningún cargo a su tarjeta." + "\n" +
                        "La operación fue Declinada por su Banco Emisor. \n" +
                        "Favor de intentar con otra tarjeta." + "\n" +
                        cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.getRspDsError();
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;
                case "error":
                    respuesta =
                         "               Error " + "\n" +
                        "Ocurrió un error al realizar el pago." + "\n" +
                        "No se realizó ningún cargo a su tarjeta." + "\n" +
                        "Favor de intentarlo más tarde. \n" +
                      cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.getRspDsError();
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;
                default:
                    respuesta =
                          "               Error " + "\n" +
                        "Ocurrió un error al realizar el pago." + "\n" +
                        "No se realizó ningún cargo a su tarjeta." + "\n" +
                        "Favor de intentarlo más tarde. \n" +
                         cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.chkPp_DsError;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;
            }
        }

        private void asignaCardTypeId(string validadorTarjeta)
        {
            try
            {
                cardTypeId = formaPagoCredito;
                if (validadorTarjeta.Contains("AMEX") && !validadorTarjeta.Contains("BANAMEX"))
                {
                    cardTypeId = formaPagoAmex;
                }
                else
                {
                    if (validadorTarjeta.ToUpper().Contains("CREDITO"))
                    {
                        cardTypeId = formaPagoCredito;
                    }
                    else if (validadorTarjeta.ToUpper().Contains("DEBITO"))
                    {
                        cardTypeId = formaPagoDebito;
                    }
                    else if (!validadorTarjeta.ToUpper().Contains("DEBITO") && !validadorTarjeta.ToUpper().Contains("CREDITO"))
                    {
                        DialogResult dialogResult = apps.Services.Dialog.ShowMessage(
                                   "¿Es tarjeta de CRÉDITO?",
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            cardTypeId = formaPagoCredito;
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            cardTypeId = formaPagoDebito;
                        }
                    }
                }
                //if (DtFormas.Rows.Count > 0)
                //{
                //    //Set default
                //    foreach (DataRow row in DtFormas.Rows)
                //    {
                //        if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER CRÉDITO"))
                //        {
                //            cardTypeId = row["CardTypeId"].ToString();
                //            break;
                //        }
                //    }
                //    if (validadorTarjeta.Contains("AMEX") && !validadorTarjeta.Contains("BANAMEX"))
                //    {
                //        foreach (DataRow row in DtFormas.Rows)
                //        {
                //            if (row["Descripcion"].ToString().ToUpper().Contains("AMERICAN"))
                //            {
                //                cardTypeId = row["CardTypeId"].ToString();
                //                break;
                //            }
                //        }
                //    }
                //    else
                //    {
                //        if (validadorTarjeta.ToUpper().Contains("CREDITO"))
                //        {
                //            foreach (DataRow row in DtFormas.Rows)
                //            {
                //                if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER CRÉDITO"))
                //                {
                //                    cardTypeId = row["CardTypeId"].ToString();
                //                    break;
                //                }
                //            }
                //        }
                //        else if (validadorTarjeta.ToUpper().Contains("DEBITO"))
                //        {
                //            foreach (DataRow row in DtFormas.Rows)
                //            {
                //                if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER DÉBITO"))
                //                {
                //                    cardTypeId = row["CardTypeId"].ToString();
                //                    break;
                //                }
                //            }
                //        }
                //        else if (!validadorTarjeta.ToUpper().Contains("DEBITO") && !validadorTarjeta.ToUpper().Contains("CREDITO"))
                //        {
                //            DialogResult dialogResult = apps.Services.Dialog.ShowMessage(
                //                       "¿Es tarjeta de CRÉDITO?",
                //                       MessageBoxButtons.YesNo,
                //                       MessageBoxIcon.Question);
                //            if (dialogResult == DialogResult.Yes)
                //            {
                //                foreach (DataRow row in DtFormas.Rows)
                //                {
                //                    if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER CRÉDITO"))
                //                    {
                //                        cardTypeId = row["CardTypeId"].ToString();
                //                        break;
                //                    }
                //                }
                //            }
                //            else if (dialogResult == DialogResult.No)
                //            {
                //                foreach (DataRow row in DtFormas.Rows)
                //                {
                //                    if (row["Descripcion"].ToString().ToUpper().Contains("SANTANDER DÉBITO"))
                //                    {
                //                        cardTypeId = row["CardTypeId"].ToString();
                //                        break;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                //else
                //{
                //    CreaMensaje("No se ha cargado la información de las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
            catch (Exception ex)
            {
                CreaMensaje("Error al asignar la forma de pago. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void VerificaRespuestaCancelacion()
        {
            string respuesta = string.Empty;
            string txtVoucher = string.Empty;
            switch (cpIntegraEMV.getRspDsResponse())
            {
                case "approved":  //Transacción Aprobada
                    respuesta = "      Cancelación aprobada" + "\n" +
                    "Núm. de Referencia: " + cpIntegraEMV.getTx_Reference() + "\n" +
                    "Núm. de Operación: " + cpIntegraEMV.getRspOperationNumber() + "\n" +
                    "Núm. de Autorización: " + cpIntegraEMV.getRspAuth() + "\n" +
                    "Tipo de Tarjeta: " + cpIntegraEMV.getCc_Type() + "\n" +
                    "Nombre TH: " + cpIntegraEMV.getCc_Name() + "\n" +
                    "Núm. Tarjeta: " + cpIntegraEMV.getCc_Number() + "\n" +
                    "Vencimiento: " + cpIntegraEMV.getCc_ExpMonth() + "/" + cpIntegraEMV.getCc_ExpYear() + "\n" +
                    "Monto Cancelación: $" + cpIntegraEMV.getTx_Amount() + " " + configClass.Currency + "\n" +
                    "Fecha Operación: " + cpIntegraEMV.getRspDate();
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    string validadorTarjeta = string.Empty;
                    validadorTarjeta = cpIntegraEMV.getCc_Type();
                    asignaCardTypeId(validadorTarjeta);

                    txtVoucher = cpIntegraEMV.getRspVoucher();
                    if (cpIntegraEMV.getRspVoucherCliente() != "")
                    {
                        VComercio = cpIntegraEMV.getRspVoucherComercio();
                        VCliente = cpIntegraEMV.getRspVoucherCliente() + cpIntegraEMV.getRspFeTxLeyenda(); //(Este parametro se usa solamente si se requiere factura electronica)
                    }
                    ImprimeVoucher();
                    status = 2;
                    decimal totalPayed = decimal.Parse(this.txtAmt.Text.ToString().Replace(",", ""));
                    SaveTransaction(totalPayed, status);
                    ControlTextBox();
                    Cerrar();
                    break;

                case "denied":
                    respuesta =
                        "          Cancelación declinada" + "\n" +
                        "No se realizó ningún abono a su tarjeta." + "\n" +
                        "La operación fue Declinada por su Banco Emisor. \n" +
                         cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.getRspDsError();
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                    break;

                case "error":
                    respuesta =
                         "               Error \n" +
                         "Ocurrió un error al realizar la cancelación \n" +
                         "No se realizó ningún reintegro a su tarjeta \n" +
                         "Favor de intentarlo más tarde. \n" +
                         cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.getRspDsError();
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                    break;

                default:
                    respuesta =
                        "               Error \n" +
                        "Ocurrió un error al realizar la cancelación \n" +
                        "No se realizó ningún reintegro a su tarjeta \n" +
                        "Favor de intentarlo más tarde. \n" +
                        cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.chkPp_DsError;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                    break;
            }
        }

        private void VerificaRespuestaCancelacionXML(XmlDocument xmlRetVal)
        {
            string respuesta = string.Empty;
            string txtVoucher = string.Empty;
            string XMLVoucher = string.Empty;
            XmlNode responseVenta = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_response");

            XmlNode nu_operaion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_operaion");
            XmlNode cd_usuario = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_usuario");
            XmlNode cd_empresa = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_empresa");
            XmlNode nu_sucursal = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_sucursal");
            XmlNode nu_afiliacion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_afiliacion");
            XmlNode nb_referencia = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_referencia");
            XmlNode cc_nombre = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_nombre");
            XmlNode cc_num = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_num");
            XmlNode cc_tp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_tp");
            XmlNode nu_importe = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_importe");
            XmlNode cd_tipopago = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_tipopago");
            XmlNode cd_tipocobro = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_tipocobro");
            XmlNode cd_instrumento = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_instrumento");
            XmlNode nb_response = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_response");
            XmlNode nu_auth = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_auth");
            XmlNode fh_registro = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/fh_registro");
            XmlNode fh_bank = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/fh_bank");
            XmlNode cd_usrtransaccion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_usrtransaccion");
            XmlNode tp_operacion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/tp_operacion");
            XmlNode nb_currency = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_currency");
            XmlNode cd_resp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_resp");
            XmlNode nb_resp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_resp");


            switch (responseVenta.InnerText)
            {
                case "approved":  //Transacción Aprobada
                    respuesta = "      Cancelación aprobada" + "\n" +
                     "Núm. de Referencia: " + nb_referencia.InnerText + "\n" +
                    "Núm. de Operación: " + nu_operaion.InnerText + "\n" +
                    "Núm. de Autorización: " + nu_auth.InnerText + "\n" +
                    "Tipo de Tarjeta: " + cc_tp.InnerText + " " + cd_instrumento.InnerText + "\n" +
                    "Nombre TH: " + cc_nombre.InnerText + "\n" +
                    "Núm. Tarjeta: " + cc_num.InnerText + "\n" +
                    "Vencimiento: " + cpIntegraEMV.getCc_ExpMonth() + "/" + cpIntegraEMV.getCc_ExpYear() + "\n" +
                    "Monto Cobro: $" + nu_importe.InnerText + " " + configClass.Currency + "\n" +
                    "Fecha Operación: " + fh_bank.InnerText;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    string validadorTarjeta = string.Empty;
                    validadorTarjeta = cc_tp.InnerText;
                    asignaCardTypeId(validadorTarjeta);

                    XMLVoucher = cpIntegraEMV.sndReimpresion(configClass.User, configClass.Pass, configClass.Company, configClass.Branch, configClass.Country, nu_operaion.InnerText);
                    XmlDocument xmlDocVoucherComercio = new XmlDocument();
                    XmlDocument xmlDocVoucherCliente = new XmlDocument();
                    if (!string.IsNullOrEmpty(XMLVoucher))
                    {
                        string VoucherComercio = XMLVoucher.Substring(XMLVoucher.IndexOf("<voucher_comercio>"), XMLVoucher.IndexOf("</voucher_comercio>") + 19);
                        xmlDocVoucherComercio.LoadXml(VoucherComercio);
                        string VoucherCliente = XMLVoucher.Substring(XMLVoucher.IndexOf("<voucher_cliente>"), (XMLVoucher.IndexOf("</voucher_cliente>") + 18 - (XMLVoucher.IndexOf("<voucher_cliente>"))));
                        xmlDocVoucherCliente.LoadXml(VoucherCliente);
                        VComercio = xmlDocVoucherComercio.InnerText;
                        VCliente = xmlDocVoucherCliente.InnerText;
                        ImprimeVoucher();
                    }

                    status = 2;
                    decimal totalPayed = decimal.Parse(nu_importe.InnerText.Replace(",", ""));
                    SaveTransactionFromXML(totalPayed, status, cd_instrumento.InnerText, nu_auth.InnerText, nb_currency.InnerText, nu_operaion.InnerText, nb_referencia.InnerText, fh_bank.InnerText, cc_nombre.InnerText, cc_num.InnerText, tp_operacion.InnerText);
                    ControlTextBox();
                    Cerrar();
                    break;

                case "denied":
                    respuesta =
                        "          Cancelación declinada" + "\n" +
                        "No se realizó ningún reintegro a su tarjeta." + "\n" +
                        "La operación fue Declinada por su Banco Emisor. \n" +
                          nb_resp.InnerText;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                    break;

                case "error":
                    respuesta =
                         "               Error \n" +
                         "Ocurrió un error al realizar la cancelación \n" +
                         "No se realizó ningún reintegro a su tarjeta \n" +
                          nb_resp.InnerText;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                    break;

                default:
                    respuesta =
                        "               Error \n" +
                        "Ocurrió un error al realizar la cancelación \n" +
                        "No se realizó ningún reintegro a su tarjeta \n" +
                        nb_resp.InnerText;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cerrar();
                    break;
            }
        }

        private void VerificaRespuestaVentaXML(XmlDocument xmlRetVal)
        {
            string respuesta = string.Empty;
            string txtVoucher = string.Empty;
            string XMLVoucher = string.Empty;

            XmlNode responseVenta = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_response");

            XmlNode nu_operaion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_operaion");
            XmlNode cd_usuario = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_usuario");
            XmlNode cd_empresa = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_empresa");
            XmlNode nu_sucursal = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_sucursal");
            XmlNode nu_afiliacion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_afiliacion");
            XmlNode nb_referencia = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_referencia");
            XmlNode cc_nombre = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_nombre");
            XmlNode cc_num = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_num");
            XmlNode cc_tp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cc_tp");
            XmlNode nu_importe = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_importe");
            XmlNode cd_tipopago = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_tipopago");
            XmlNode cd_tipocobro = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_tipocobro");
            XmlNode cd_instrumento = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_instrumento");
            XmlNode nb_response = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_response");
            XmlNode nu_auth = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nu_auth");
            XmlNode fh_registro = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/fh_registro");
            XmlNode fh_bank = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/fh_bank");
            XmlNode cd_usrtransaccion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_usrtransaccion");
            XmlNode tp_operacion = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/tp_operacion");
            XmlNode nb_currency = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_currency");
            XmlNode cd_resp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/cd_resp");
            XmlNode nb_resp = xmlRetVal.DocumentElement.SelectSingleNode("transaccion/nb_resp");

            switch (responseVenta.InnerText)
            {
                case "approved":  //Transacción Aprobada
                    respuesta = "          Cobro aprobado" + "\n" +
                    "Núm. de Referencia: " + nb_referencia.InnerText + "\n" +
                    "Núm. de Operación: " + nu_operaion.InnerText + "\n" +
                    "Núm. de Autorización: " + nu_auth.InnerText + "\n" +
                    "Tipo de Tarjeta: " + cc_tp.InnerText + " " + cd_instrumento.InnerText + "\n" +
                    "Nombre TH: " + cc_nombre.InnerText + "\n" +
                    "Núm. Tarjeta: " + cc_num.InnerText + "\n" +
                    "Vencimiento: " + cpIntegraEMV.getCc_ExpMonth() + "/" + cpIntegraEMV.getCc_ExpYear() + "\n" +
                    "Monto Cobro: $" + nu_importe.InnerText + " " + configClass.Currency + "\n" +
                    "Fecha Operación: " + fh_bank.InnerText;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    XMLVoucher = cpIntegraEMV.sndReimpresion(configClass.User, configClass.Pass, configClass.Company, configClass.Branch, configClass.Country, nu_operaion.InnerText);
                    XmlDocument xmlDocVoucherComercio = new XmlDocument();
                    XmlDocument xmlDocVoucherCliente = new XmlDocument();
                    if (!string.IsNullOrEmpty(XMLVoucher))
                    {
                        string VoucherComercio = XMLVoucher.Substring(XMLVoucher.IndexOf("<voucher_comercio>"), XMLVoucher.IndexOf("</voucher_comercio>") + 19);
                        xmlDocVoucherComercio.LoadXml(VoucherComercio);
                        string VoucherCliente = XMLVoucher.Substring(XMLVoucher.IndexOf("<voucher_cliente>"), (XMLVoucher.IndexOf("</voucher_cliente>") + 18 - (XMLVoucher.IndexOf("<voucher_cliente>"))));
                        xmlDocVoucherCliente.LoadXml(VoucherCliente);

                        VComercio = xmlDocVoucherComercio.InnerText;
                        VCliente = xmlDocVoucherCliente.InnerText;
                        ImprimeVoucher();
                    }

                    status = 1;
                    decimal totalPayed = decimal.Parse(nu_importe.InnerText.Replace(",", ""));

                    string validadorTarjeta = string.Empty;
                    validadorTarjeta = cc_tp.InnerText;
                    asignaCardTypeId(validadorTarjeta);

                    SaveTransactionFromXML(totalPayed, status, cd_instrumento.InnerText, nu_auth.InnerText, nb_currency.InnerText, nu_operaion.InnerText, nb_referencia.InnerText, fh_bank.InnerText, cc_nombre.InnerText, cc_num.InnerText, tp_operacion.InnerText);
                    ControlTextBox();
                    this.DialogResult = DialogResult.OK;
                    Cerrar();
                    break;

                case "denied":
                    respuesta =
                        "            Cobro declinado" + "\n" +
                        "No se realizó ningún cargo a su tarjeta." + "\n" +
                        "La operación fue Declinada por su Banco Emisor. \n" +
                        "Favor de intentar con otra tarjeta." + "\n" +
                        nb_resp.InnerText;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;
                case "error":
                    respuesta =
                         "               Error " + "\n" +
                        "Ocurrió un error al realizar el pago." + "\n" +
                        "No se realizó ningún cargo a su tarjeta." + "\n" +
                        "Favor de intentar con otra tarjeta." + "\n" +
                      nb_resp.InnerText;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;
                default:
                    respuesta =
                          "               Error " + "\n" +
                        "Ocurrió un error al realizar el pago." + "\n" +
                        "No se realizó ningún cargo a su tarjeta." + "\n" +
                        "Favor de intentar con otra tarjeta." + "\n" +
                          nb_resp.InnerText;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    CancelarOperacion();
                    break;
            }
        }

        private void ImprimeVoucher()
        {
            PrintClass print = new PrintClass(apps);

            //Preguntar por Pin Pad con impresora integrada
            if (cpIntegraEMV.chkPp_Printer == "1")
            {
                if (!string.IsNullOrEmpty(cpIntegraEMV.getRspVoucherCliente()) && cpIntegraEMV.getRspVoucherCliente() != "")
                {
                    cpIntegraEMV.dbgPrintVoucher(cpIntegraEMV.getRspVoucher());
                }
            }
            else
            {
                if (VComercio != "")
                {
                    if (formatedXML) { print.ImprimeTicketFromXML(VComercio); }
                    else { print.ImprimeTicket(VComercio); }
                }
                if (VCliente != "")
                {
                    if (formatedXML) { print.ImprimeTicketFromXML(VCliente); }
                    else { print.ImprimeTicket(VCliente); }
                }
            }
            //DialogResult dialogResult = apps.Services.Dialog.ShowMessage(
            //           "¿Desea imprimir el ticket en la impresora del POS?",
            //           MessageBoxButtons.YesNo,
            //           MessageBoxIcon.Question);
            //if (dialogResult == DialogResult.Yes)
            //{
            //    if (VComercio != "")
            //    {
            //        if (formatedXML) { print.ImprimeTicketFromXML(VComercio); }
            //        else { print.ImprimeTicket(VComercio); }
            //    }
            //    if (VCliente != "")
            //    {
            //        if (formatedXML) { print.ImprimeTicketFromXML(VCliente); }
            //        else { print.ImprimeTicket(VCliente); }
            //    }
            //}
        }

        /// <summary>
        /// Controlar los campos que se llenarán automáticamente con MIT
        /// </summary>
        private void ControlTextBox()
        {
            txtName.Enabled = false;
            txtCarnbr.Enabled = false;
            txtNote.Enabled = false;
            txtSecCod.Enabled = false;
            txtexpDate.Enabled = false;
            comboBox1.Enabled = false;
        }

        private void LecturaTarjeta()
        {
            try
            {
                if (cpIntegraEMV.dbgSetReader() == true)
                {
                    if (cpIntegraEMV.chkPp_Com != "9")
                    {
                        CreaMensaje("La pin pad se encuentra conectada en el puerto COM" + cpIntegraEMV.chkPp_Com + " se recomienda cambiarlo al puerto COM9.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    cbxType = string.Empty;
                    cvvAmex = string.Empty;
                    cpIntegraEMV.dbgStartTxEMV(this.txtAmt.Text.ToString().Replace(",", ""));

                    if (cpIntegraEMV.chkPp_CdError == "")
                    {
                        txtCarnbr.Text = cpIntegraEMV.chkCc_Number;
                        txtName.Text = "";
                        txtName.Text = cpIntegraEMV.chkCc_Name;

                        string mesTarjeta = string.Empty;
                        string anioTarjeta = string.Empty;

                        if (!string.IsNullOrEmpty(cpIntegraEMV.chkCc_ExpMonth) && !string.IsNullOrEmpty(cpIntegraEMV.chkCc_ExpYear))
                            AsignaFechaTarjeta();

                        #region Reglas de negocio Anfora
                        //tendertype = indiceSantanderCredito;//Default
                        //string validadorTarjeta = string.Empty;
                        //validadorTarjeta = cpIntegraEMV.getCc_Type();
                        //if (validadorTarjeta.Contains("AMEX"))
                        //{
                        //    cbxType = "AMEX";
                        //    tendertype = indiceAmerican;
                        //}
                        //else
                        //{
                        //    cbxType = "V/MC";
                        //    if (validadorTarjeta.ToUpper().Contains("CREDITO"))
                        //    {
                        //        tendertype = indiceSantanderCredito;
                        //    }
                        //    else if (validadorTarjeta.ToUpper().Contains("DEBITO"))
                        //    {
                        //        tendertype = indiceSantanderDebito;
                        //    }
                        //    else if (!validadorTarjeta.ToUpper().Contains("DEBITO") && !validadorTarjeta.ToUpper().Contains("CREDITO"))
                        //    {
                        //        DialogResult dialogResult = apps.Services.Dialog.ShowMessage(
                        //                   "¿Es tarjeta de CRÉDITO?",
                        //                   MessageBoxButtons.YesNo,
                        //                   MessageBoxIcon.Question);
                        //        if (dialogResult == DialogResult.Yes)
                        //        {
                        //            tendertype = indiceSantanderCredito;
                        //        }
                        //        else if (dialogResult == DialogResult.No)
                        //        {
                        //            tendertype = indiceSantanderDebito;
                        //        }
                        //    }
                        //}
                        #endregion

                        respuestaMerchant = cpIntegraEMV.dbgGetMerchantBanda(configClass.Tp_operacion);
                        if (!string.IsNullOrEmpty(respuestaMerchant))
                        {
                            lecturaCorrecta = true;
                        }
                        else
                        {
                            string respuesta =
                               "          Error de lectura \n" +
                               "Ocurrió un error en la lectura de su tarjeta. \n" +
                               "No se realizó ningún cargo a su tarjeta. \n" +
                               cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.chkPp_DsError;
                            CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            LecturaIncorrecta();
                        }
                    }
                    else
                    {
                        string respuesta =
                               "          Error de lectura \n" +
                               "Ocurrió un error en la lectura de su tarjeta. \n" +
                               "No se realizó ningún cargo a su tarjeta. \n" +
                               cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.chkPp_DsError;
                        CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        LecturaIncorrecta();
                    }
                }
                else
                {
                    string respuesta =
                                           "          Error de lectura \n" +
                                           "Ocurrió un error en el dispositivo. \n" +
                                           "No se realizó ningún cargo a su tarjeta. \n" +
                                           cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.chkPp_DsError;
                    CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    LecturaIncorrecta();
                }
            }
            catch (Exception ex)
            {
                string respuesta =
                                           "          Error de lectura \n" +
                                           "Ocurrió un error en el dispositivo. \n" +
                                           "No se realizó ningún cargo a su tarjeta. \n" +
                                           ex.Message + "\n" +
                                           cpIntegraEMV.getRspCdError() + "-" + cpIntegraEMV.chkPp_DsError;
                CreaMensaje(respuesta, MessageBoxButtons.OK, MessageBoxIcon.Error);
                LecturaIncorrecta();
            }
        }

        private void LecturaIncorrecta()
        {
            CancelarOperacion();
        }

        private void EjecutaCobroBanda(string Merchant)
        {
            cpIntegraEMV.sndVtaDirectaEMV(configClass.User, configClass.Pass, Transaction.Shift.StaffId, configClass.Company, configClass.Branch, configClass.Country, cbxType, Merchant, referencia, configClass.Tp_operacion, configClass.Currency, ref cvvAmex);
        }

        private void EjecutaCancelacion()
        {
            if (cargoCancelacion)
            {
                string amountCancela = decimal.Negate(decimal.Parse(txtAmt.Text.Replace(",", ""))).ToString();
                cpIntegraEMV.sndCancelacion(configClass.User, configClass.Pass, Transaction.Shift.StaffId, configClass.Company, configClass.Branch, configClass.Country, amountCancela, OperationNumber, AutNumber);
            }
            else
            {
                CreaMensaje("No se ha cargado la cancelación correctamente. Contacte a sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void numAmount_EnterPressed()
        {
            txtAmt.Text = numAmount.EnteredDecimalValue.ToString();
        }

        private bool ValidaObligatorios()
        {
            if (txtCarnbr.Text.Trim() == "" || txtNote.Text.Trim() == "")
            {
                CreaMensaje("Por favor llenar los campos obligatorios", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (!ValidaDigitos())
            {
                return false;
            }
            else
                return true;
        }

        private bool ValidaDigitos()
        {
            if (txtCarnbr.Text.Trim() != "")
            {
                string tempString = string.Empty;
                long IntegerTemp = 0;
                tempString = txtCarnbr.Text.Trim();
                if (tempString.Length != 4 || !long.TryParse(tempString, out IntegerTemp))
                {
                    CreaMensaje("El número de dígitos del número de la tarjeta no es válido. Escriba únicamente los últimos 4 dígitos.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return true;
        }

        private void DetectPinPad()
        {
            if (cpIntegraEMV.dbgSetReader() == true)
            {

            }
            else
            {
                CreaMensaje("No es encontró el lector conectado. Contacte a sistemas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cerrar();
            }
        }

        private void AsignaFechaTarjeta()
        {
            System.DateTime moment = DateTime.Today;
            System.DateTime newDate = new System.DateTime(Int32.Parse(cpIntegraEMV.chkCc_ExpYear) + 2000, Int32.Parse(cpIntegraEMV.chkCc_ExpMonth), 1);

            txtexpDate.Value = newDate;
        }

        private void SaveTransaction(decimal amount, int status)
        {
            Tender t = new Tender();
            t = new TenderData(ApplicationSettings.Database.LocalConnection, ApplicationSettings.Database.DATAAREAID).GetTender(tenderTypeId, ApplicationSettings.Terminal.StoreId);
            LSRetailPosis.Transaction.Line.TenderItem.TenderLineItem lt = new LSRetailPosis.Transaction.Line.TenderItem.TenderLineItem();
            lt.Amount = amount;
            lt.Comment = "Pago MIT";
            lt.Description = t.TenderName;
            lt.AboveMinimumTenderId = t.AboveMinimumTenderId;
            lt.ChangeTenderID = t.ChangeTenderID;
            lt.TenderTypeId = t.TenderID;
            Transaction.Add(lt, true);
            Transaction.CalcTotals();
            Transaction.CalculateAmountDue();
            Transaction.Save();
            bool retval = false;
            retval = odac.GuardaPagoTarjeta(configClass.User, Transaction.Shift.StaffId, cpIntegraEMV.chkCc_Name, cpIntegraEMV.chkCc_Number.Replace(" ", ""), txtexpDate.Text, cpIntegraEMV.getCc_Type(), txtAmt.Text.Replace(",", ""), cpIntegraEMV.getRspAuth(), configClass.Currency, cpIntegraEMV.getRspDsMerchant(), cpIntegraEMV.getRspDsOperationType(), cpIntegraEMV.getRspOperationNumber(), cpIntegraEMV.getTx_Reference(), cpIntegraEMV.getRspDate(), DateTime.Now.ToString("HHmmss"), apps.Settings.Database.DataAreaID, Transaction.StoreId, Transaction.Shift.TerminalId, Transaction.TransactionId, 1, lt.LineId, Transaction.Shift.BatchId);
            if (retval == false)
            {
                CreaMensaje("Error al guardar la transacción, favor de contactar con sistemas, la operación continuará.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //CreaMensaje(cardTypeId, MessageBoxButtons.OK, MessageBoxIcon.Information);

            odac.GuardaDetalletarjeta(ApplicationSettings.Database.StoreID, ApplicationSettings.Database.DATAAREAID, Transaction.Shift.TerminalId, Transaction.TenderLines.Last.Value.LineId, tenderTypeId, cardTypeId, Transaction.TransactionId);

            if ((Transaction.TransSalePmtDiff) == 0)
            {
                LSRetailPosis.POSProcesses.PayCash payC = new LSRetailPosis.POSProcesses.PayCash();
                payC.Amount = 0;
                payC.OperationID = PosisOperations.PayCash;
                payC.OperationInfo = new LSRetailPosis.POSProcesses.OperationInfo();
                payC.POSTransaction = Transaction;
                payC.RunOperation();
            }
        }

        private void SaveTransactionFromXML(decimal amount, int status, string cd_instrumento, string nu_auth, string nb_currency, string nu_operaion, string nb_referencia, string fh_bank, string cc_nombre, string cc_num, string tp_operacion)
        {
            Tender t = new Tender();
            //tendertype = comboBox1.SelectedValue.ToString();
            t = new TenderData(ApplicationSettings.Database.LocalConnection, ApplicationSettings.Database.DATAAREAID).GetTender(tenderTypeId, ApplicationSettings.Terminal.StoreId);
            LSRetailPosis.Transaction.Line.TenderItem.TenderLineItem lt = new LSRetailPosis.Transaction.Line.TenderItem.TenderLineItem();
            lt.Amount = amount;
            lt.Comment = "Pago MIT";
            lt.Description = t.TenderName;
            lt.AboveMinimumTenderId = t.AboveMinimumTenderId;
            lt.ChangeTenderID = t.ChangeTenderID;
            lt.TenderTypeId = t.TenderID;
            Transaction.Add(lt, true);
            Transaction.CalcTotals();
            Transaction.CalculateAmountDue();
            Transaction.Save();
            bool retval = false;
            if (string.IsNullOrEmpty(respuestaMerchant))
            {
                respuestaMerchant = configClass.Tp_operacion;
            }
            string tipoOperacion = string.Empty;
            if (!string.IsNullOrEmpty(tp_operacion))
            {
                tipoOperacion = tp_operacion.Substring(0, 3);
            }
            else
            {
                if (!numAmount.NegativeMode)
                {
                    tipoOperacion = "VEN";
                }
                else
                {
                    tipoOperacion = "CAN";
                }
            }
            retval = odac.GuardaPagoTarjeta(configClass.User, Transaction.Shift.StaffId, cc_nombre, cc_num.Replace(" ", ""), txtexpDate.Text, cd_instrumento, amount.ToString().Replace(",", ""), nu_auth, nb_currency, respuestaMerchant, tipoOperacion, nu_operaion, nb_referencia, fh_bank, DateTime.Now.ToString("HHmmss"), apps.Settings.Database.DataAreaID, Transaction.StoreId, Transaction.Shift.TerminalId, Transaction.TransactionId, 1, lt.LineId, Transaction.Shift.BatchId);
            if (retval == false)
            {
                CreaMensaje("Error al guardar la transacción, favor de contactar con sistemas, la operación continuará.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            odac.GuardaDetalletarjeta(ApplicationSettings.Database.StoreID, ApplicationSettings.Database.DATAAREAID, Transaction.Shift.TerminalId, Transaction.TenderLines.Last.Value.LineId, tenderTypeId, cardTypeId, Transaction.TransactionId);

            if ((Transaction.TransSalePmtDiff) == 0)
            {
                LSRetailPosis.POSProcesses.PayCash payC = new LSRetailPosis.POSProcesses.PayCash();
                payC.Amount = 0;
                payC.OperationID = PosisOperations.PayCash;
                payC.OperationInfo = new LSRetailPosis.POSProcesses.OperationInfo();
                payC.POSTransaction = Transaction;
                payC.RunOperation();
            }
        }

        private void BtnAmt_Click_1(object sender, EventArgs e)
        {
            txtAmt.Text = apps.Services.Rounding.Round(amtDue, false);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataRowView DrV;
            //string TempValue = string.Empty;
            //DrV = (DataRowView)comboBox1.SelectedItem;
            //TempValue = DrV.Row.ItemArray[1].ToString().ToUpperInvariant();
        }

        private void txtCarnbr_TextChanged(object sender, EventArgs e)
        {

        }

        private void CargaCancelacion()
        {
            try
            {
                AutNumber = string.Empty;
                OperationNumber = string.Empty;
                ReturnTransId = string.Empty;
                ReturnTerminalId = string.Empty;
                ReturnStoreId = string.Empty;
                if (Transaction.SaleItems.Count > 0)
                {
                    foreach (SaleLineItem item in Transaction.SaleItems)
                    {
                        if (!item.Voided)
                        {
                            if (!string.IsNullOrEmpty(item.ReturnTransId) && !string.IsNullOrEmpty(item.ReturnTerminalId) && !string.IsNullOrEmpty(item.ReturnStoreId))
                            {
                                ReturnTransId = item.ReturnTransId;
                                ReturnTerminalId = Transaction.Shift.TerminalId;
                                ReturnStoreId = item.ReturnStoreId;
                                continue;
                            }
                        }
                    }
                }
                DAC odac = new DAC(new SqlConnection(apps.Settings.Database.Connection.ConnectionString));
                DataTable DtConfig = odac.selectData("Select [CC_NUMBER],[TX_AMOUNT],[TX_AUTH],[TX_OPERATIONNUMBER],[TX_REFERENCE],[FECHAHORA] from [ax].[GRW_MITTRAN] where DataAreaId = '" + apps.Settings.Database.DataAreaID + "' AND TRANSACTIONID = '" + ReturnTransId + "' AND STOREID = '" + ReturnStoreId + "' AND TERMINAL = '" + ReturnTerminalId + "'");
                if (DtConfig.Rows.Count > 0)
                {
                    using (frmTran nfrmTran = new frmTran(DtConfig, decimal.Parse(this.txtAmt.Text.ToString().Replace(",", ""))))
                    {
                        LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(nfrmTran);
                        if (nfrmTran.DialogResult == DialogResult.OK)
                        {
                            if (!string.IsNullOrEmpty(nfrmTran.selectedReference))
                            {
                                OperationNumber = nfrmTran.selectedOperationNum;
                                AutNumber = nfrmTran.selectedAuth;
                                referencia = nfrmTran.selectedReference;
                                CANCEL_AMOUNT = nfrmTran.selectedAmount;
                                CANCEL_AMOUNT = decimal.Negate(decimal.Parse(CANCEL_AMOUNT.Replace(",", ""))).ToString();
                                this.txtAmt.Text = CANCEL_AMOUNT;
                                amtDue = decimal.Parse(CANCEL_AMOUNT);
                                lblTotalAmtDue.Text = "Monto total de la deuda " + CANCEL_AMOUNT;
                                BtnAmt.Text = CANCEL_AMOUNT;
                                cargoCancelacion = true;
                            }
                        }
                    }
                }
                else
                {
                    CreaMensaje("No se pudo cargar la transacción de cancelación, favor de contactar con Sistemas.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.BtnAceptar.Enabled = false;
                    CancelarOperacion();
                    Cerrar();
                }
            }
            catch (Exception ex)
            {
                CreaMensaje("No se pudo cargar la transacción de cancelación.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.BtnAceptar.Enabled = false;
                CancelarOperacion();
                Cerrar();
            }
        }

        private void BtnCancel_Click_1(object sender, EventArgs e)
        {
            Cerrar();
        }

        private bool cargaFormasPago()
        {
            bool retval = false;
            try
            {
                odac = new DAC(apps.Settings.Database.Connection);
                DtFormas = odac.getCardTenderTypes(apps.Settings.Database.DataAreaID);
                #region Validate Voided TenderTypes
                if (DtFormas.Rows.Count > 0)
                {
                    foreach (DataRow dr in DtFormas.Rows)
                    {
                        if (dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("CREDIT"))
                        {
                            formaPagoCredito = dr["RETAILCARDTYPEID"].ToString();
                        }
                        else if (dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("DEBIT"))
                        {
                            formaPagoDebito = dr["RETAILCARDTYPEID"].ToString();
                        }
                        else if (dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("AMEX") && !dr["CARDTYPE"].ToString().ToUpperInvariant().Contains("BANAMEX"))
                        {
                            formaPagoAmex = dr["RETAILCARDTYPEID"].ToString();
                        }
                    }
                    if (!string.IsNullOrEmpty(formaPagoCredito) && !string.IsNullOrEmpty(formaPagoDebito) && !string.IsNullOrEmpty(formaPagoAmex))
                        retval = true;
                    else
                    {
                        CreaMensaje("Error al cargar las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    CreaMensaje("Error al cargar las formas de pago. ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                #endregion
            }
            catch (Exception ex)
            {
                CreaMensaje("Error al cargar las formas de pago.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CreaMensaje("Descripción del error: " + ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return retval;
        }

        #endregion

        #region COMPLEMENTARY METHODS

        private void CancelarOperacion()
        {
            cpIntegraEMV.dbgCancelOperation();
            Cerrar();
        }

        private void Cerrar()
        {
            cpIntegraEMV.dbgEndOperation();
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void CreaMensaje(string mensaje, MessageBoxButtons btn, MessageBoxIcon icon)
        {
            using (LSRetailPosis.POSProcesses.frmMessage dialog = new LSRetailPosis.POSProcesses.frmMessage(mensaje, btn, icon))
            {
                LSRetailPosis.POSProcesses.POSFormsManager.ShowPOSForm(dialog);
            }
        }

        private void TranslateLabels()
        {
            //this.Text = "Cliente"; // Customer
        }

        /// <summary>
        /// Método que recibe el valor a formatear con ceros a la izquierda y la cantidad de caracteres que debe contener, el resultado es un string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cZero"></param>
        /// <returns></returns>
        private string formatZeroValues(int value, int cZero)
        {
            string formatedValue = string.Empty;
            string zeroCharacters = string.Empty;

            try
            {
                for (int i = 1; i <= cZero; i++)
                {
                    zeroCharacters += "0";
                }

                formatedValue = zeroCharacters + value.ToString();
                formatedValue = formatedValue.Substring(formatedValue.Length - cZero);

            }
            catch (Exception ex)
            {
                CreaMensaje("Ocurrió un error, favor de contactar con sistemas. " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return formatedValue;
        }

        //private void CargaFormasPago(IApplication appli)
        //{
        //    odac = new DAC(appli.Settings.Database.Connection);
        //    DtFormas = odac.getCardDetailPayments(appli.Shift.StoreId, appli.Settings.Database.DataAreaID, tenderTypeId);
        //    comboBox1.DisplayMember = "Descripcion";
        //    comboBox1.ValueMember = "CardTypeId";
        //    comboBox1.DataSource = DtFormas;
        //}

        #endregion

    }
}
